package ua.com.danit.classworks;

import java.io.*;

public class HelloWorld {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Writer wr = new FileWriter("out.txt");
        String str = reader.readLine();
        wr.write(str);
        wr.flush();
        wr.close();
    }
}
