package ua.com.danit.classworks.classwork11;

import java.util.Date;

public class DateClass {
    private static Date dateOfStart;

    public static void main(String[] args) {
        startTime(1595526546311L);
        System.out.println(timeLeft());
    }

    public static long timeLeft() {
        return (dateOfStart.getTime() + 90 * 60 * 1000) / 1000 / 60;
    }

    public static void startTime(long millis) {
        dateOfStart = new Date(millis);
    }
}
