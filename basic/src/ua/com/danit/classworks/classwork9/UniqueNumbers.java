package ua.com.danit.classworks.classwork9;

import java.util.HashSet;
import java.util.Random;

public class UniqueNumbers {
    public static void main(String[] args) {
        Random random = new Random();
        HashSet<Integer> hashSet = new HashSet<>();

        while(hashSet.size() < 20) {
            hashSet.add(random.nextInt(30));
        }
        System.out.println(hashSet);
    }
}
