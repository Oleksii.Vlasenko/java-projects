package ua.com.danit.classworks.classwork4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class StartTask {
    String name;
    int a;
    byte b;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public byte getB() {
        return b;
    }

    public void setB(byte b) {
        this.b = b;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(Math.abs((-31) % 2));
    }
}
