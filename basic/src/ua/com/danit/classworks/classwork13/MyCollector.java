package ua.com.danit.classworks.classwork13;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class MyCollector<E> implements Collector<E, LinkedList<E>, LinkedList<E>> {
    @Override
    public Supplier<LinkedList<E>> supplier() {
        return LinkedList::new;
    }

    @Override
    public BiConsumer<LinkedList<E>, E> accumulator() {
        return LinkedList::add;
    }

    @Override
    public BinaryOperator<LinkedList<E>> combiner() {
        return (a, b) -> new LinkedList<E>(){{
            addAll(a);
            addAll(b);
        }};
    }

    @Override
    public Function<LinkedList<E>, LinkedList<E>> finisher() {
        return a -> a;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return null;
    }

    public static <E> LinkedList<E> toLinkedList() {
        return new LinkedList<E>();
    }
}

class MyClass {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6);
//        Object collect = integers.stream().collect();
    }
}