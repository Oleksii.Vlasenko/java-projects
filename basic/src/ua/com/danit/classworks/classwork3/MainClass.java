package ua.com.danit.classworks.classwork3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainClass {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String userLine = reader.readLine();
        String outputLine = userLine.substring(0, 1).toUpperCase() + userLine.substring(1).toLowerCase();
        System.out.println(outputLine);
    }
}
