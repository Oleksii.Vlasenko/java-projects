package ua.com.danit.classworks.classwork3;

import java.util.Arrays;
import java.util.Random;

public class StringDemo2 {
    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[15];
        int i = 0;
        while (i < 15) {
            int item = random.nextInt(15) + 1;
            if (Arrays.asList(arr).indexOf(item) < 0) {
                arr[i] = item;
                i++;
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}
