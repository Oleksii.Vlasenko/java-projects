package ua.com.danit.homeworks.homework_9.entity.humans;

import ua.com.danit.homeworks.homework_9.entity.Human;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

public final class Woman extends Human {

    public Woman() {
        super();
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void cook() {
        System.out.println("I am cooking!");
    }

    @Override
    public void greetPet() {
        System.out.println("I am your mother!");
    }

}
