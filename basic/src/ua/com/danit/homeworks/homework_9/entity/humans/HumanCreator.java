package ua.com.danit.homeworks.homework_9.entity.humans;

import ua.com.danit.homeworks.homework_9.entity.Human;

public interface HumanCreator {
    Human bornChild(Human human) throws Exception;
}
