package ua.com.danit.homeworks.homework_9.entity.humans;

import ua.com.danit.homeworks.homework_9.entity.Human;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

public final class Man extends Human {
    public Man() {
        super();
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void repair() {
        System.out.println("I am repairing!");
    }

    @Override
    public void greetPet() {
        System.out.println("I am your owner!");
    }
}
