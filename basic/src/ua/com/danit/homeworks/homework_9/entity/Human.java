package ua.com.danit.homeworks.homework_9.entity;

import ua.com.danit.homeworks.homework_9.entity.humans.HumanCreator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.Random;

public abstract class Human {

    private final String name;
    private final String surname;
    private final int year;
    private final int iq;
    private final HashMap<String, String> schedule;
    private Family family;

    static {
        System.out.println("Download the Human class");
    }

    {
        System.out.println("Create the Human object");
    }

    public Human() {
        this.name = "Unknown";
        this.surname = "Unknown";
        this.year = 0;
        this.iq = 0;
        this.schedule = null;
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = 0;
        this.schedule = null;
    }

    public Human(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public HashMap<String, String> getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void greetPet() {
        Iterator<Pet> iter = this.family.getPets().iterator();
        while(iter.hasNext()) {
            System.out.println("Привет, " + iter.next().getNickname());
        }
    }

    public void describePet() {
        for (Pet pet : this.family.getPets()) {
            String trickLevel = pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
            String message = "У меня есть "
                    + pet.getSpecies()
                    + ", ему "
                    + pet.getAge()
                    + " лет, он " + trickLevel
                    + ".";
            System.out.println(message);
        }
    }

    public boolean feedPet(boolean feed) {
        if (this.family.getPets().size() == 0) {
            System.out.println("There are no pets!");
            return false;
        }
        boolean isFeed = true;
        if (!feed) {
            Random random = new Random();
            for (Pet pet : this.family.getPets()) {
                int randomFeed = random.nextInt(101);
                System.out.println(randomFeed < pet.getTrickLevel()
                        ? "Хм... покормлю ка я " + pet.getNickname() + "."
                        : "Думаю, " + pet.getNickname() + " не голоден.");
                isFeed = isFeed && pet.getTrickLevel() > randomFeed;
            }
            return isFeed;
        }
        Iterator<Pet> iter = this.family.getPets().iterator();
        while(iter.hasNext()) {
            System.out.println("Покормлю ка я " + iter.next().getNickname() + ".");
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Human{name='")
                .append(this.name)
                .append("', surname='")
                .append(this.surname)
                .append("', year=")
                .append(this.year);
        if (this.iq != 0) {
            str.append(", iq=").append(this.iq);
        }
        if (this.schedule != null) {
            str.append(", schedule=").append(this.schedule.toString());
        }
        str.append("}");
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                iq == human.iq;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }

    @Override
    public void finalize() {
        System.out.println("Human object deleted!");
    }
}
