package ua.com.danit.homeworks.homework_9.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_9.entity.Family;
import ua.com.danit.homeworks.homework_9.entity.Human;
import ua.com.danit.homeworks.homework_9.entity.Pet;
import ua.com.danit.homeworks.homework_9.entity.humans.Man;
import ua.com.danit.homeworks.homework_9.entity.humans.Woman;
import ua.com.danit.homeworks.homework_9.entity.pets.Dog;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class DogTest {
    private static Family family;
    private static Human mother;
    private static Human father;
    private static HashSet<Pet> pets;
    private static Family family_2;
    private static Human mother_2;
    private static Human father_2;

    @BeforeAll
    public static void initHuman() {
        mother = new Woman("Mother", "Family", 2000);
        father = new Man("Father", "Family", 2000);
        pets = new HashSet<Pet>(Collections.singletonList(new Dog("BILL")));
        family = new Family(mother, father, pets);

        mother_2 = new Woman();
        father_2 = new Man();
        family_2 = new Family(mother_2, father_2);
    }

    @Test
    @DisplayName("must get same family")
    void getFamily() {
        Iterator<Pet> iter = pets.iterator();
        boolean isSameFamily = true;
        while(iter.hasNext()) {
            isSameFamily = isSameFamily && (iter.next().getFamily().equals(family_2));
        }
        assertTrue(isSameFamily);
    }

    @Test
    @DisplayName("must set family")
    void setFamily() {
        Iterator<Pet> iter = pets.iterator();
        while(iter.hasNext()) {
            iter.next().setFamily(family_2);
        }
        boolean isSameFamily = true;
        Iterator<Pet> iter2 = pets.iterator();
        while(iter.hasNext()) {
            isSameFamily = isSameFamily && (iter.next().getFamily().equals(family_2));
        }
        assertTrue(isSameFamily);
    }

    @Test
    @DisplayName("must set nickname")
    void setNickname() {
        for (Pet value : pets) {
            value.setNickname("WILLY");
            break;
        }
        boolean flag = false;
        for (Pet pet : pets) {
            flag = flag || pet.getNickname().equals("WILLY");
            break;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("must set new age")
    void setAge() {
        for (Pet value : pets) {
            value.setAge(5);
            break;
        }
        boolean flag = false;
        for (Pet pet : pets) {
            flag = flag || pet.getAge() == 5;
            break;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("must set trick level")
    void setTrickLevel() {
        for (Pet value : pets) {
            value.setTrickLevel(70);
            break;
        }
        boolean flag = false;
        for (Pet pet : pets) {
            flag = flag || pet.getTrickLevel() == 70;
            break;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("must set and get habits")
    void getHabits() {
        HashSet<String> habits = new HashSet<String>(Arrays.asList("sleep", "jump", "run"));
        for (Pet value : pets) {
            value.setHabits(habits);
            break;
        }
        boolean flag = false;
        for (Pet pet : pets) {
            flag = flag || pet.getHabits().equals(habits);
            break;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("must get species")
    void getSpecies() {
        String species = "";
        for (Pet pet : pets) {
            species = pet.getSpecies();
            break;
        }
        assertEquals("DOG", species);
    }

    @Test
    @DisplayName("must get nickname")
    void getNickname() {
        Pet dog = new Dog("BILL");
        assertEquals("BILL", dog.getNickname());
    }

    @Test
    @DisplayName("must get trick level")
    void getTrickLevel() {
        Dog dog = new Dog("BILL");
        dog.setTrickLevel(70);
        assertEquals(70, dog.getTrickLevel());
    }

    @Test
    @DisplayName("must get age")
    void getAge() {
        Dog dog = new Dog("BILL");
        dog.setAge(3);
        assertEquals(3, dog.getAge());
    }

    @Test
    @DisplayName("must get pet.toString")
    void testToString() {
        Dog dog = new Dog("BILL");
        String str = dog.getSpecies() + "{nickname='" + dog.getNickname() + "', trickLevel=" + dog.getTrickLevel();
        str += ", can't fly, has 4 legs, has fur}";
        assertEquals(str, dog.toString());
    }

    @Test
    @DisplayName("pet must equals new pet")
    void testEquals() {
        Pet newPet = new Dog("BILL");
        newPet.setAge(5);
        newPet.setFamily(family_2);
        Pet newPet2 = new Dog("BILL");
        newPet2.setAge(5);
        newPet2.setFamily(family_2);
        assertEquals(newPet2, newPet);
    }

    @Test
    @DisplayName("pet must not equals new pet")
    void testEquals_false() {
        Pet newPet = new Dog("BILL");
        newPet.setAge(5);
        newPet.setFamily(family);
        Pet newPet2 = new Dog("BILL");
        newPet2.setAge(5);
        newPet2.setFamily(family_2);
        assertFalse(newPet2.equals(newPet));
    }

    @Test
    @DisplayName("pet's hashCode must equals family pet's")
    void testHashCode() {
        Pet newPet = new Dog("BILL");
        newPet.setAge(5);
        newPet.setFamily(family);
        Pet newPet2 = new Dog("BILL");
        newPet2.setAge(5);
        newPet2.setFamily(family_2);
        assertEquals(newPet.hashCode(), newPet2.hashCode());
    }

    @Test
    @DisplayName("pet's hashCode must not equals new pet's")
    void testHashCode_false() {
        Pet newPet = new Dog("BILL");
        newPet.setAge(4);
        newPet.setFamily(family);
        Pet newPet2 = new Dog("BILL");
        newPet2.setAge(5);
        newPet2.setFamily(family);
        assertNotEquals(newPet2.hashCode(), newPet.hashCode());
    }
}