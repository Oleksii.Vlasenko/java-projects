package ua.com.danit.homeworks.homework_9.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_9.entity.Family;
import ua.com.danit.homeworks.homework_9.entity.Human;
import ua.com.danit.homeworks.homework_9.entity.humans.Man;
import ua.com.danit.homeworks.homework_9.entity.humans.Woman;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    private static Family family;
    private static Woman mother;
    private static Man father;

    @BeforeAll
    public static void initFamily() throws Exception {
        mother = new Woman();
        father = new Man();
        family = new Family(mother, father);
    }

    @Test
    @DisplayName("Must added 1 child")
    public void addChild_true() {
        int beforeLength = family.getChildren().size();
        family.addChild(new Man());
        assertEquals(beforeLength + 1, family.getChildren().size());
    }

    @Test
    @DisplayName("Must not deleted child by index")
    void deleteChildByIndex_false() {
        family.deleteChild(1);
        assertNotEquals(3, family.getChildren().size());
    }

    @Test
    @DisplayName("must deleted child by index")
    void deleteChildByIndex() {
        int beforeLength = family.getChildren().size();
        family.addChild(new Man());
        family.addChild(new Man());
        family.addChild(new Man());
        family.deleteChild(1);
        assertEquals(beforeLength + 2, family.getChildren().size());
    }

    @Test
    @DisplayName("must deleted child")
    void deleteChildByChild() throws Exception {
        int beforeLength = family.getChildren().size();
        family.addChild(new Man());
        Human testChild = new Man();
        family.addChild(new Man());
        family.deleteChild(testChild);
        assertEquals(beforeLength + 1, family.getChildren().size());
    }


    @Test
    @DisplayName("Must added 2 family members")
    void countFamily() throws Exception {
        Family testFamily = new Family(new Woman(), new Man());
        testFamily.addChild(new Man());
        assertEquals(3, testFamily.countFamily());
    }

    @Test
    @DisplayName("must to String")
    void testToString() {
        Human mother_1 = new Woman("Mother_1", "Family_1", 2000);
        Human father_1 = new Man("Father_1", "Family_1", 2000);
        Family family_1 = new Family(mother_1, father_1);
        String str = mother_1.toString() + '\n' + father_1.toString();
        assertEquals(str, family_1.toString());
    }

    @Test
    @DisplayName("family must equal another family")
    void testEquals() {
        assertTrue(new Family(new Woman(), new Man()).equals(new Family(new Woman(), new Man())));
    }

    @Test
    @DisplayName("family must not equal new family")
    void testEquals_false () {
        Human mother_1 = new Woman("Mother", "Family", 2000, 148, null);
        assertFalse(new Family(mother_1, new Man()).equals(new Family(new Woman(), new Man())));
    }

    @Test
    @DisplayName("mother's hashCode must equals family mother's")
    void testHashCode() {
        assertEquals(family.hashCode(), mother.getFamily().hashCode());
    }

    @Test
    @DisplayName("family's hashCode must not equals another family's")
    void testHashCode_false() {
        Human mother_1 = new Woman();
        Human father_1 = new Man("Father_1", "Family_1", 1990);
        Family family_1 = new Family(mother_1, father_1);
        Human mother_2 = new Woman();
        Human father_2 = new Man();
        Family family_2 = new Family(mother_2, father_2);
        assertNotEquals(family_1.hashCode(), family_2.hashCode());
    }
}