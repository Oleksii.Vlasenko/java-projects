package ua.com.danit.homeworks.homework_9.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_9.entity.DayOfWeek;
import ua.com.danit.homeworks.homework_9.entity.Family;
import ua.com.danit.homeworks.homework_9.entity.Human;
import ua.com.danit.homeworks.homework_9.entity.Pet;
import ua.com.danit.homeworks.homework_9.entity.humans.Man;
import ua.com.danit.homeworks.homework_9.entity.humans.Woman;
import ua.com.danit.homeworks.homework_9.entity.pets.Dog;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class ManTest {
    private static Family family;
    private static Human mother;
    private static Human father;
    private static HashSet<Pet> pets;
    private static HashMap<String, String> schedule;

    @BeforeAll
    public static void initHuman() {

        HashMap<String, String> schedule = new HashMap<String, String>();
        schedule.put(DayOfWeek.MONDAY.name(), "Education");
        schedule.put(DayOfWeek.TUESDAY.name(), "Sport");
        schedule.put(DayOfWeek.WEDNESDAY.name(), "Education");
        schedule.put(DayOfWeek.THURSDAY.name(), "Sport");
        schedule.put(DayOfWeek.FRIDAY.name(), "Education");
        schedule.put(DayOfWeek.SATURDAY.name(), "Sport");
        schedule.put(DayOfWeek.SUNDAY.name(), "Rest");

        mother = new Woman("Mother", "Family", 2000, 148, schedule);
        father = new Man("Father", "Family", 2000, 148, schedule);
        pets = new HashSet<Pet>(Collections.singletonList(new Dog("BILL", 5, 0, null)));
        family = new Family(mother, father, pets);
    }

    @Test
    @DisplayName("Return human's name")
    void getName() {
        assertEquals("Father", father.getName());
    }

    @Test
    @DisplayName("Return human's surname")
    void getSurname() {
        assertEquals("Family", father.getSurname());
    }

    @Test
    @DisplayName("Return human's year")
    void getYear() {
        assertEquals(2000, father.getYear());
    }

    @Test
    @DisplayName("Return human's iq")
    void getIq() {
        assertEquals(148, father.getIq());
    }

    @Test
    @DisplayName("Return human's default iq")
    void getDefaultIq() {
        assertEquals(148, father.getIq());
    }

    @Test
    @DisplayName("family equals father.family")
    void getFamily() {
        assertEquals(null, father.getFamily());
    }

    @Test
    @DisplayName("father.family equals mother.family")
    void getFamilyByFamilyMembers() {
        assertEquals(father.getFamily(), mother.getFamily());
    }

    @Test
    @DisplayName("change father.family - null")
    void setFamily() {
        father.setFamily(null);
        assertEquals(null, father.getFamily());
    }

    @Test
    @DisplayName("feed pet")
    void feedPet() {
        assertTrue(father.feedPet(true));
    }

    @Test
    @DisplayName("feed pet - false")
    void dontFeedPet() {
        assertFalse(father.feedPet(false));
    }

    @Test
    @DisplayName("must toString")
    void testToString() {
        String str = "Human{name='"
                + father.getName()
                + "', surname='"
                + father.getSurname()
                + "', year="
                + father.getYear()
                + ", iq="
                + father.getIq()
                + ", schedule="
                + father.getSchedule().toString()
                + "}";
        assertEquals(str, father.toString());

    }

    @Test
    @DisplayName("father must equal new father")
    void testEquals() {
        Human anotherFather = new Man("Father", "Family", 2000, 148, null);
        anotherFather.setFamily(family);
        assertTrue(father.equals(anotherFather));
    }

    @Test
    @DisplayName("father must not equal new father")
    void testEquals_false () {
        Human anotherFather = new Man("Father", "Family", 2002, 148, schedule);
        assertFalse(father.equals(anotherFather));
    }

    @Test
    @DisplayName("father's hashCode must equals family father's")
    void testHashCode() {
        assertEquals(father.hashCode(), family.getFather().hashCode());
    }

    @Test
    @DisplayName("father's hashCode must not equals new father's")
    void testHashCode_false() {
        Human newFather = new Man("Father", "Family", 2000, 130, null);
        assertNotEquals(father.hashCode(), newFather.hashCode());
    }
}