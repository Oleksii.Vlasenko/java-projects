package ua.com.danit.homeworks.homework_9.dao;

import ua.com.danit.homeworks.homework_9.entity.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= this.families.size()) {
            return false;
        }
        return this.families.remove(index) != null;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return this.families.remove(family);
    }

    @Override
    public boolean saveFamily(Family family) {
        return this.families.add(family);
    }

    @Override
    public String toString() {
        return "families=" + families;
    }
}
