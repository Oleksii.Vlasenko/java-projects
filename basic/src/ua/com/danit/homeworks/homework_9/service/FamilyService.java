package ua.com.danit.homeworks.homework_9.service;

import ua.com.danit.homeworks.homework_9.dao.CollectionFamilyDao;
import ua.com.danit.homeworks.homework_9.entity.Family;
import ua.com.danit.homeworks.homework_9.dao.FamilyDao;
import ua.com.danit.homeworks.homework_9.entity.Human;
import ua.com.danit.homeworks.homework_9.entity.Pet;
import ua.com.danit.homeworks.homework_9.entity.humans.Man;
import ua.com.danit.homeworks.homework_9.entity.humans.Woman;

import java.util.*;

public class FamilyService {

    private final CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    private List<Family> getAllFamilies() {
        return collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        System.out.println(collectionFamilyDao.toString());
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        List<Family> resultList = new ArrayList<>();
        for (Family currFamily : this.collectionFamilyDao.getAllFamilies()) {
            int familyCount = 0;
            if (currFamily.getFather() != null) familyCount++;
            if (currFamily.getMother() != null) familyCount++;
            familyCount += currFamily.getChildren().size();
            if (familyCount > count) resultList.add(currFamily);
        }
        return resultList;
    }

    public List<Family> getFamiliesLessThan(int count) {
        List<Family> resultList = new ArrayList<>();
        for (Family currFamily : this.collectionFamilyDao.getAllFamilies()) {
            int familyCount = 0;
            if (currFamily.getFather() != null) familyCount++;
            if (currFamily.getMother() != null) familyCount++;
            familyCount += currFamily.getChildren().size();
            if (familyCount < count) resultList.add(currFamily);
        }
        return resultList;
    }

    public int countFamiliesWithMemberNumber(int count) {
        int families = 0;
        for (Family currFamily : this.collectionFamilyDao.getAllFamilies()) {
            int familyCount = 0;
            if (currFamily.getFather() != null) familyCount++;
            if (currFamily.getMother() != null) familyCount++;
            familyCount += currFamily.getChildren().size();
            if (familyCount == count) families++;
        }
        return families;
    }

    public boolean createNewFamily(Human mother, Human father) {
        return this.collectionFamilyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return this.collectionFamilyDao.deleteFamily(index);
    }

    public boolean bornChild(Family family, String mName, String fName) {

        if (family.getMother() == null || family.getFather() == null) {
            return false;
        }
        Random random = new Random();
        Human child;
        int year =  new Date().getYear();
        int iq = (family.getFather().getIq() + family.getMother().getIq()) / 2;
        if (random.nextInt(2) == 1) {
            String name = mName;
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            child = new Man(name, family.getFather().getSurname(), year, iq, null);
        } else {
            String name = fName;
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            child = new Woman(name, family.getFather().getSurname(), year, iq, null);
        }
        family.addChild(child);
        return true;
    }

    public boolean adoptChild(Family family, Human human) {
        boolean flag = false;
        for (Family currFamily : this.collectionFamilyDao.getAllFamilies()) {
            if (family.equals(currFamily)) {
                currFamily.addChild(human);
                flag = true;
                break;
            }
        }
        return flag;
    }

    public void deleteAllChildrenOlderThen(int age) {
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        for (Family family : collectionFamilyDao.getAllFamilies()) {
            List<Human> newChildrenList = new ArrayList<>();
            for (Human human : family.getChildren()) {
                if ((year - human.getYear()) > age) {
                    newChildrenList.add(human);
                }
            }
            for (int i = 0; i < newChildrenList.size(); i++) {
                family.deleteChild(newChildrenList.get(i));
            }
        }
    }

    public int count() {
        return this.collectionFamilyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return this.collectionFamilyDao.getFamilyByIndex(index);
    }

    public HashSet<Pet> getPets(int index) {
        return this.collectionFamilyDao.getFamilyByIndex(index).getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        this.collectionFamilyDao.getFamilyByIndex(familyIndex).addPet(pet);
    }

    public FamilyDao getFamilyDao() {
        return this.collectionFamilyDao;
    }
}
