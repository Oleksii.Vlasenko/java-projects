package ua.com.danit.homeworks.homework_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Tasks {

    String[][] schedule = new String[7][2];
    String[] WEEK_DAYS = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    Tasks() {
        this.schedule[0][0] = WEEK_DAYS[0];
        this.schedule[0][1] = "Go to the park.";
        this.schedule[1][0] = WEEK_DAYS[1];
        this.schedule[1][1] = "Go in for sports. Watch a film.";
        this.schedule[2][0] = WEEK_DAYS[2];
        this.schedule[2][1] = "Go to courses.";
        this.schedule[3][0] = WEEK_DAYS[3];
        this.schedule[3][1] = "Go in for sports. Watch a film.";
        this.schedule[4][0] = WEEK_DAYS[4];
        this.schedule[4][1] = "Go to courses.";
        this.schedule[5][0] = WEEK_DAYS[5];
        this.schedule[5][1] = "Go in for sports. Watch a film.";
        this.schedule[6][0] = WEEK_DAYS[6];
        this.schedule[6][1] = "Go to courses.";
    }

    public static void main(String[] args) throws IOException {
        Tasks tasks = new Tasks();
        tasks.showTask();
    }

    public void showTask() throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                System.out.print("Please, input the day of the week: ");
                String command = reader.readLine();
                int a = 3;
                String taskMessage = "Your tasks for ";
                switch (command.trim().toLowerCase().split(" ")[0]) {
                    case "sunday":
                        System.out.println(taskMessage + this.schedule[0][0] + ": " + this.schedule[0][1]);
                        break;
                    case "monday":
                        System.out.println(taskMessage + this.schedule[1][0] + ": " + this.schedule[1][1]);
                        break;
                    case "tuesday":
                        System.out.println(taskMessage + this.schedule[2][0] + ": " + this.schedule[2][1]);
                        break;
                    case "wednesday":
                        System.out.println(taskMessage + this.schedule[3][0] + ": " + this.schedule[3][1]);
                        break;
                    case "thursday":
                        System.out.println(taskMessage + this.schedule[4][0] + ": " + this.schedule[4][1]);
                        break;
                    case "friday":
                        System.out.println(taskMessage + this.schedule[5][0] + ": " + this.schedule[5][1]);
                        break;
                    case "saturday":
                        System.out.println(taskMessage + this.schedule[6][0] + ": " + this.schedule[6][1]);
                        break;
                    case "change":
                        this.changeSchedule(command.split(" ")[1]);
                        break;
                    case "reschedule":
                        this.changeSchedule(command.split(" ")[1]);
                        break;
                    case "exit":
                        return;
                    default:
                        throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }

    private void changeSchedule (String day) throws Exception {

        switch (day.toLowerCase()) {
            case "sunday":
                changeTaskMessage(day);
                this.schedule[0][1] = createTask();
                break;
            case "monday":
                changeTaskMessage(day);
                this.schedule[1][1] = createTask();
                break;
            case "tuesday":
                changeTaskMessage(day);
                this.schedule[2][1] = createTask();
                break;
            case "wednesday":
                changeTaskMessage(day);
                this.schedule[3][1] = createTask();
                break;
            case "thursday":
                changeTaskMessage(day);
                this.schedule[4][1] = createTask();
                break;
            case "friday":
                changeTaskMessage(day);
                this.schedule[5][1] = createTask();
                break;
            case "saturday":
                changeTaskMessage(day);
                this.schedule[6][1] = createTask();
                break;
            default:
                throw new Exception();
        }

    }

    public void changeTaskMessage(String str) {
        System.out.println("Please, input new tasks for " + str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase());
    }

    public String createTask() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
}
