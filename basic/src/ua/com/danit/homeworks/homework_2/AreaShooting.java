package ua.com.danit.homeworks.homework_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class AreaShooting {
    char[][] area = new char[5][5];
    Target target = new Target();

    public static void main(String[] args) {
        AreaShooting areaShooting = new AreaShooting();
        while(areaShooting.target.life > 0) {
            try {
                areaShooting.shot();
                System.out.println(areaShooting);
            } catch (Exception e) {
                System.out.println("It's not a correct value. Please, try again!");
            }
        }
        System.out.println("You have won!");
    }

    AreaShooting() {
        for (int i = 0; i < this.area.length; i++) {
            for (int j = 0; j < this.area.length; j++) {
                area[i][j] = '-';
            }
        }
    }

    private void shot() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Row: ");
        int i = Integer.parseInt(reader.readLine());
        if (i <= 0 || i > 5) {
            throw new Exception();
        }
        System.out.print("Col: ");
        int j = Integer.parseInt(reader.readLine());
        if (j <= 0 || j > 5) {
            throw new Exception();
        }
        int[] hit = {i - 1, j - 1};
        this.area[i - 1][j - 1] = this.target.checkShot(hit) ? 'X' : '*';
    }

    public String toString() {
        StringBuilder outputLine = new StringBuilder("0");
        for (int i = 0; i < this.area.length; i++) {
            outputLine.append(" | ").append(i + 1);
        }
        outputLine.append('\n');
        for (int i = 0; i < this.area.length; i++) {
            outputLine.append(i + 1);
            for (int j = 0; j < this.area.length; j++) {
                outputLine.append(" | ").append(this.area[i][j]);
            }
            outputLine.append('\n');
        }
        return outputLine.toString();
    }
}
