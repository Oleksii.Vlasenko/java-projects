package ua.com.danit.homeworks.homework_2;

import java.util.Arrays;
import java.util.Random;

public class Target {
    int life = 3;
    int[][] position = new int[3][3];

    Target() {
        Random random = new Random();
        if (random.nextInt(2) == 0) {
            int temp = random.nextInt(5);
            for (int i = 0; i < 3; i++) {
                this.position[i][0] = temp;
                this.position[i][2] = 1;
            }
            temp = random.nextInt(3);
            for (int i = 0; i < 3; i++) {
                this.position[i][1] = temp + i;
            }
        } else {
            int temp = random.nextInt(5);
            for (int i = 0; i < 3; i++) {
                this.position[i][1] = temp;
                this.position[i][2] = 1;
            }
            temp = random.nextInt(3);
            for (int i = 0; i < 3; i++) {
                this.position[i][0] = temp + i;
            }
        }
        System.out.println(Arrays.toString(this.position));
    }

    public boolean checkShot (int[] shot) {
        for (int i = 0; i < 3; i++) {
            if (this.position[i][0] == shot[0] && this.position[i][1] == shot[1] && this.position[i][2] == 1) {
                this.position[i][2] = 0;
                this.life--;
                return true;
            }
        }
        return false;
    }
}
