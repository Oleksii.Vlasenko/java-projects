package ua.com.danit.homeworks.homework_4;

import java.util.Arrays;

public class Pet {
    String species;
    String nickname;
    int age;
    int trickLevel;
    String[] habits;

    Pet() {
        this.species = "pet";
        this.nickname = "unknown";
        this.age = -1;
        this.trickLevel = 0;
        this.habits = null;
    }

    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.age = -1;
        this.trickLevel = 0;
        this.habits = null;
    }

    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я кушаю!)");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!)");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...)");
    }

    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.species).append("{nickname='").append(this.nickname).append("'");
        if (this.age != -1) {
            str.append(", age=").append(this.age);
        }
        if (this.trickLevel != -1) {
            str.append(", trickLevel=").append(this.trickLevel);
        }
        if (this.habits != null) {
            str.append(", habits=").append(Arrays.toString(this.habits));
        }
        str.append("}");
        return str.toString();
    }
}
