package ua.com.danit.homeworks.homework_4;

import java.util.Random;

public class Human {
    String name;
    String surname;
    int year;
    int iq;
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule;

    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = 0;
        this.pet = null;
        this.mother = null;
        this.father = null;
        this.schedule = null;
    }

    Human(String name, String surname, int year, Human father, Human mother) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.father = father;
        this.mother = mother;
        this.iq = 0;
        this.pet = null;
        this.schedule = null;
    }

    Human(String name, String surna100me, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        mother.pet = pet;
        father.pet = pet;
        this.schedule = schedule;
    }

    public void greetPet() {
        System.out.println("Привет, " + pet.nickname);
    }

    public void describePet() {
        String trickLevel = pet.trickLevel > 50 ? "очень хитрый" : "почти не хитрый";
        String message = "У меня есть " + pet.species + ", ему " + pet.age + " лет, он " + trickLevel + ".";
        System.out.println(message);
    }

    public boolean feedPet(boolean feed) {
        if (this.pet == null) {
            System.out.println("There are no pets!");
            return false;
        }
        if (!feed) {
            Random random = new Random();
            int randomFeed = random.nextInt(101);
            System.out.println(randomFeed < this.pet.trickLevel
                    ? "Хм... покормлю ка я " + this.pet.nickname + "."
                    : "Думаю, " + this.pet.nickname + " не голоден.");
            return this.pet.trickLevel > randomFeed;
        }
        System.out.println("Покормлю ка я " + this.pet.nickname + ".");
        return feed;
    }

    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Human{name='")
                .append(this.name)
                .append("', surname='")
                .append(this.surname)
                .append("', year=")
                .append(this.year);
        if (this.iq != 0) {
            str.append(", iq=").append(this.iq);
        }
        if (this.mother != null) {
            str.append(", mother=")
                    .append(this.mother.name)
                    .append(" ")
                    .append(this.mother.surname);
        }
        if (this.father != null) {
            str.append(", father=")
                    .append(this.father.name)
                    .append(" ")
                    .append(this.father.surname);
        }
        if (this.pet != null) {
            str.append(", pet=").append(this.pet.toString());
        }
        str.append("}");
        return str.toString();
    }
}
