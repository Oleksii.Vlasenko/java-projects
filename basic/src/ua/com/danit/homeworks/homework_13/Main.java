package ua.com.danit.homeworks.homework_13;

import ua.com.danit.homeworks.homework_13.application.ConsoleApp;

public class Main {
    public static void main(String[] args) {
        try {
            ConsoleApp.startApp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
