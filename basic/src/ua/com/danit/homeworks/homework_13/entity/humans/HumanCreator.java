package ua.com.danit.homeworks.homework_13.entity.humans;

import ua.com.danit.homeworks.homework_13.entity.Human;

public interface HumanCreator {
    Human bornChild(Human human) throws Exception;
}
