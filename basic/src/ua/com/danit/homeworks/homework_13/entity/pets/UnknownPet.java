package ua.com.danit.homeworks.homework_13.entity.pets;

import ua.com.danit.homeworks.homework_13.entity.Pet;

import java.io.Serializable;

public class UnknownPet extends Pet implements Serializable {

    private static final long serialVersionUID = 110910111511610596L;

    public UnknownPet() {
        super("UNKNOWN");
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я не знаю кто я, но соскучился!)");
    }
}
