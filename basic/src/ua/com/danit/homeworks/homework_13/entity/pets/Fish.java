package ua.com.danit.homeworks.homework_13.entity.pets;

import ua.com.danit.homeworks.homework_13.entity.Pet;

import java.io.Serializable;
import java.util.HashSet;

public class Fish extends Pet implements Serializable {

    private static final long serialVersionUID = 110910111511610598L;

    public Fish() {
        super("FISH");
    }

    public Fish(String nickname) {
        super("FISH", nickname);
    }

    public Fish(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super("FISH", nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Буль-буль!");
    }
}
