package ua.com.danit.homeworks.homework_13.entity.humans;

import ua.com.danit.homeworks.homework_13.entity.Human;

import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;

public final class Man extends Human implements Serializable {

    private static final long serialVersionUID = 110910111511610198L;

    public Man() {
        super();
    }

    public Man(String name, String surname, String birth) throws ParseException {
        super(name, surname, birth);
    }

    public Man(String name, String surname, String birth, int iq) throws ParseException {
        super(name, surname, birth, iq);
    }

    public Man(String name, String surname, String birth, int iq, HashMap<String, String> schedule) throws ParseException {
        super(name, surname, birth, iq, schedule);
    }

    public void repair() {
        System.out.println("I am repairing!");
    }

    @Override
    public void greetPet() {
        System.out.println("I am your owner!");
    }
}
