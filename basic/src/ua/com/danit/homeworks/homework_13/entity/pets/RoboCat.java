package ua.com.danit.homeworks.homework_13.entity.pets;

import ua.com.danit.homeworks.homework_13.entity.Pet;

import java.io.Serializable;
import java.util.HashSet;

public class RoboCat extends Pet implements Serializable {

    private static final long serialVersionUID = 110910111511610597L;

    public RoboCat() {
        super("ROBOCAT");
    }

    public RoboCat(String nickname) {
        super("ROBOCAT", nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super("ROBOCAT", nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!)");
    }
}
