package ua.com.danit.homeworks.homework_13.entity.pets;

import ua.com.danit.homeworks.homework_13.entity.Pet;

import java.io.Serializable;
import java.util.HashSet;

public class DomesticCat extends Pet implements Foul, Serializable {

    private static final long serialVersionUID = 110910111511610599L;

    public DomesticCat() {
        super("DOMESTICCAT");
    }

    public DomesticCat(String nickname) {
        super("DOMESTICCAT", nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super("DOMESTICCAT", nickname, age, trickLevel, habits);
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...)");
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!)");
    }
}
