package ua.com.danit.homeworks.homework_13.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

public abstract class Human implements Serializable {

    private static final long serialVersionUID = 220910111511670598L;

    private final String name;
    private final String surname;
    private final long birthDate;

    private final int iq;
    private final HashMap<String, String> schedule;
    private Family family;

    static {
        System.out.println("Download the Human class");
    }

    {
        System.out.println("Create the Human object");
    }

    public Human() {
        this.name = "Unknown";
        this.surname = "Unknown";
        this.birthDate = new Date().getTime();
        this.iq = 0;
        this.schedule = null;
    }

    public Human(String name, String surname, String birth) throws ParseException {
        this.name = name;
        this.surname = surname;
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(birth);
        this.birthDate = date.getTime();
        this.iq = 0;
        this.schedule = null;
    }

    public Human(String name, String surname, String birth, int iq) throws ParseException {
        this.name = name;
        this.surname = surname;
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(birth);
        this.birthDate = date.getTime();
        this.iq = iq;
        this.schedule = null;
    }

    public Human(String name, String surname, String birth, int iq, HashMap<String, String> schedule) throws ParseException {
        this.name = name;
        this.surname = surname;
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(birth);
        this.birthDate = date.getTime();
        this.iq = iq;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public int getIq() {
        return iq;
    }

    public HashMap<String, String> getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getFormatBirthDate() {
        Date date = new Date(this.birthDate);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return simpleDateFormat.format(date);
    }

    public void greetPet() {
        Iterator<Pet> iter = this.family.getPets().iterator();
        while(iter.hasNext()) {
            System.out.println("Привет, " + iter.next().getNickname());
        }
    }

    public void describePet() {
        for (Pet pet : this.family.getPets()) {
            String trickLevel = pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
            String message = "У меня есть "
                    + pet.getSpecies()
                    + ", ему "
                    + pet.getAge()
                    + " лет, он " + trickLevel
                    + ".";
            System.out.println(message);
        }
    }

    public String describeAge() {
        LocalDate birthDate = Instant.ofEpochMilli(this.birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currDate = Instant.ofEpochMilli(new Date().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        Period period = Period.between(birthDate, currDate);
        return period.getYears() + "-" + period.getMonths() + "-" + period.getDays();
    }

    public boolean feedPet(boolean feed) {
        if (this.family.getPets().size() == 0) {
            System.out.println("There are no pets!");
            return false;
        }
        boolean isFeed = true;
        if (!feed) {
            Random random = new Random();
            for (Pet pet : this.family.getPets()) {
                int randomFeed = random.nextInt(101);
                System.out.println(randomFeed < pet.getTrickLevel()
                        ? "Хм... покормлю ка я " + pet.getNickname() + "."
                        : "Думаю, " + pet.getNickname() + " не голоден.");
                isFeed = isFeed && pet.getTrickLevel() > randomFeed;
            }
            return isFeed;
        }
        Iterator<Pet> iter = this.family.getPets().iterator();
        while(iter.hasNext()) {
            System.out.println("Покормлю ка я " + iter.next().getNickname() + ".");
        }
        return true;
    }

    public String prettyFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String birthToString = simpleDateFormat.format(new Date(this.birthDate));
        StringBuilder str = new StringBuilder();
        str.append("{name='")
                .append(this.name)
                .append("', surname='")
                .append(this.surname)
                .append("', birthDate='")
                .append(birthToString)
                .append("', iq=")
                .append(this.iq)
                .append(", schedule=")
                .append(this.schedule)
                .append("}");
        return str.toString();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Human{name='")
                .append(this.name)
                .append("', surname='")
                .append(this.surname)
                .append("', birthDate=")
                .append(this.getFormatBirthDate());
        if (this.iq != 0) {
            str.append(", iq=").append(this.iq);
        }
        if (this.schedule != null) {
            str.append(", schedule=").append(this.schedule.toString());
        }
        str.append("}");
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                iq == human.iq;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq);
    }

    @Override
    public void finalize() {
        System.out.println("Human object deleted!");
    }
}
