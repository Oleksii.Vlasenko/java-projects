package ua.com.danit.homeworks.homework_13.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_13.dao.CollectionFamilyDao;
import ua.com.danit.homeworks.homework_13.entity.Family;
import ua.com.danit.homeworks.homework_13.entity.Human;
import ua.com.danit.homeworks.homework_13.entity.humans.Man;
import ua.com.danit.homeworks.homework_13.entity.humans.Woman;
import ua.com.danit.homeworks.homework_13.entity.pets.Dog;
import ua.com.danit.homeworks.homework_13.entity.pets.DomesticCat;
import ua.com.danit.homeworks.homework_13.service.FamilyService;
import ua.com.danit.homeworks.homework_13.service.LoggerService;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {

    static FamilyService familyService;

    @BeforeAll
    static void initFamilyService() {
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        familyService = new FamilyService(collectionFamilyDao);
        familyService.createNewFamily(new Woman(), new Man());
    }

    @Test
    void getFamiliesBiggerThan() {
        List<Family> list = new ArrayList<>();
        assertEquals(list, familyService.getFamiliesBiggerThan(3));
    }

    @Test
    void getFamiliesLessThan() {
        int beforeCount = familyService.getFamiliesLessThan(3).size();
        familyService.createNewFamily(new Woman(), new Man());
        assertEquals(beforeCount + 1, familyService.getFamiliesLessThan(3).size());
    }

    @Test
    void countFamiliesWithMemberNumber() {
        int beforeCount = familyService.countFamiliesWithMemberNumber(2);
        familyService.createNewFamily(new Woman(), new Man());
        assertEquals(beforeCount + 1, familyService.countFamiliesWithMemberNumber(2));
    }

    @Test
    void createNewFamily() {
        int beforeCount = familyService.count();
        familyService.createNewFamily(new Woman(), new Man());
        assertEquals(beforeCount + 1, familyService.count());
    }

    @Test
    void deleteFamilyByIndex() {
        familyService.deleteFamilyByIndex(1);
        assertEquals(1, familyService.count());
    }

    @Test
    void bornChild() throws ParseException {
        familyService.createNewFamily(new Woman(), new Man());
        familyService.bornChild(familyService.getFamilyById(1), "Adam", "Ada");
        assertEquals(1, familyService.countFamiliesWithMemberNumber(3));
    }

    @Test
    void adoptChild() throws ParseException {
        int beforeCount = familyService.countFamiliesWithMemberNumber(3);
        Human child = new Man("Boris", "OldSurname", "11/05/2000", 140, null);
        familyService.adoptChild(familyService.getFamilyById(0), child);
        assertEquals(beforeCount + 1, familyService.countFamiliesWithMemberNumber(3));
    }

    @Test
    void deleteAllChildrenOlderThen() throws ParseException {
        Human child = new Man("Child", "Childenko", "11/05/2000", 140, null);
        familyService.adoptChild(familyService.getFamilyById(0), child);
        int beforeCount = familyService.countFamiliesWithMemberNumber(3);
        familyService.deleteAllChildrenOlderThen(9);
        assertEquals(beforeCount - 1, familyService.countFamiliesWithMemberNumber(3));
    }

    @Test
    void count() {
        int beforeCount = familyService.count();
        familyService.createNewFamily(new Woman(), new Man());
        familyService.createNewFamily(new Woman(), new Man());
        assertEquals(beforeCount + 2, familyService.count());
    }

    @Test
    void getFamilyById() throws ParseException {
        familyService.createNewFamily(new Woman("Ada", "Adanenko", "11/05/2000"), new Man());
        assertEquals(957992400000L, familyService.getFamilyById(familyService.count() - 1).getMother().getBirthDate());
    }

    @Test
    void getPets() {
        int beforeCount = familyService.getPets(0).size();
        familyService.getFamilyById(0).addPet(new DomesticCat());
        assertEquals(beforeCount + 1, familyService.getPets(0).size());
    }

    @Test
    void addPet() {
        int beforeCount = familyService.getPets(0).size();
        familyService.addPet(0, new Dog());
        assertEquals(beforeCount + 1, familyService.getPets(0).size());
    }

    @Test
    void getFamilyDao() {
        assertTrue(familyService.getFamilyDao() instanceof CollectionFamilyDao);
    }

    @Test
    void loadData() throws ParseException, IOException {
        Human mother = new Woman("Ada", "Adamenko", "02/02/2000", 150);
        Human father = new Man("Adam", "Adamenko", "02/02/2000", 150);
        familyService.createNewFamily(mother, father);
        assertTrue(familyService.loadData());
    }

    @Test
    void uploadData() throws ParseException {
        assertTrue(familyService.uploadFamilies());
    }
}