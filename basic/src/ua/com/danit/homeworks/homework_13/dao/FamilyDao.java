package ua.com.danit.homeworks.homework_13.dao;

import ua.com.danit.homeworks.homework_13.entity.Family;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    boolean saveFamily(Family family);

    boolean loadData() throws IOException;
}
