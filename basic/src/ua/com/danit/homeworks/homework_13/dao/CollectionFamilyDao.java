package ua.com.danit.homeworks.homework_13.dao;

import ua.com.danit.homeworks.homework_13.entity.Family;
import ua.com.danit.homeworks.homework_13.service.LoggerService;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        LoggerService.info("getting all families");
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        LoggerService.info("getting family by index (" + index + ")");
        return this.families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= this.families.size()) {
            return false;
        }
        LoggerService.info("deleting family by index (" + index + ")");
        return this.families.remove(index) != null;
    }

    @Override
    public boolean deleteFamily(Family family) {
        LoggerService.info("deleting family (" + family.getFather().getSurname() + ")");
        return this.families.remove(family);
    }

    @Override
    public boolean saveFamily(Family family) {
        LoggerService.info("saving family (" + family.getFather().getSurname() + ")");
        return this.families.add(family);
    }

    @Override
    public boolean loadData() throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("families.data"));
        this.families.forEach(family -> {
            try {
                objectOutputStream.writeObject(family);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        objectOutputStream.close();
        LoggerService.info("loading data from base");
        return true;
    }

    @Override
    public String toString() {
        LoggerService.info("return families info");
        return "families=" + families;
    }
}
