package ua.com.danit.homeworks.homework_7.humans;

import ua.com.danit.homeworks.homework_7.Human;

import java.util.Calendar;
import java.util.Random;

public final class Man extends Human implements HumanCreator{
    public Man() {
        super();
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void repair() {
        System.out.println("I am repairing!");
    }

    @Override
    public void greetPet() {
        System.out.println("I am your owner!");
    }

    @Override
    public Human bornChild(Human human) throws Exception {
        if (this.getFamily() == null && human.getFamily() == null) {
            throw new Exception("You should get married before!");
        }
        if (this.getFamily().hashCode() != human.getFamily().hashCode()) {
            throw new Exception("You should be ashamed!");
        }
        if (!(human instanceof Woman)) {
            throw new Exception("You need woman to born a child!");
        }
        Random random = new Random();
        Human child;
        int year =  Calendar.getInstance().get(Calendar.YEAR);
        int iq = (this.getIq() + human.getIq()) / 2;
        if (random.nextInt(2) == 1) {
            int nameIndex = random.nextInt(ManName.values().length);
            String name = ManName.values()[nameIndex].toString();
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            child = new Man(name, this.getSurname(), year, iq, null);
        } else {
            int nameIndex = random.nextInt(WomanName.values().length);
            String name = WomanName.values()[nameIndex].toString();
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            child = new Woman(name, this.getSurname(), year, iq, null);
        }
        this.getFamily().addChild(child);
        return child;
    }

}
