package ua.com.danit.homeworks.homework_7.humans;

public enum ManName {
    AARON,
    BADGER,
    CALVIN,
    DALE,
    EADMUND,
    FABIAN,
    GABRIEL,
    HANK,
    IAN,
    JACK,
    KARL,
    LAMBERT,
    MABEL,
    NATHANIEL,
    OLIVER,
    PADDY,
    QUENTIN,
    RALF,
    SAM,
    TEDDY,
    UPTON,
    WADDELL,
    XAVIER,
    YOUNG,
    ZACHARY;
}
