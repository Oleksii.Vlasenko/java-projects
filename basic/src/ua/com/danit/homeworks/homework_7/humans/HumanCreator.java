package ua.com.danit.homeworks.homework_7.humans;

import ua.com.danit.homeworks.homework_7.Human;

public interface HumanCreator {
    Human bornChild(Human human) throws Exception;
}
