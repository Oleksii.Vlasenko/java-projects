package ua.com.danit.homeworks.homework_7.humans;

public enum WomanName {
    ADA,
    BARBARA,
    CAITLIN,
    DAISY,
    EDITH,
    FAITH,
    GABRIELLA,
    HANNA,
    IDA,
    JACKLYN,
    KAREN,
    LAURA,
    MAGGIE,
    NANCY,
    OCTAVIA,
    PAMELA,
    RACHEL,
    SABINA,
    TABITHA,
    ULRICA,
    VALERIE,
    WANDA,
    YOLANDA,
    ZOE;
}
