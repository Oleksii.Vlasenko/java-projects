package ua.com.danit.homeworks.homework_7;

import ua.com.danit.homeworks.homework_7.pets.Species;

import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;
    private Family family;

    static {
        System.out.println("Download the Pet class");
    }

    {
        System.out.println("Create the Pet object");
    }

    public Pet(String species) {
        this.species = Species.valueOf(species);
        this.nickname = "unknown";
        this.age = -1;
        this.trickLevel = 0;
        this.habits = null;
    }

    public Pet(String species, String nickname) {
        this.species = Species.valueOf(species);
        this.nickname = nickname;
        this.age = -1;
        this.trickLevel = 0;
        this.habits = null;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = Species.valueOf(species);
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String getSpecies() {
        return this.species.toString();
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public int getAge() {
        return this.age;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void eat() {
        System.out.println("Я кушаю!)");
    }

    public abstract void respond();

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.species).append("{nickname='").append(this.nickname).append("'");
        if (this.age != -1) {
            str.append(", age=").append(this.age);
        }
        if (this.trickLevel != -1) {
            str.append(", trickLevel=").append(this.trickLevel);
        }
        if (this.habits != null) {
            str.append(", habits=").append(Arrays.toString(this.habits));
        }
        str.append(", ").append(this.species.canFly ? "can fly" : "can't fly");
        str.append(", has ").append(this.species.numberOfLegs).append(" legs");
        str.append(", ").append(this.species.hasFur ? "has fur" : "hasn't fur");
        str.append("}");
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname) &&
                Objects.equals(age, pet.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }

    @Override
    public void finalize() {
        System.out.println("Pet object deleted!");
    }
}
