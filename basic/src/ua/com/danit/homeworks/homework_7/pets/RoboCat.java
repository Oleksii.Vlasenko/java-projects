package ua.com.danit.homeworks.homework_7.pets;

import ua.com.danit.homeworks.homework_7.Pet;

public class RoboCat extends Pet {
    public RoboCat() {
        super("ROBOCAT");
    }

    public RoboCat(String nickname) {
        super("ROBOCAT", nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super("ROBOCAT", nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!)");
    }
}
