package ua.com.danit.homeworks.homework_7.pets;

import ua.com.danit.homeworks.homework_7.Pet;

public class DomesticCat extends Pet implements Foul {

    public DomesticCat() {
        super("DOMESTICCAT");
    }

    public DomesticCat(String nickname) {
        super("DOMESTICCAT", nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super("DOMESTICCAT", nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!)");
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...)");
    }
}
