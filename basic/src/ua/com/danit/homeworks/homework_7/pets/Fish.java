package ua.com.danit.homeworks.homework_7.pets;

import ua.com.danit.homeworks.homework_7.Pet;

public class Fish extends Pet {

    public Fish() {
        super("FISH");
    }

    public Fish(String nickname) {
        super("FISH", nickname);
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super("FISH", nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Буль-буль!");
    }
}
