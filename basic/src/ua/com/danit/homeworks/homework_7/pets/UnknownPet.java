package ua.com.danit.homeworks.homework_7.pets;

import ua.com.danit.homeworks.homework_7.Pet;

public class UnknownPet extends Pet {
    public UnknownPet() {
        super("UNKNOWN");
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я не знаю кто я, но соскучился!)");
    }
}
