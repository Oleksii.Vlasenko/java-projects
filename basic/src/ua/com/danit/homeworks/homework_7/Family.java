package ua.com.danit.homeworks.homework_7;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    private Human mother;
    private Human father;

    public Human[] getChildren() {
        return children;
    }

    private Human[] children = new Human[0];
    private Pet pet;

    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return father;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    static {
        System.out.println("Download the Family class");
    }

    {
        System.out.println("Create the Family object");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.pet.setFamily(this);
    }

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] temp = new Human[this.children.length + 1];
        System.arraycopy(this.children, 0, temp, 0, this.children.length);
        temp[temp.length - 1] = child;
        this.children = temp;
    }

    public void deleteChild(Human child) {
        if (this.children.length > 0) {
            Human[] temp = new Human[this.children.length - 1];
            int index = this.children.length - 1;
            int j = 0;
            for (int i = 0; i < temp.length; i++) {
                if (!this.children[i].equals(child)) {
                    temp[i] = this.children[j];
                } else {
                    index = i;
                    j++;
                }
                j++;
            }
            if (this.children[index].equals(child)) {
                this.children = temp;
                child.setFamily(null);
                System.out.println("Delete successfully!");
            } else {
                System.out.println("Wrong child to remove!");
            }
        } else {
            System.out.println("There is no any child!");
        }
    }

    public void deleteChild(int index) {
        if (this.children.length <= index) {
            return;
        }
        Human[] temp = new Human[this.children.length - 1];
        for (int i = 0; i < this.children.length; i++) {
            if (i < index) {
                temp[i] = this.children[i];
            }
            if (i > index) {
                temp[i - 1] = this.children[i];
            }
        }
        this.children = temp;
    }

    public int countFamily() {
        int count = 0;
        if (this.mother != null) {
            count++;
        }
        if (this.father != null) {
            count++;
        }
        for (Human child : children) {
            if (child != null) {
                count++;
            }
        }
        return count;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        if (this.mother != null) {
            str.append(this.mother.toString());
        }
        if (this.father != null) {
            str.append('\n').append(this.father.toString());
        }
        if (this.children != null && this.children.length > 0) {
            for (Human child : this.children) {
                str.append('\n').append(child.toString());
            }
        }
        if (this.pet != null) {
            str.append('\n').append(this.pet.toString());
        }
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public void finalize() {
        System.out.println("Family object deleted!");
    }
}
