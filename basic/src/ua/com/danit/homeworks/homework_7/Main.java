package ua.com.danit.homeworks.homework_7;

import ua.com.danit.homeworks.homework_7.humans.Man;
import ua.com.danit.homeworks.homework_7.humans.Woman;
import ua.com.danit.homeworks.homework_7.pets.Dog;
import ua.com.danit.homeworks.homework_7.pets.DomesticCat;
import ua.com.danit.homeworks.homework_7.pets.UnknownPet;

public class Main {
    public static void main(String[] args) {
        try {
            String[][] schedule_1 = {
                    {DayOfWeek.MONDAY.name(), "Education"},
                    {DayOfWeek.TUESDAY.name(), "Sport"},
                    {DayOfWeek.WEDNESDAY.name(), "Education"},
                    {DayOfWeek.THURSDAY.name(), "Sport"},
                    {DayOfWeek.FRIDAY.name(), "Education"},
                    {DayOfWeek.SATURDAY.name(), "Sport"},
                    {DayOfWeek.SUNDAY.name(), "Rest"}
            };

            UnknownPet unknownPet = new UnknownPet();
            unknownPet.eat();
            System.out.println(unknownPet.toString());

            String[] dogHabits = {"sleep", "jump", "run"};
            Dog dog = new Dog("Doggy", 1, 40, dogHabits);
            dog.respond();

            Woman mother_1 = new Woman("Mother", "Family_1", 1980, 148, schedule_1);
            Man father_1 = new Man("Father", "Family_1", 1976, 146, schedule_1);
            Family family1 = new Family(mother_1, father_1, dog);

            Human child_1;
            child_1 = father_1.bornChild(mother_1);

            mother_1.greetPet();
            father_1.greetPet();

            child_1.describePet();
            child_1.feedPet(true);

            System.out.println(child_1.getFamily().getPet().toString());

            mother_1.feedPet(false);
            mother_1.cook();
            father_1.repair();

            System.out.println(mother_1.toString());
            System.out.println(father_1.toString());
            System.out.println(child_1.toString());
            System.out.println(dog.toString());

            DomesticCat cat = new DomesticCat("Catty");
            Woman mother_2 = new Woman("Mother", "Family_2", 1988, 136, schedule_1);
            Man father_2 = new Man("Father", "Family_2", 1984, 142, schedule_1);

            Family family2 = new Family(mother_2, father_2, cat);
            Human child_2 = father_2.bornChild(mother_2);

            System.out.println(mother_2.toString());
            System.out.println(father_2.toString());
            System.out.println(child_2.toString());
            System.out.println(cat.toString());

            child_2.feedPet(false);

            family2.deleteChild(child_2);
            family1.deleteChild(child_1);
            System.out.println(family1.toString());

//            for (int i = 0; i < 1000000; i++) {
//                Human human = new Man();
//            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
