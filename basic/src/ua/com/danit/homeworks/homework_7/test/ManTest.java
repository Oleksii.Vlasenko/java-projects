package ua.com.danit.homeworks.homework_7.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_7.DayOfWeek;
import ua.com.danit.homeworks.homework_7.Family;
import ua.com.danit.homeworks.homework_7.Human;
import ua.com.danit.homeworks.homework_7.Pet;
import ua.com.danit.homeworks.homework_7.humans.Man;
import ua.com.danit.homeworks.homework_7.humans.Woman;
import ua.com.danit.homeworks.homework_7.pets.Dog;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ManTest {
    private static Family family;
    private static Human mother;
    private static Human father;
    private static Pet pet;
    private static String[][] schedule;

    @BeforeAll
    public static void initHuman() {
        schedule = new String[][]{
                {DayOfWeek.MONDAY.name(), "Education"},
                {DayOfWeek.TUESDAY.name(), "Sport"},
                {DayOfWeek.WEDNESDAY.name(), "Education"},
                {DayOfWeek.THURSDAY.name(), "Sport"},
                {DayOfWeek.FRIDAY.name(), "Education"},
                {DayOfWeek.SATURDAY.name(), "Sport"},
                {DayOfWeek.SUNDAY.name(), "Rest"}
        };
        mother = new Woman("Mother", "Family", 2000, 148, schedule);
        father = new Man("Father", "Family", 2000, 148, schedule);
        pet = new Dog("BILL", 5, 0, null);
        family = new Family(mother, father, pet);
    }

    @Test
    @DisplayName("Return human's name")
    void getName() {
        assertEquals("Father", father.getName());
    }

    @Test
    @DisplayName("Return human's surname")
    void getSurname() {
        assertEquals("Family", father.getSurname());
    }

    @Test
    @DisplayName("Return human's year")
    void getYear() {
        assertEquals(2000, father.getYear());
    }

    @Test
    @DisplayName("Return human's iq")
    void getIq() {
        assertEquals(148, father.getIq());
    }

    @Test
    @DisplayName("Return human's default iq")
    void getDefaultIq() {
        assertEquals(148, father.getIq());
    }

    @Test
    @DisplayName("Return human's correct schedule")
    void getSchedule() {
        assertArrayEquals(schedule, father.getSchedule());
    }

    @Test
    @DisplayName("Return human's default schedule")
    void getDefaultSchedule() {
        assertArrayEquals(schedule, father.getSchedule());
    }

    @Test
    @DisplayName("family equals father.family")
    void getFamily() {
        assertEquals(null, father.getFamily());
    }

    @Test
    @DisplayName("father.family equals mother.family")
    void getFamilyByFamilyMembers() {
        assertEquals(father.getFamily(), mother.getFamily());
    }

    @Test
    @DisplayName("change father.family - null")
    void setFamily() {
        father.setFamily(null);
        assertEquals(null, father.getFamily());
    }

    @Test
    @DisplayName("feed pet")
    void feedPet() {
        assertTrue(father.feedPet(true));
    }

    @Test
    @DisplayName("feed pet - false")
    void dontFeedPet() {
        assertFalse(father.feedPet(false));
    }

    @Test
    @DisplayName("must toString")
    void testToString() {
        String str = "Human{name='"
                + father.getName()
                + "', surname='"
                + father.getSurname()
                + "', year="
                + father.getYear()
                + ", iq="
                + father.getIq()
                + ", schedule="
                + Arrays.deepToString(father.getSchedule())
                + "}";
        assertEquals(str, father.toString());

    }

    @Test
    @DisplayName("father must equal new father")
    void testEquals() {
        Human anotherFather = new Man("Father", "Family", 2000, 148, null);
        anotherFather.setFamily(family);
        assertTrue(father.equals(anotherFather));
    }

    @Test
    @DisplayName("father must not equal new father")
    void testEquals_false () {
        Human anotherFather = new Man("Father", "Family", 2002, 148, schedule);
        assertFalse(father.equals(anotherFather));
    }

    @Test
    @DisplayName("father's hashCode must equals family father's")
    void testHashCode() {
        assertEquals(father.hashCode(), family.getFather().hashCode());
    }

    @Test
    @DisplayName("father's hashCode must not equals new father's")
    void testHashCode_false() {
        Human newFather = new Man("Father", "Family", 2000, 130, null);
        assertNotEquals(father.hashCode(), newFather.hashCode());
    }
}