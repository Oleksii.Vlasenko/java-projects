package ua.com.danit.homeworks.homework_7.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_7.Family;
import ua.com.danit.homeworks.homework_7.Human;
import ua.com.danit.homeworks.homework_7.humans.Man;
import ua.com.danit.homeworks.homework_7.humans.Woman;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    private static Family family;
    private static Woman mother;
    private static Man father;
    private static Human child_1;
    private static Human child_2;
    private static Human child_3;
    private static Human child_4;

    @BeforeAll
    public static void initFamily() throws Exception {
        mother = new Woman();
        father = new Man();
        family = new Family(mother, father);
        child_1 = father.bornChild(mother);
        child_2 = father.bornChild(mother);
        child_3 = mother.bornChild(father);
        child_4 = mother.bornChild(father);
    }

    @Test
    @DisplayName("Must added 1 child")
    public void addChild_true() {
        int beforeLength = family.getChildren().length;
        family.addChild(child_4);
        assertEquals(beforeLength + 1, family.getChildren().length);
    }

    @Test
    @DisplayName("Must not deleted child by index")
    void deleteChildByIndex_false() {
        family.deleteChild(1);
        assertNotEquals(3, family.getChildren().length);
    }

    @Test
    @DisplayName("Must not deleted child")
    void deleteChildByChild_false() throws Exception {
        Human child_5 = father.bornChild(mother);
        family.deleteChild(child_5);
        assertNotEquals(3, family.getChildren().length);
    }

    @Test
    @DisplayName("must deleted child by index")
    void deleteChildByIndex() {
        int beforeLength = family.getChildren().length;
        family.deleteChild(1);
        assertEquals(beforeLength - 1, family.getChildren().length);
    }

    @Test
    @DisplayName("must deleted child")
    void deleteChildByChild() throws Exception {
        Woman mother_1 = new Woman();
        Man father_1 = new Man();
        Family family_1 = new Family(mother_1, father_1);
        Human newChild_1 = father_1.bornChild(mother_1);
        Human newChild_2 = father_1.bornChild(mother_1);
        int beforeLength = family_1.getChildren().length;
        family_1.deleteChild(newChild_1);
        assertEquals(beforeLength - 1, family_1.getChildren().length);
    }


    @Test
    @DisplayName("Must added 2 family members")
    void countFamily() throws Exception {
        int beforeCount = family.countFamily();
        Human newChild_1 = father.bornChild(mother);
        Human newChild_2 = father.bornChild(mother);
        assertEquals(beforeCount + 2, family.countFamily());
    }

    @Test
    @DisplayName("must to String")
    void testToString() {
        Human mother_1 = new Woman("Mother_1", "Family_1", 2000);
        Human father_1 = new Man("Father_1", "Family_1", 2000);
        Family family_1 = new Family(mother_1, father_1);
        String str = mother_1.toString() + '\n' + father_1.toString();
        assertEquals(str, family_1.toString());
    }

    @Test
    @DisplayName("family must equal another family")
    void testEquals() {
        assertTrue(new Family(new Woman(), new Man()).equals(new Family(new Woman(), new Man())));
    }

    @Test
    @DisplayName("family must not equal new family")
    void testEquals_false () {
        Human mother_1 = new Woman("Mother", "Family", 2000, 148, null);
        assertFalse(new Family(mother_1, new Man()).equals(new Family(new Woman(), new Man())));
    }

    @Test
    @DisplayName("mother's hashCode must equals family mother's")
    void testHashCode() {
        assertEquals(family.hashCode(), mother.getFamily().hashCode());
    }

    @Test
    @DisplayName("family's hashCode must not equals another family's")
    void testHashCode_false() {
        Human mother_1 = new Woman();
        Human father_1 = new Man("Father_1", "Family_1", 1990);
        Family family_1 = new Family(mother_1, father_1);
        Human mother_2 = new Woman();
        Human father_2 = new Man();
        Family family_2 = new Family(mother_2, father_2);
        assertNotEquals(family_1.hashCode(), family_2.hashCode());
    }

    @Test
    @DisplayName("check items after del child")
    void testDeleteChildItems() throws Exception {
        Woman testMother = new Woman();
        Man testFather = new Man();
        Family testFamily = new Family(testMother, testFather);
        testFather.bornChild(testMother);
        testFather.bornChild(testMother);
        testFather.bornChild(testMother);
        testFather.bornChild(testMother);
        Human lastChild = testFather.bornChild(testMother);

        testFamily.deleteChild(3);
        boolean isContainLastItem = false;
        for (Human child : testFamily.getChildren()) {
            isContainLastItem = isContainLastItem || child.equals(lastChild);
        }
        assertTrue(isContainLastItem);
    }
}