package ua.com.danit.homeworks.homework_7.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_7.DayOfWeek;
import ua.com.danit.homeworks.homework_7.Family;
import ua.com.danit.homeworks.homework_7.Human;
import ua.com.danit.homeworks.homework_7.Pet;
import ua.com.danit.homeworks.homework_7.humans.Man;
import ua.com.danit.homeworks.homework_7.humans.Woman;
import ua.com.danit.homeworks.homework_7.pets.Dog;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class WomanTest {
    private static Family family;
    private static Human mother;
    private static Human father;
    private static Pet pet;
    private static String[][] schedule;

    @BeforeAll
    public static void initHuman() {
        schedule = new String[][]{
                {DayOfWeek.MONDAY.name(), "Education"},
                {DayOfWeek.TUESDAY.name(), "Sport"},
                {DayOfWeek.WEDNESDAY.name(), "Education"},
                {DayOfWeek.THURSDAY.name(), "Sport"},
                {DayOfWeek.FRIDAY.name(), "Education"},
                {DayOfWeek.SATURDAY.name(), "Sport"},
                {DayOfWeek.SUNDAY.name(), "Rest"}
        };
        mother = new Woman("Mother", "Family", 2000, 148, schedule);
        father = new Man("Father", "Family", 2000, 148, schedule);
        pet = new Dog("BILL", 5, 0, null);
        family = new Family(mother, father, pet);
    }

    @Test
    @DisplayName("Return human's name")
    void getName() {
        assertEquals("Mother", mother.getName());
    }

    @Test
    @DisplayName("Return human's surname")
    void getSurname() {
        assertEquals("Family", mother.getSurname());
    }

    @Test
    @DisplayName("Return human's year")
    void getYear() {
        assertEquals(2000, mother.getYear());
    }

    @Test
    @DisplayName("Return human's iq")
    void getIq() {
        assertEquals(148, mother.getIq());
    }

    @Test
    @DisplayName("Return human's default iq")
    void getDefaultIq() {
        assertEquals(148, mother.getIq());
    }

    @Test
    @DisplayName("Return human's correct schedule")
    void getSchedule() {
        assertArrayEquals(schedule, mother.getSchedule());
    }

    @Test
    @DisplayName("Return human's default schedule")
    void getDefaultSchedule() {
        assertArrayEquals(schedule, mother.getSchedule());
    }

    @Test
    @DisplayName("family equals mother.family")
    void getFamily() {
        assertEquals(null, mother.getFamily());
    }

    @Test
    @DisplayName("father.family equals mother.family")
    void getFamilyByFamilyMembers() {
        assertEquals(father.getFamily(), mother.getFamily());
    }

    @Test
    @DisplayName("change mother.family - null")
    void setFamily() {
        mother.setFamily(null);
        assertEquals(null, mother.getFamily());
    }

    @Test
    @DisplayName("feed pet")
    void feedPet() {
        assertTrue(mother.feedPet(true));
    }

    @Test
    @DisplayName("feed pet - false")
    void dontFeedPet() {
        assertFalse(mother.feedPet(false));
    }

    @Test
    @DisplayName("must toString")
    void testToString() {
        String str = "Human{name='"
                + mother.getName()
                + "', surname='"
                + mother.getSurname()
                + "', year="
                + mother.getYear()
                + ", iq="
                + mother.getIq()
                + ", schedule="
                + Arrays.deepToString(mother.getSchedule())
                + "}";
        assertEquals(str, mother.toString());

    }

    @Test
    @DisplayName("mother must equal new mother")
    void testEquals() {
        Human anotherMother = new Woman("Mother", "Family", 2000, 148, null);
        anotherMother.setFamily(family);
        assertTrue(mother.equals(anotherMother));
    }

    @Test
    @DisplayName("mother must not equal new mother")
    void testEquals_false () {
        Human anotherMother = new Woman("Mother", "Family", 2002, 148, schedule);
        assertFalse(mother.equals(anotherMother));
    }

    @Test
    @DisplayName("mother's hashCode must equals family mother's")
    void testHashCode() {
        assertEquals(mother.hashCode(), family.getMother().hashCode());
    }

    @Test
    @DisplayName("mother's hashCode must not equals new mother's")
    void testHashCode_false() {
        Human newFather = new Man("Mother", "Family", 2000, 130, null);
        assertNotEquals(father.hashCode(), newFather.hashCode());
    }
}