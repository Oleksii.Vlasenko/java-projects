package ua.com.danit.homeworks.homework_7.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_7.Family;
import ua.com.danit.homeworks.homework_7.Human;
import ua.com.danit.homeworks.homework_7.Pet;
import ua.com.danit.homeworks.homework_7.humans.Man;
import ua.com.danit.homeworks.homework_7.humans.Woman;
import ua.com.danit.homeworks.homework_7.pets.DomesticCat;

import static org.junit.jupiter.api.Assertions.*;

class DomesticCatTest {
    private static Family family;
    private static Human mother;
    private static Human father;
    private static Pet pet;
    private static Family family_2;
    private static Human mother_2;
    private static Human father_2;

    @BeforeAll
    public static void initHuman() {
        mother = new Woman("Mother", "Family", 2000);
        father = new Man("Father", "Family", 2000);
        pet = new DomesticCat("BILL");
        family = new Family(mother, father, pet);

        mother_2 = new Woman();
        father_2 = new Man();
        family_2 = new Family(mother_2, father_2);
    }

    @Test
    @DisplayName("must get family")
    void getFamily() {
        assertEquals(family_2, pet.getFamily());
    }

    @Test
    @DisplayName("must set family")
    void setFamily() {
        pet.setFamily(family_2);
        assertEquals(family_2, pet.getFamily());
    }

    @Test
    @DisplayName("must set nickname")
    void setNickname() {
        pet.setNickname("WILLY");
        assertEquals("WILLY", pet.getNickname());
    }

    @Test
    @DisplayName("must set new age")
    void setAge() {
        pet.setAge(5);
        assertEquals(5, pet.getAge());
    }

    @Test
    @DisplayName("must set trick level")
    void setTrickLevel() {
        pet.setTrickLevel(70);
        assertEquals(70, pet.getTrickLevel());
    }

    @Test
    @DisplayName("must get habits")
    void getHabits() {
        String[] habits = {"sleep", "jump", "run"};
        assertArrayEquals(habits, pet.getHabits());
    }

    @Test
    @DisplayName("must set habits")
    void setHabits() {
        String[] habits = {"sleep", "jump", "run"};
        pet.setHabits(habits);
        assertArrayEquals(habits, pet.getHabits());
    }

    @Test
    @DisplayName("must get species")
    void getSpecies() {
        assertEquals("DOMESTICCAT", pet.getSpecies());
    }

    @Test
    @DisplayName("must get nickname")
    void getNickname() {
        assertEquals("BILL", pet.getNickname());
    }

    @Test
    @DisplayName("must get trick level")
    void getTrickLevel() {
        assertEquals(70, pet.getTrickLevel());
    }

    @Test
    @DisplayName("must get age")
    void getAge() {
        assertEquals(-1, pet.getAge());
    }

    @Test
    @DisplayName("must get pet.toString")
    void testToString() {
        String str = pet.getSpecies() + "{nickname='" + pet.getNickname() + "', trickLevel=" + pet.getTrickLevel();
        str += ", can't fly, has 4 legs, has fur}";
        assertEquals(str, pet.toString());
    }

    @Test
    @DisplayName("pet must equals new pet")
    void testEquals() {
        Pet newPet = new DomesticCat("BILL");
        newPet.setAge(5);
        newPet.setFamily(family_2);
        assertEquals(pet, newPet);
    }

    @Test
    @DisplayName("pet must not equals new pet")
    void testEquals_false() {
        Pet newPet = new DomesticCat("BILL");
        newPet.setAge(5);
        newPet.setFamily(family);
        assertFalse(pet.equals(newPet));
    }

    @Test
    @DisplayName("pet's hashCode must equals family pet's")
    void testHashCode() {
        assertEquals(pet.hashCode(), family.getPet().hashCode());
    }

    @Test
    @DisplayName("pet's hashCode must not equals new pet's")
    void testHashCode_false() {
        Pet newPet = new DomesticCat("BILL");
        newPet.setAge(4);
        newPet.setFamily(family);
        assertNotEquals(pet.hashCode(), newPet.hashCode());
    }
}