package ua.com.danit.homeworks.homework_10.controller;

import ua.com.danit.homeworks.homework_10.dao.FamilyDao;
import ua.com.danit.homeworks.homework_10.entity.Family;
import ua.com.danit.homeworks.homework_10.entity.Human;
import ua.com.danit.homeworks.homework_10.entity.Pet;
import ua.com.danit.homeworks.homework_10.service.FamilyService;

import java.text.ParseException;
import java.util.HashSet;
import java.util.List;

public class FamilyController {

    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public void displayAllFamilies() {
        this.familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return this.familyService.getFamiliesBiggerThan(count);
    }

    public List<Family> getFamiliesLessThan(int count) {
        return this.familyService.getFamiliesLessThan(count);
    }

    public int countFamiliesWithMemberNumber(int count) {
        return this.familyService.countFamiliesWithMemberNumber(count);
    }

    public boolean createNewFamily(Human mother, Human father) {
        return this.familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return this.familyService.deleteFamilyByIndex(index);
    }

    public boolean bornChild(Family family, String mName, String fName) throws ParseException {
        return this.familyService.bornChild(family, mName, fName);
    }

    public boolean adoptChild(Family family, Human human) {
        return this.familyService.adoptChild(family, human);
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return this.familyService.count();
    }

    public Family getFamilyById(int index) {
        return this.familyService.getFamilyById(index);
    }

    public HashSet<Pet> getPets(int index) {
        return this.familyService.getPets(index);
    }

    public void addPet(int familyIndex, Pet pet) {
        this.familyService.addPet(familyIndex, pet);
    }

    public FamilyDao getFamilyDao() {
        return this.familyService.getFamilyDao();
    }
}
