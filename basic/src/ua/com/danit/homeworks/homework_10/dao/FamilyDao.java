package ua.com.danit.homeworks.homework_10.dao;

import ua.com.danit.homeworks.homework_10.entity.Family;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    boolean saveFamily(Family family);
}
