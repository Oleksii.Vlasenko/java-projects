package ua.com.danit.homeworks.homework_10.entity.humans;

import ua.com.danit.homeworks.homework_10.entity.Human;

import java.text.ParseException;
import java.util.HashMap;

public final class Woman extends Human {

    public Woman() {
        super();
    }

    public Woman(String name, String surname, String birth) throws ParseException {
        super(name, surname, birth);
    }

    public Woman(String name, String surname, String birth, int iq, HashMap<String, String> schedule) throws ParseException {
        super(name, surname, birth, iq, schedule);
    }

    public void cook() {
        System.out.println("I am cooking!");
    }

    @Override
    public void greetPet() {
        System.out.println("I am your mother!");
    }

}
