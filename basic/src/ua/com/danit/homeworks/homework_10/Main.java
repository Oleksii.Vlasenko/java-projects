package ua.com.danit.homeworks.homework_10;

import ua.com.danit.homeworks.homework_10.controller.FamilyController;
import ua.com.danit.homeworks.homework_10.dao.CollectionFamilyDao;
import ua.com.danit.homeworks.homework_10.entity.DayOfWeek;
import ua.com.danit.homeworks.homework_10.entity.Family;
import ua.com.danit.homeworks.homework_10.entity.Human;
import ua.com.danit.homeworks.homework_10.entity.humans.Man;
import ua.com.danit.homeworks.homework_10.entity.humans.ManName;
import ua.com.danit.homeworks.homework_10.entity.humans.Woman;
import ua.com.danit.homeworks.homework_10.entity.humans.WomanName;
import ua.com.danit.homeworks.homework_10.entity.pets.Dog;
import ua.com.danit.homeworks.homework_10.entity.pets.DomesticCat;
import ua.com.danit.homeworks.homework_10.service.FamilyService;

import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        try {
            CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
            FamilyService familyService = new FamilyService(collectionFamilyDao);
            FamilyController familyController = new FamilyController(familyService);

            HashMap<String, String> schedule_1 = new HashMap<String, String>();
            schedule_1.put(DayOfWeek.MONDAY.name(), "Education");
            schedule_1.put(DayOfWeek.TUESDAY.name(), "Sport");
            schedule_1.put(DayOfWeek.WEDNESDAY.name(), "Education");
            schedule_1.put(DayOfWeek.THURSDAY.name(), "Sport");
            schedule_1.put(DayOfWeek.FRIDAY.name(), "Education");
            schedule_1.put(DayOfWeek.SATURDAY.name(), "Sport");
            schedule_1.put(DayOfWeek.SUNDAY.name(), "Rest");

            HashSet<String> dogHabits = new HashSet<>();
            dogHabits.add("sleep");
            dogHabits.add("jump");
            dogHabits.add("run");

            Dog dog = new Dog("Doggy", 1, 40, dogHabits);

            Woman mother_1 = new Woman("Mother", "Family_1", "16/01/1997", 148, schedule_1);
            System.out.println(mother_1.describeAge());
            Man father_1 = new Man("Father", "Family_1", "16/01/1987", 146, schedule_1);
            System.out.println(father_1.describeAge());
            Family family1 = null;

            if (familyController.createNewFamily(mother_1, father_1)) {
                family1 = familyController.getFamilyById(familyController.count() - 1);
            }
            if (family1 != null) {
                familyController.bornChild(family1, ManName.SAM.toString(), WomanName.ADA.toString());
            }

            familyController.createNewFamily(new Woman(), new Man());
            familyController.createNewFamily(new Woman(), new Man());

            System.out.println(familyController.getFamiliesBiggerThan(2).toString());
            System.out.println(familyController.getFamiliesLessThan(3).size());
            System.out.println(familyController.countFamiliesWithMemberNumber(3));
            if (familyController.deleteFamilyByIndex(2)) {
                familyController.displayAllFamilies();
            }
            Human child = new Woman(WomanName.WANDA.toString(), "Unknown", "11/05/2010", 140, null);
            if (familyController.adoptChild(familyController.getFamilyById(1), child)) {
                System.out.println("Adopt successful!");
            }
            System.out.println();
            System.out.println(familyController.getFamilyById(1).toString());
            familyController.deleteAllChildrenOlderThen(8);
            System.out.println();
            System.out.println(familyController.getFamilyById(1).toString());

            System.out.println(familyController.count());

            familyController.addPet(0, new DomesticCat());
            System.out.println(familyController.getPets(0).toString());

            System.out.println(familyController.getFamilyDao().toString());


        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
