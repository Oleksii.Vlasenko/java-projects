package ua.com.danit.homeworks.homework_8;

import ua.com.danit.homeworks.homework_8.humans.Man;
import ua.com.danit.homeworks.homework_8.humans.Woman;
import ua.com.danit.homeworks.homework_8.pets.Dog;
import ua.com.danit.homeworks.homework_8.pets.DomesticCat;
import ua.com.danit.homeworks.homework_8.pets.UnknownPet;

import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        try {
            HashMap<String, String> schedule_1 = new HashMap<String, String>();
            schedule_1.put(DayOfWeek.MONDAY.name(), "Education");
            schedule_1.put(DayOfWeek.TUESDAY.name(), "Sport");
            schedule_1.put(DayOfWeek.WEDNESDAY.name(), "Education");
            schedule_1.put(DayOfWeek.THURSDAY.name(), "Sport");
            schedule_1.put(DayOfWeek.FRIDAY.name(), "Education");
            schedule_1.put(DayOfWeek.SATURDAY.name(), "Sport");
            schedule_1.put(DayOfWeek.SUNDAY.name(), "Rest");

            UnknownPet unknownPet = new UnknownPet();
            unknownPet.eat();
            System.out.println(unknownPet.toString());

            HashSet<String> dogHabits = new HashSet<String>();
            dogHabits.add("sleep");
            dogHabits.add("jump");
            dogHabits.add("run");
            Dog dog = new Dog("Doggy", 1, 40, dogHabits);
            HashSet<Pet> pets_1 = new HashSet<Pet>();
            dog.respond();
            pets_1.add(dog);

            Woman mother_1 = new Woman("Mother", "Family_1", 1980, 148, schedule_1);
            Man father_1 = new Man("Father", "Family_1", 1976, 146, schedule_1);
            Family family1 = new Family(mother_1, father_1, pets_1);

            Human child_1;
            child_1 = father_1.bornChild(mother_1);

            mother_1.greetPet();
            father_1.greetPet();

            child_1.describePet();
            child_1.feedPet(true);

            for (Pet pet : family1.getPets()) {
                System.out.println(pet.toString());
            }

            mother_1.feedPet(false);
            mother_1.cook();
            father_1.repair();

            System.out.println(mother_1.toString());
            System.out.println(father_1.toString());
            System.out.println(child_1.toString());
            System.out.println(dog.toString());

            DomesticCat cat = new DomesticCat("Catty");
            HashSet<Pet> pets_2 = new HashSet<Pet>();
            pets_2.add(cat);
            Woman mother_2 = new Woman("Mother", "Family_2", 1988, 136, schedule_1);
            Man father_2 = new Man("Father", "Family_2", 1984, 142, schedule_1);

            Family family2 = new Family(mother_2, father_2, pets_2);
            Human child_2 = father_2.bornChild(mother_2);

            System.out.println(mother_2.toString());
            System.out.println(father_2.toString());
            System.out.println(child_2.toString());
            System.out.println(cat.toString());

            child_2.feedPet(false);

            family2.deleteChild(child_2);
            family1.deleteChild(child_1);
            System.out.println(family1.toString());

//            for (int i = 0; i < 1000000; i++) {
//                Human human = new Man();
//            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
