package ua.com.danit.homeworks.homework_8.humans;

import ua.com.danit.homeworks.homework_8.Human;

public interface HumanCreator {
    Human bornChild(Human human) throws Exception;
}
