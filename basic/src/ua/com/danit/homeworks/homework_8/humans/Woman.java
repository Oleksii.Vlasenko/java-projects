package ua.com.danit.homeworks.homework_8.humans;

import ua.com.danit.homeworks.homework_8.Human;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

public final class Woman extends Human implements HumanCreator {

    public Woman() {
        super();
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void cook() {
        System.out.println("I am cooking!");
    }

    @Override
    public void greetPet() {
        System.out.println("I am your mother!");
    }

    @Override
    public Human bornChild(Human human) throws Exception {
        if (!(human instanceof Man)) {
            throw new Exception("You need man to born a child!");
        }
        if (this.getFamily() == null && human.getFamily() == null) {
            throw new Exception("You should get married before!");
        }
        if (this.getFamily().hashCode() != human.getFamily().hashCode()) {
            throw new Exception("You should be ashamed!");
        }
        if (!(human instanceof Man)) {
            throw new Exception("You need woman to born a child!");
        }
        Random random = new Random();
        Human child;
        int year =  Calendar.getInstance().get(Calendar.YEAR);
        int iq = (this.getIq() + human.getIq()) / 2;
        if (random.nextInt(2) == 1) {
            int nameIndex = random.nextInt(ManName.values().length);
            String name = ManName.values()[nameIndex].toString();
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            child = new Man(name, human.getSurname(), year, iq, null);
        } else {
            int nameIndex = random.nextInt(WomanName.values().length);
            String name = WomanName.values()[nameIndex].toString();
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            child = new Woman(name, human.getSurname(), year, iq, null);
        }
        this.getFamily().addChild(child);
        return child;
    }
}
