package ua.com.danit.homeworks.homework_5;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    Human mother;
    Human father;
    Human[] children = new Human[0];
    Pet pet;

    static {
        System.out.println("Download the Pet class");
    }

    {
        System.out.println("Create the Pet object");
    }

    Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.pet.setFamily(this);
    }

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] temp = new Human[this.children.length + 1];
        System.arraycopy(this.children, 0, temp, 0, this.children.length);
        temp[temp.length - 1] = child;
        this.children = temp;
    }

    public void deleteChild(Human child) {
        Human[] temp = new Human[this.children.length - 1];
        int index = this.children.length - 1;
        if (this.children.length > 0) {
            for (int i = 0; i < temp.length; i++) {
                if (!this.children[i].equals(child)) {
                    temp[i] = this.children[i];
                } else {
                    index = i;
                }
            }
            if (this.children[index].equals(child)) {
                this.children = temp;
                child.setFamily(null);
                System.out.println("Deleted successfully!");
            } else {
                System.out.println("Wrong child to remove!");
            }
        } else {
            System.out.println("Wrong child to remove!");
        }
    }

    public int countFamily () {
        int count = 0;
        if (this.mother != null) {
            count++;
        }
        if (this.father != null) {
            count++;
        }
        for (Human child : children) {
            if (child != null) {
                count++;
            }
        }
        return count;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        if (this.mother != null) {
            str.append(this.mother.toString());
        }
        if (this.father != null) {
            str.append(this.father.toString());
        }
        if (this.children != null) {
            for (Human child: this.children) {
                str.append(child.toString());
            }
        }
        if (this.pet != null) {
            str.append(this.pet.toString());
        }
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
}
