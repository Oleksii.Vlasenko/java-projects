package ua.com.danit.homeworks.homework_5;

public class Main {
    public static void main(String[] args) {
        Pet unknownPet = new Pet();
        unknownPet.eat();
        unknownPet.foul();
        System.out.println(unknownPet.toString());

        String[] dogHabits = {"sleep", "jump", "run"};
        Pet dog = new Pet("dog", "Doggy", 1, 40, dogHabits);
        dog.respond();

        Human mother_1 = new Human("Mother", "Family_1", 1980, null, null);
        Human father_1 = new Human("Father", "Family_1", 1976);
        String[][] schedule_1 = {{"Monday", "Education"}, {"Tuesday", "Sport"}, {"Wednesday", "Education"}, {"Thursday", "Sport"}, {"Friday", "Education"}, {"Saturday", "Sport"}, {"Sunday", "Rest"}};
        Human son_1 = new Human("Son", "Family_1", 2000, 148, schedule_1);

        Family family1 = new Family(mother_1, father_1, dog);
        family1.addChild(son_1);

        son_1.greetPet();
        son_1.describePet();

        mother_1.feedPet(false);
        son_1.feedPet(true);
        System.out.println(son_1.getFamily().pet.toString());

        System.out.println(mother_1.toString());
        System.out.println(father_1.toString());
        System.out.println(son_1.toString());
        System.out.println(dog.toString());

        String[] catHabits = {"jump", "run"};
        Pet cat = new Pet("cat", "Catty");
        Human mother_2 = new Human("Mother", "Family_2", 1988);
        Human father_2 = new Human("Father", "Family_2", 1984, null, null);
        String[][] schedule_2 = {{"Monday", "Education"}, {"Tuesday", "Sport"}, {"Wednesday", "Education"}, {"Thursday", "Sport"}, {"Friday", "Education"}, {"Saturday", "Sport"}, {"Sunday", "Rest"}};
        Human daughter_2 = new Human("Daughter", "Family_2", 2006, 148, schedule_2);

        Family family2 = new Family(mother_2, father_2, cat);
        family2.addChild(daughter_2);


        System.out.println(mother_2.toString());
        System.out.println(father_2.toString());
        System.out.println(daughter_2.toString());
        System.out.println(cat.toString());

        daughter_2.feedPet(false);

        family1.deleteChild(daughter_2);
        family1.deleteChild(son_1);
        System.out.println(family1.toString());

    }
}
