package ua.com.danit.homeworks.homework_5;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Download the Pet class");
    }

    {
        System.out.println("Create the Pet object");
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    private Family family;

    Pet() {
        this.species = "pet";
        this.nickname = "unknown";
        this.age = -1;
        this.trickLevel = -1;
        this.habits = null;
    }

    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        this.age = -1;
        this.trickLevel = -1;
        this.habits = null;
    }

    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String getSpecies() {
        return this.species;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public int getAge() {
        return this.age;
    }

    public void eat() {
        System.out.println("Я кушаю!)");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!)");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...)");
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.species).append("{nickname='").append(this.nickname).append("'");
        if (this.age != -1) {
            str.append(", age=").append(this.age);
        }
        if (this.trickLevel != -1) {
            str.append(", trickLevel=").append(this.trickLevel);
        }
        if (this.habits != null) {
            str.append(", habits=").append(Arrays.toString(this.habits));
        }
        str.append("}");
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname) &&
                Objects.equals(family, pet.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, family);
    }
}
