package ua.com.danit.homeworks.homework_5;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {

    private final String name;
    private final String surname;
    private final int year;
    private final int iq;
    private final String[][] schedule;
    private Family family;

    static {
        System.out.println("Download the Human class");
    }

    {
        System.out.println("Create the Human object");
    }

    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = 0;
        this.schedule = null;
    }

    Human(String name, String surname, int year, Human father, Human mother) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = 0;
        this.schedule = null;
    }

    Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void greetPet() {
        System.out.println("Привет, " + this.family.pet.getNickname());
    }

    public void describePet() {
        String trickLevel = this.family.pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
        String message = "У меня есть " + this.family.pet.getSpecies() + ", ему " + this.family.pet.getAge() + " лет, он " + trickLevel + ".";
        System.out.println(message);
    }

    public boolean feedPet(boolean feed) {
        if (this.family.pet == null) {
            System.out.println("There are no pets!");
            return false;
        }
        if (!feed) {
            Random random = new Random();
            int randomFeed = random.nextInt(101);
            System.out.println(randomFeed < this.family.pet.getTrickLevel()
                    ? "Хм... покормлю ка я " + this.family.pet.getNickname() + "."
                    : "Думаю, " + this.family.pet.getNickname() + " не голоден.");
            return this.family.pet.getTrickLevel() > randomFeed;
        }
        System.out.println("Покормлю ка я " + this.family.pet.getNickname() + ".");
        return true;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Human{name='")
                .append(this.name)
                .append("', surname='")
                .append(this.surname)
                .append("', year=")
                .append(this.year);
        if (this.iq != 0) {
            str.append(", iq=").append(this.iq);
        }
        if (this.schedule != null) {
            str.append(" schedule=").append(Arrays.deepToString(this.schedule));
        }
        str.append("}");
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, family);
    }
}
