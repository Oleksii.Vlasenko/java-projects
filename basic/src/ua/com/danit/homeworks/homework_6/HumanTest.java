package ua.com.danit.homeworks.homework_6;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    private static Family family;
    private static Human mother;
    private static Human father;
    private static Pet pet;
    private static String[][] schedule;

    @BeforeAll
    public static void initHuman() {
        schedule = new String[][]{
                {Human.DayOfWeek.MONDAY.name(), "Education"},
                {Human.DayOfWeek.TUESDAY.name(), "Sport"},
                {Human.DayOfWeek.WEDNESDAY.name(), "Education"},
                {Human.DayOfWeek.THURSDAY.name(), "Sport"},
                {Human.DayOfWeek.FRIDAY.name(), "Education"},
                {Human.DayOfWeek.SATURDAY.name(), "Sport"},
                {Human.DayOfWeek.SUNDAY.name(), "Rest"}
        };
        mother = new Human("Mother", "Family", 2000, 148, schedule);
        father = new Human("Father", "Family", 2000);
        pet = new Pet("DOG", "BILL");
        family = new Family(mother, father, pet);
    }

    @Test
    @DisplayName("Return human's name")
    void getName() {
        assertEquals("Mother", mother.getName());
    }

    @Test
    @DisplayName("Return human's surname")
    void getSurname() {
        assertEquals("Family", father.getSurname());
    }

    @Test
    @DisplayName("Return human's year")
    void getYear() {
        assertEquals(2000, mother.getYear());
    }

    @Test
    @DisplayName("Return human's iq")
    void getIq() {
        assertEquals(148, mother.getIq());
    }

    @Test
    @DisplayName("Return human's default iq")
    void getDefaultIq() {
        assertEquals(0, father.getIq());
    }

    @Test
    @DisplayName("Return human's correct schedule")
    void getSchedule() {
        assertArrayEquals(schedule, mother.getSchedule());
    }

    @Test
    @DisplayName("Return human's default schedule")
    void getDefaultSchedule() {
        assertArrayEquals(null, father.getSchedule());
    }

    @Test
    @DisplayName("family equals mother.family")
    void getFamily() {
        assertEquals(family, mother.getFamily());
    }

    @Test
    @DisplayName("father.family equals mother.family")
    void getFamilyByFamilyMembers() {
        assertEquals(father.getFamily(), mother.getFamily());
    }

    @Test
    @DisplayName("change father.family - null")
    void setFamily() {
        father.setFamily(null);
        assertEquals(null, father.getFamily());
    }
//
    @Test
    @DisplayName("feed pet")
    void feedPet() {
        assertTrue(mother.feedPet(true));
    }

    @Test
    @DisplayName("don't feed pet")
    void feedPet_false() {
        assertFalse(mother.feedPet(false));
    }

    @Test
    @DisplayName("must toString")
    void testToString() {
        String str = "Human{name='" + father.getName() + "', surname='" + father.getSurname() + "', year=" + father.getYear() + "}";
        assertEquals(str, father.toString());

    }

    @Test
    @DisplayName("mother must equal new mother")
    void testEquals() {
        Human anotherMother = new Human("Mother", "Family", 2000, 148, schedule);
        anotherMother.setFamily(family);
        assertTrue(mother.equals(anotherMother));
    }

    @Test
    @DisplayName("mother must not equal new mother")
    void testEquals_false () {
        Human anotherMother = new Human("Mother", "Family", 2002, 148, schedule);
        assertFalse(mother.equals(anotherMother));
    }

    @Test
    @DisplayName("mother's hashCode must equals family mother's")
    void testHashCode() {
        assertEquals(mother.hashCode(), family.getMother().hashCode());
    }

    @Test
    @DisplayName("father's hashCode must not equals new father's")
    void testHashCode_false() {
        Human newFather = new Human("Father", "Family", 2000, 130, null);
        assertNotEquals(father.hashCode(), newFather.hashCode());
    }
}