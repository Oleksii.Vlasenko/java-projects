package ua.com.danit.homeworks.homework_6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    private static Family family;
    private static Human mother;
    private static Human father;
    private static Human child_1;
    private static Human child_2;
    private static Human child_3;
    private static Human child_4;

    @BeforeAll
    public static void initFamily() {
        mother = new Human();
        father = new Human();
        family = new Family(mother, father);
        child_1 = new Human();
        child_2 = new Human();
        child_3 = new Human();
        child_4 = new Human();
        family.addChild(child_1);
        family.addChild(child_2);
        family.addChild(child_3);
    }

    @Test
    @DisplayName("Must added 1 child")
    public void addChild_true() {
        int beforeLength = family.getChildren().length;
        family.addChild(child_4);
        assertEquals(beforeLength + 1, family.getChildren().length);
    }

    @Test
    @DisplayName("Must not deleted child by index")
    void deleteChildByIndex_false() {
        family.deleteChild(5);
        assertNotEquals(3, family.getChildren().length);
    }

    @Test
    @DisplayName("Must not deleted child")
    void deleteChildByChild_false() {
        Human child_5 = new Human();
        family.deleteChild(child_5);
        assertNotEquals(3, family.getChildren().length);
    }

    @Test
    @DisplayName("must deleted child by index")
    void deleteChildByIndex() {
        int beforeLength = family.getChildren().length;
        family.deleteChild(1);
        assertEquals(beforeLength - 1, family.getChildren().length);
    }

    @Test
    @DisplayName("must deleted child")
    void deleteChildByChild() {
        Human mother_1 = new Human();
        Human father_1 = new Human();
        Family family_1 = new Family(mother_1, father_1);
        Human newChild_1 = new Human();
        Human newChild_2 = new Human();
        family_1.addChild(newChild_1);
        family_1.addChild(newChild_2);
        int beforeLength = family_1.getChildren().length;
        family_1.deleteChild(newChild_1);
        assertEquals(beforeLength - 1, family_1.getChildren().length);
    }


    @Test
    @DisplayName("Must added 2 family members")
    void countFamily() {
        int beforeCount = family.countFamily();
        family.addChild(new Human());
        family.addChild(new Human());
        assertEquals(beforeCount + 2, family.countFamily());
    }

    @Test
    @DisplayName("must to String")
    void testToString() {
        Human mother_1 = new Human("Mother_1", "Family_1", 2000);
        Human father_1 = new Human("Father_1", "Family_1", 2000);
        Family family_1 = new Family(mother_1, father_1);
        String str = mother_1.toString() + '\n' + father_1.toString();
        assertEquals(str, family_1.toString());
    }

    @Test
    @DisplayName("family must equal another family")
    void testEquals() {
        assertTrue(new Family(new Human(), new Human()).equals(new Family(new Human(), new Human())));
    }

    @Test
    @DisplayName("family must not equal new family")
    void testEquals_false () {
        Human mother_1 = new Human("Mother", "Family", 2000, 148, null);
        assertFalse(new Family(mother_1, new Human()).equals(new Family(new Human(), new Human())));
    }

    @Test
    @DisplayName("mother's hashCode must equals family mother's")
    void testHashCode() {
        assertEquals(family.hashCode(), mother.getFamily().hashCode());
    }

    @Test
    @DisplayName("family's hashCode must not equals another family's")
    void testHashCode_false() {
        Human mother_1 = new Human();
        Human father_1 = new Human("Father_1", "Family_1", 1990);
        Family family_1 = new Family(mother_1, father_1);
        Human mother_2 = new Human();
        Human father_2 = new Human();
        Family family_2 = new Family(mother_2, father_2);
        assertNotEquals(family_1.hashCode(), family_2.hashCode());
    }

    @Test
    @DisplayName("check items after del child")
    void testDeleteChildItems() {
        Human testMather = new Human();
        Human testFather = new Human();
        Family testFamily = new Family(testMather, testFather);
        Human testChild1 = new Human("Child 1", "TestFamily", 1);
        Human testChild2 = new Human("Child 2", "TestFamily", 2);
        Human testChild3 = new Human("Child 3", "TestFamily", 3);
        Human testChild4 = new Human("Child 4", "TestFamily", 4);
        Human testChild5 = new Human("Child 5", "TestFamily", 5);
        testFamily.addChild(testChild1);
        testFamily.addChild(testChild2);
        testFamily.addChild(testChild3);
        testFamily.addChild(testChild4);
        testFamily.addChild(testChild5);
        testFamily.deleteChild(3);
        boolean isContainLastItem = false;
        for (Human child : testFamily.getChildren()) {
            isContainLastItem = isContainLastItem || child.getName().equals("Child 5");
        }
        assertTrue(isContainLastItem);
    }
}