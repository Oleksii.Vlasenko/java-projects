package ua.com.danit.homeworks.homework_12.entity.humans;

import ua.com.danit.homeworks.homework_12.entity.Human;

public interface HumanCreator {
    Human bornChild(Human human) throws Exception;
}
