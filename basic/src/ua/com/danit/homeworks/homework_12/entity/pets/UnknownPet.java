package ua.com.danit.homeworks.homework_12.entity.pets;

import ua.com.danit.homeworks.homework_12.entity.Pet;

public class UnknownPet extends Pet {
    public UnknownPet() {
        super("UNKNOWN");
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я не знаю кто я, но соскучился!)");
    }
}
