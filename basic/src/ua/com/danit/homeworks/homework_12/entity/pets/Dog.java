package ua.com.danit.homeworks.homework_12.entity.pets;

import ua.com.danit.homeworks.homework_12.entity.Pet;

import java.util.HashSet;
import java.util.List;
import java.util.Stack;

public class Dog extends Pet implements Foul {
    public Dog() {
        super("DOG");
    }

    public Dog(String nickname) {
        super("DOG", nickname);
    }

    public Dog(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super("DOG", nickname, age, trickLevel, habits);
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...)");
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + this.getNickname() + ". Я соскучился!)");
    }
}
