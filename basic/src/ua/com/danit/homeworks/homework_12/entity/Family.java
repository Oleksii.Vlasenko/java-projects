package ua.com.danit.homeworks.homework_12.entity;

import ua.com.danit.homeworks.homework_12.entity.humans.Man;

import java.nio.MappedByteBuffer;
import java.text.SimpleDateFormat;
import java.util.*;

public class Family {

    private final Human mother;
    private final Human father;
    private final ArrayList<Human> children = new ArrayList<Human>();
    private HashSet<Pet> pets = new HashSet<Pet>();

    public ArrayList<Human> getChildren() {
        return children;
    }

    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return father;
    }

    public HashSet<Pet> getPets() {
        return pets;
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
    }

    static {
        System.out.println("Download the Family class");
    }

    {
        System.out.println("Create the Family object");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Family(Human mother, Human father, HashSet<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.pets = pets;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        for (Pet pet : this.pets) {
            pet.setFamily(this);
        }
    }

    public void addChild(Human child) {
        this.children.add(child);
        child.setFamily(this);
    }

    public void deleteChild(Human child) {
        this.children.remove(child);
    }

    public void deleteChild(int index) {
        this.children.remove(index);
    }

    public int countFamily() {
        int count = 0;
        if (this.mother != null) {
            count++;
        }
        if (this.father != null) {
            count++;
        }
        for (Human child : children) {
            if (child != null) {
                count++;
            }
        }
        return count;
    }

    public String prettyFormat() {
        StringBuilder str = new StringBuilder();
        str.append("family:")
                .append('\n');
        if (this.mother != null) {
            str.append('\t')
                    .append("mother: ")
                    .append(this.mother.prettyFormat())
                    .append(",")
                    .append('\n');
        }
        if (this.father != null) {
            str.append('\t')
                    .append("father: ")
                    .append(this.father.prettyFormat())
                    .append(",")
                    .append('\n');
        }
        if (this.children.size() > 0) {
            str.append('\t')
                    .append("children: ")
                    .append('\n');
            for (Human child : this.children) {
                String childType = child instanceof Man ? "boy: " : "girl: ";
                str.append('\t')
                        .append('\t')
                        .append('\t')
                        .append(childType)
                        .append(child.prettyFormat())
                        .append('\n');
            }
        }
        str.append('\t')
                .append("pets: [");
        for (Pet pet : pets) {
            str.append("{")
                    .append(pet.prettyFormat())
                    .append("},");
        }
        if (this.pets.size() > 0) {
            str.deleteCharAt(str.length() - 1);
        }
        str.append("]");
        return str.toString();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        if (this.mother != null) {
            str.append(this.mother.toString());
        }
        if (this.father != null) {
            str.append('\n').append(this.father.toString());
        }
        if (this.children.size() > 0) {
            for (Human child : this.children) {
                str.append('\n').append(child.toString());
            }
        }
        for (Pet pet : pets) {
            str.append('\n').append(pet.toString());
        }
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                children.equals(family.children) &&
                Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pets);
        result = 31 * result + Arrays.hashCode(new ArrayList[]{children});
        return result;
    }

    @Override
    public void finalize() {
        System.out.println("Family object deleted!");
    }
}
