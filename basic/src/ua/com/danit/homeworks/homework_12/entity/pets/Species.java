package ua.com.danit.homeworks.homework_12.entity.pets;

public enum Species {
    UNKNOWN(false, 0, false),
    DOG(false, 4, true),
    DOMESTICCAT(false, 4, true),
    FISH(false, 0, false),
    ROBOCAT(false, 4, false);

    public boolean canFly;
    public int numberOfLegs;
    public boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}
