package ua.com.danit.homeworks.homework_12;

import ua.com.danit.homeworks.homework_12.application.ConsoleApp;
import ua.com.danit.homeworks.homework_12.controller.FamilyController;
import ua.com.danit.homeworks.homework_12.dao.CollectionFamilyDao;
import ua.com.danit.homeworks.homework_12.entity.DayOfWeek;
import ua.com.danit.homeworks.homework_12.entity.Family;
import ua.com.danit.homeworks.homework_12.entity.Human;
import ua.com.danit.homeworks.homework_12.entity.humans.Man;
import ua.com.danit.homeworks.homework_12.entity.humans.ManName;
import ua.com.danit.homeworks.homework_12.entity.humans.Woman;
import ua.com.danit.homeworks.homework_12.entity.humans.WomanName;
import ua.com.danit.homeworks.homework_12.entity.pets.Dog;
import ua.com.danit.homeworks.homework_12.entity.pets.DomesticCat;
import ua.com.danit.homeworks.homework_12.service.FamilyService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        try {
            ConsoleApp.startApp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
