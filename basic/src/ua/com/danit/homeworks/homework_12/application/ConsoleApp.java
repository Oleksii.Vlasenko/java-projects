package ua.com.danit.homeworks.homework_12.application;

import ua.com.danit.homeworks.homework_12.controller.FamilyController;
import ua.com.danit.homeworks.homework_12.dao.CollectionFamilyDao;
import ua.com.danit.homeworks.homework_12.entity.Human;
import ua.com.danit.homeworks.homework_12.entity.humans.Man;
import ua.com.danit.homeworks.homework_12.entity.humans.Woman;
import ua.com.danit.homeworks.homework_12.service.FamilyService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class ConsoleApp {
    public static void startApp() {
        try {
            CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
            FamilyService familyService = new FamilyService(collectionFamilyDao);
            FamilyController familyController = new FamilyController(familyService);

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String inputLine;

            boolean stopProgram = false;
            while (!stopProgram) {
                try {
                    System.out.println();
                    showMainMenu();
                    System.out.println();
                    System.out.print("Enter the command: ");
                    inputLine = reader.readLine();
                    System.out.println();
                    switch (inputLine) {
                        case "1":
                            familyController.createNewFamily(new Woman(), new Man());
                            familyController.createNewFamily(new Woman(), new Man());
                            System.out.println("Families created!");
                            break;
                        case "2":
                            System.out.println("Family(-s): ");
                            familyController.displayAllFamilies();
                            break;
                        case "3":
                            System.out.print("Enter the count: ");
                            int countBigger = Integer.parseInt(reader.readLine());
                            familyController.getFamiliesBiggerThan(countBigger).forEach(f -> System.out.println(f.prettyFormat()));
                            break;
                        case "4":
//                        try {
                            System.out.print("Enter the count: ");
                            int countLess = Integer.parseInt(reader.readLine());
                            familyController.getFamiliesLessThan(countLess).forEach(f -> System.out.println(f.prettyFormat()));
                            break;
//                        } catch (NumberFormatException e) {
//                            System.out.println(e.getMessage());
//                            break;
//                        }
                        case "5":
//                        try {
                            System.out.print("Enter the number of family members: ");
                            int countMembers = Integer.parseInt(reader.readLine());
                            System.out.println(familyController.countFamiliesWithMemberNumber(countMembers) + " family(-s)");
                            break;
//                        } catch (NumberFormatException e) {
//                            System.out.println(e.getMessage());
//                            break;
//                        }
                        case "6":
//                        try {
                            System.out.println("New family: ");

                            System.out.print("Mother's name: ");
                            String motherName = reader.readLine();
                            System.out.print("Mother's surname: ");
                            String motherSurname = reader.readLine();
                            System.out.print("Mother's year: ");
                            int motherYear = Integer.parseInt(reader.readLine());
                            System.out.print("Mother's month: ");
                            int motherMonth = Integer.parseInt(reader.readLine());
                            System.out.print("Mother's day: ");
                            int motherDay = Integer.parseInt(reader.readLine());
                            System.out.print("Mother's iq: ");
                            int motherIq = Integer.parseInt(reader.readLine());
                            String motherBirth = motherDay + "/" + motherMonth + "/" + motherYear;
                            Human mother = new Woman(motherName, motherSurname, motherBirth, motherIq);

                            System.out.print("Father's name: ");
                            String fatherName = reader.readLine();
                            System.out.print("Father's surname: ");
                            String fatherSurname = reader.readLine();
                            System.out.print("Father's year: ");
                            int fatherYear = Integer.parseInt(reader.readLine());
                            System.out.print("Father's month: ");
                            int fatherMonth = Integer.parseInt(reader.readLine());
                            System.out.print("Father's day: ");
                            int fatherDay = Integer.parseInt(reader.readLine());
                            System.out.print("Father's iq: ");
                            int fatherIq = Integer.parseInt(reader.readLine());
                            String fatherBirth = fatherDay + "/" + fatherMonth + "/" + fatherYear;
                            Human father = new Man(fatherName, fatherSurname, fatherBirth, fatherIq);

                            familyController.createNewFamily(mother, father);
                            break;
//                        } catch (NumberFormatException e) {
//                            System.out.println(e.getMessage());
//                            break;
//                        }
                        case "7":
//                        try {
                            System.out.print("Enter index to delete: ");
                            int remIndex = Integer.parseInt(reader.readLine());
                            familyController.deleteFamilyByIndex(remIndex);
                            break;
//                        } catch (NumberFormatException e) {
//                            System.out.println(e.getMessage());
//                            break;
//                        }
                        case "8":
//                        try {
                            System.out.println("1. Born child.");
                            System.out.println("2. Adopt child.");
                            System.out.println("3. Back to main menu.");
                            String userSelect = reader.readLine();
                            switch (userSelect) {
                                case "1":
                                    System.out.println("Born child");
                                    System.out.print("Enter family index: ");
                                    int bornIndex = Integer.parseInt(reader.readLine());
                                    System.out.print("Enter boy's name: ");
                                    String boyName = reader.readLine();
                                    System.out.print("Enter girl's name: ");
                                    String girlName = reader.readLine();
                                    familyController.bornChild(familyController.getFamilyById(bornIndex), boyName, girlName);
                                    break;
                                case "2":
                                    System.out.println("Adopt child");
                                    System.out.print("Enter family index: ");
                                    int familyIndex = Integer.parseInt(reader.readLine());
                                    System.out.print("Enter child's name: ");
                                    String name = reader.readLine();
                                    System.out.print("Enter child's surname: ");
                                    String surname = reader.readLine();
                                    System.out.print("Enter child's year: ");
                                    int year = Integer.parseInt(reader.readLine());
                                    System.out.print("Enter child's month: ");
                                    int month = Integer.parseInt(reader.readLine());
                                    System.out.print("Enter child's day: ");
                                    int day = Integer.parseInt(reader.readLine());
                                    System.out.print("Enter child's iq: ");
                                    int iq = Integer.parseInt(reader.readLine());
                                    String birth = day + "/" + month + "/" + year;
                                    Human child = new Man(name, surname, birth, iq);
                                    familyController.adoptChild(familyController.getFamilyById(familyIndex), child);
                                    break;
                                case "3":
                                    break;
                                default:
                                    System.out.println("Unknown operation!");
                            }
                            break;
//                        } catch (NumberFormatException e) {
//                            System.out.println(e.getMessage());
//                            break;
//                        }
                        case "9":
//                        try {
                            System.out.print("Age to delete: ");
                            int ageToDelete = Integer.parseInt(reader.readLine());
                            familyController.deleteAllChildrenOlderThen(ageToDelete);
                            break;
//                        } catch (NumberFormatException e) {
//                            System.out.println(e.getMessage());
//                            break;
//                        }
                        case "exit":
                            stopProgram = true;
                            return;
                        default:
                            System.out.println("Unknown operation!");


                    }
                } catch (NumberFormatException | IndexOutOfBoundsException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                    System.out.println();
                }
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void showMainMenu() {
        System.out.println("MAIN MENU");
        System.out.println("1. Fill in with test data (automatically create several families and save them in the database)");
        System.out.println("2. Display the entire list of families (displays a list of all families with an index starting with 1)");
        System.out.println("3. Display a list of families where the number of people is more than a given");
        System.out.println("4. Display a list of families where the number of people is less than a given");
        System.out.println("5. Count the number of families where the number of members is");
        System.out.println("6. Create a new family");
        System.out.println("7. Delete a family by family index in the general list");
        System.out.println("8. Edit a family by family index in the general list");
        System.out.println("9. Delete all children over the age (children over the specified age are deleted in all families - we will assume that they have grown)");
    }


}
