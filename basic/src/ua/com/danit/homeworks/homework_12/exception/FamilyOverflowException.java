package ua.com.danit.homeworks.homework_12.exception;

public class FamilyOverflowException extends RuntimeException {

    private int familyCount;

    public FamilyOverflowException(int familyCount) {
        super("Exceeding the number of family members: " + familyCount);
        this.familyCount = familyCount;
    }

    public FamilyOverflowException(String message, int familyCount) {
        super(message);
        this.familyCount = familyCount;
    }

    public int getFamilyCount() {
        return familyCount;
    }
}
