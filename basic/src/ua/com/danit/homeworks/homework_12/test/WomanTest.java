package ua.com.danit.homeworks.homework_12.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.com.danit.homeworks.homework_12.entity.DayOfWeek;
import ua.com.danit.homeworks.homework_12.entity.Family;
import ua.com.danit.homeworks.homework_12.entity.Human;
import ua.com.danit.homeworks.homework_12.entity.Pet;
import ua.com.danit.homeworks.homework_12.entity.humans.Man;
import ua.com.danit.homeworks.homework_12.entity.humans.Woman;
import ua.com.danit.homeworks.homework_12.entity.pets.Dog;

import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class WomanTest {
    private static Family family;
    private static Human mother;
    private static Human father;
    private static HashSet<Pet> pets;
    private static HashMap<String, String> schedule;

    @BeforeAll
    public static void initHuman() throws ParseException {

        HashMap<String, String> schedule = new HashMap<String, String>();
        schedule.put(DayOfWeek.MONDAY.name(), "Education");
        schedule.put(DayOfWeek.TUESDAY.name(), "Sport");
        schedule.put(DayOfWeek.WEDNESDAY.name(), "Education");
        schedule.put(DayOfWeek.THURSDAY.name(), "Sport");
        schedule.put(DayOfWeek.FRIDAY.name(), "Education");
        schedule.put(DayOfWeek.SATURDAY.name(), "Sport");
        schedule.put(DayOfWeek.SUNDAY.name(), "Rest");

        mother = new Woman("Mother", "Family", "11/05/2000", 148, schedule);
        father = new Man("Father", "Family", "11/05/2000", 148, schedule);
        pets = new HashSet<Pet>(Collections.singletonList(new Dog("BILL", 5, 0, null)));
        family = new Family(mother, father, pets);
    }

    @Test
    @DisplayName("Return human's name")
    void getName() {
        assertEquals("Mother", mother.getName());
    }

    @Test
    @DisplayName("Return human's surname")
    void getSurname() {
        assertEquals("Family", mother.getSurname());
    }

    @Test
    @DisplayName("Return human's birthDate")
    void getBirthDate() {
        assertEquals(2000, mother.getBirthDate());
    }

    @Test
    @DisplayName("Return human's iq")
    void getIq() {
        assertEquals(148, mother.getIq());
    }

    @Test
    @DisplayName("Return human's default iq")
    void getDefaultIq() {
        assertEquals(148, mother.getIq());
    }

    @Test
    @DisplayName("family equals mother.family")
    void getFamily() {
        assertEquals(null, mother.getFamily());
    }

    @Test
    @DisplayName("father.family equals mother.family")
    void getFamilyByFamilyMembers() {
        assertEquals(father.getFamily(), mother.getFamily());
    }

    @Test
    @DisplayName("change mother.family - null")
    void setFamily() {
        mother.setFamily(null);
        assertEquals(null, mother.getFamily());
    }

    @Test
    @DisplayName("feed pet")
    void feedPet() {
        assertTrue(mother.feedPet(true));
    }

    @Test
    @DisplayName("feed pet - false")
    void dontFeedPet() {
        assertFalse(mother.feedPet(false));
    }

    @Test
    @DisplayName("must toString")
    void testToString() {
        String str = "Human{name='"
                + mother.getName()
                + "', surname='"
                + mother.getSurname()
                + "', birthDate="
                + mother.getFormatBirthDate()
                + ", iq="
                + mother.getIq()
                + ", schedule="
                + mother.getSchedule().toString()
                + "}";
        assertEquals(str, mother.toString());

    }

    @Test
    @DisplayName("mother must equal new mother")
    void testEquals() throws ParseException {
        Human anotherMother = new Woman("Mother", "Family", "11/05/2000", 148, null);
        anotherMother.setFamily(family);
        assertTrue(mother.equals(anotherMother));
    }

    @Test
    @DisplayName("mother must not equal new mother")
    void testEquals_false () throws ParseException {
        Human anotherMother = new Woman("Mother", "Family", "11/05/2000", 148, schedule);
        assertFalse(mother.equals(anotherMother));
    }

    @Test
    @DisplayName("mother's hashCode must equals family mother's")
    void testHashCode() {
        assertEquals(mother.hashCode(), family.getMother().hashCode());
    }

    @Test
    @DisplayName("mother's hashCode must not equals new mother's")
    void testHashCode_false() throws ParseException {
        Human newFather = new Man("Mother", "Family", "11/05/2000", 130, null);
        assertNotEquals(father.hashCode(), newFather.hashCode());
    }
}