package ua.com.danit.homeworks.homework_1;
import ua.com.danit.homeworks.homework_1.RandomNumber;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter your name: ");
        String name = reader.readLine();

        System.out.println("Let the game begin!");

        RandomNumber randomNumber = new RandomNumber(name);
        randomNumber.guessNumber();
    }
}
