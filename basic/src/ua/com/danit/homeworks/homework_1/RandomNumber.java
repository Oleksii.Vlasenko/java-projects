package ua.com.danit.homeworks.homework_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

public class RandomNumber {
    String[][] dates = {{"29", "Crucifixion of Jesus Christ"},
            {"375", "Huns' invasion of Europe"},
            {"570", "Prophet Mohammed born at Mecca"},
            {"622", "Flight of Mohammed from Mecca to Madina"},
            {"632", "Death of Mohammed; Beginning of Hijiri Era"},
            {"711", "Arabs invade Spain"},
            {"1066", "Norman invasion of England; Victory of William the Conquerer over the English King Harold II at Hastings"},
            {"1280", "Roger Bacon invents gunpowder"},
            {"1338", "The Hundred years War broke out"},
            {"1348", "English faces Black Death Plague"},
            {"1453", "Turks captured Constantinople; Renaissance in Europe"},
            {"1492", "Discovery of America by Columbus"},
            {"1498", "Sea-route to India discovered by Vasco-de-Gama"},
            {"1588", "Spanish Armada defeated"},
            {"1600", "British East India Company established in India"},
            {"1649", "Execution of Charles I"},
            {"1660", "Monarchy restored in England"},
            {"1665", "The Great Plague of England"},
            {"1668", "Glorious Revolution in England"},
            {"1704", "Battle of Blenheim"},
            {"1707", "Union of England and Scotland"},
            {"1776", "Declaration of American Independence"},
            {"1789", "French Revolution; George Washington elected the first President of America"},
            {"1805", "Battle of Trafalagar and Nelson's death"},
            {"1815", "Battle of Waterloo; Napolean exiled to St. Helena"},
            {"1821", "Death of Napolean"},
            {"1832", "Reforms Bill passed in England"},
            {"1837", "Queen Victoria's accession to the throne of England"},
            {"1861", "Beginning of the American Civil War"},
            {"1863", "Slavery abolished in USA"},
            {"1865", "Assassination of Abraham Lincoln"},
            {"1869", "Opening of the Suez Canal for traffic"},
            {"1895", "Roentgen discovered X-Rays"},
            {"1896", "Marconi invented wireless"},
            {"1904", "Russiao-Japan war"},
            {"1905", "Japan defeated Russia; Discovery of the theory of Relativity by Einstein"},
            {"1911", "Chinese Revolution"},
            {"1912", "Republic of China established"},
            {"1914", "Beginning of World War I"},
            {"1917", "Russian Revolution"},
            {"1918", "End of World War I"},
            {"1919", "Treaty of Versailles signed"},
            {"1920", "Formation of the League of Nations"},
            {"1923", "Turkey declared Republic"},
            {"1933", "Hitler became the Chancellor of Germany"},
            {"1936", "Beginning of the Spanish Civil War"},
            {"1939", "World War II begins"},
            {"1941", "Russia invaded by Hitler; Pearl Harbour invaded by Japan"},
            {"1945", "Establishment of UNO; End of World War II; Hiroshima and Nagasaki experience the first dropping of the Atom Bomb; Death of President Roosevelt"},
            {"1946", "Civil War in China"},
            {"1948", "Burma and Ceylon get independence"},
            {"1949", "Indonesia gets independence; The Communists capture power in China"},
            {"1952", "General Eisenhower elected as the American President"},
            {"1953", "Death of Stalin; Mt. Everest conquered for the first time"},
            {"1954", "Military Aid Pact between China and Pakistan; Chou En-lai visits India"},
            {"1955", "Austria gets independence; Bandung Conference"},
            {"1956", "Suez Canal nationalised by President Nasser; Egypt attacked by the forces of Britain; France and Israel"},
            {"1957", "First artificial satellite launched by Russia"},
            {"1958", "Egypt and Syria united and renamed United Arab Republic (UAR)"},
            {"1959", "Chinese capture Tibet; Dalai Lama flees to India; Sputnik launched by Russia"},
            {"1960", "Explosion of an atom bomb device by France; Election of John F. Kennedy as President of USA"},
            {"1961", "Yuri Gagarin of USSR becomes the first spaceman"},
            {"1963", "Partial Nuclear Test-Ban Treaty signed; Malaysia established; John F. Kennedy assassinated"},
            {"1965", "Death of Sir Winston Churchill; Singapore becomes the sovereign independent nation; outbreak of Indo-Pak war"},
            {"1966", "Tashkent Pact; A Russian aircraft lands on moon"},
            {"1967", "Chinese explode hydrogen bomb; Arab-Israel War; Suez Canal closed"},
            {"1971", "Outbreak of Indo-Pak war; Birth of Bangladesh; Surrender of 93,000 Pakistani troops; Khruschev died; Z.A. Bhutto new President of Pakistan"},
            {"1972", "Sheikh Mujibur Rahman freed from Pakistani Jail and assumed the office of P.M. Bangladesh; Nixon of USA visited China; King Mahendra of Nepal died; USA and the USSR sign Strategic Arms Limitations Treaty"},
            {"1973", "Outbreak of fourth Arab-Israeli war; Fourth non-aligned summit in Algiers"},
            {"1975", "Sheikh Mujibur Rahman, President of Bangladesh assassinated; King Faisal of Saudi Arabia, assassinated; Suez Canal reopened; Red Cross force Cambodia Government to Surrender"},
            {"1976", "Chou-En Lai, P.M. of China, died; Seychelles gets independence; Viking I lands on Mars; Mao Tse-tung died; Jimmy Carter elected President of USA"},
            {"1978", "Agreement between Israel and Egypt; Vietnam attacked Cambodia; Z.A. Bhutto, former P.M. of Pakistan, sentenced to death; Bloody coup in Afghanistan; Mohammed Daoud assassinated; World's first test-tube baby born"},
            {"1979", "Chinese aggression in Vietnam; Cambodian rebels grab power in Pnom Penh; Mr. Z.A. Bhutto hanged; Mrs. Margaret Thatcher is the first woman P.M. of Britain"},
            {"1980", "War starts between Iran and Iraq; Ronald Reagon elected USA President"},
            {"1982", "Falklands, captured by Argentina; Israel attacks South Lebanon; Argentina forces surrender to British; P.L.O. Chief Yesser Arafat leaves Beirut; Bashir Gemyel, the President elect of Lebanon, assassinated; Soviet President breathes his last"},
            {"1983", "US attacks Grenada; USA withdraws from UNESCO"},
            {"1985", "India gets Presidentship of UN Security Council; Soviet President, Mr. Konstantin Chernenko, dies; Vietnam withdraws troops from Kampuchia"},
            {"1986", "American air attack on Libya"},
            {"1987", "Nuclear tests by USSR; Fresh proposal by Gorbachev; Group 77 meet at Havana; Unsuccessful military coup in Philippines, Prime Minister of Lebanon killed"},
            {"1988", "WHO observes 7th of April as no smoking day, French President re-elected, Gen. Zia-ul-Haq killed in plane crash, Quake kills about 1,000 people in Bihar (India), George Bush elected President of USA, Arafat declares on independent state of Palestine, Nearly 1,00,000 people killed in earthquake in Armenia"},
            {"1989", "The UN Peace keeping force starts implementation of UN Resolution 435 for the independence of  Namibia"},
            {"1990", "The Panamanian President surrenders to the United States. South Africa lifts lean on African National Congress. Lithuania declares independence from the Soviet Union. Iraq overruns Kuwait. East and West Germanys unite"},
            {"1991", "War breaks out in the Gulf, With the defeat of Iraq and freedom of Kuwait, Gulf war ends"},
            {"1993", "5 new members of security council START II treaty between Russian & US Presidents, Security Council resolution on Angola, Emergency in Zambia, Elections in Australia"},
            {"1994", "South Africa emerged from aparted regime with Nelson Mandela as its president. GATT treaty signed to create World Trade Organisation (WTO)"},
            {"1995", "WTO comes into existence. Nuclear test by France. Balkan peace accord signed"},
            {"1996", "Kofi Annan new UN Secretary General. Clinton re-elected US President. India refuses to sign CTBT. Shekh Hasina Wajed new PM of Bangladesh"},
            {"1997", "Tony Blair back in power in UK. Mohd. Khatami elected president of Iran. Hong Kong goes back to China after 99 year British rule"},
            {"1998", "Indonesian President Suharto resigns. Pakistan test fires `Gauri' missile. US President Clinton faces impeachment"},
            {"1999", "G-15 Summit ends. Yugoslavia accepts a peace plan for Kosovo"}};
    int randomNumber;
    String player;
    int[] inputNumbers = new int[0];

    RandomNumber() {
        Random random = new Random();
        this.randomNumber = random.nextInt(this.dates.length);
        this.player = "Unknown player";
    }

    RandomNumber(String name) {
        Random random = new Random();
        this.randomNumber = random.nextInt(this.dates.length);
        this.player = name;
    }

    public void guessNumber () throws IOException {

        String lessMessage = "Your number is too small. Please, try again.";
        String moreMessage = "Your number is too big. Please, try again.";
        String finishMessage = "Congratulations, " + this.player + "!";

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int inputNumber = -1;
        int date = Integer.parseInt(this.dates[this.randomNumber][0]);
        System.out.println("This date: " + this.dates[this.randomNumber][1]);

        while (inputNumber != date) {
            try {
                inputNumber = Integer.parseInt(reader.readLine());
                if (inputNumber == date) break;
                System.out.println(inputNumber < date ? lessMessage : moreMessage);
                this.addNumberToArray(inputNumber);
            } catch (Exception e) {
                System.out.println("It's not a correct value. Please, try again!");
            }
        }

        System.out.println(finishMessage);
        System.out.println("Your numbers: " + Arrays.toString(this.sortArray(this.inputNumbers)));
    }

    private void addNumberToArray(int number) {
        int[] temp = new int[this.inputNumbers.length + 1];

        System.arraycopy(this.inputNumbers, 0, temp, 0, this.inputNumbers.length);

        temp[temp.length - 1] = number;
        this.inputNumbers = temp;
    }

    private int[] sortArray(int[] array) {
        for (int left = 0; left < array.length; left++) {
            int value = array[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value > array[i]) {
                    array[i + 1] = array[i];
                } else {
                    break;
                }
            }
            array[i + 1] = value;
        }
        return array;
    }
}
