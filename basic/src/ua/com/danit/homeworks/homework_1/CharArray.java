package ua.com.danit.homeworks.homework_1;

import java.util.ArrayList;
import java.util.Arrays;

public class CharArray {
    StringBuilder stringArray = new StringBuilder();
    String standardString = "";
    StringBuilder charArray = new StringBuilder();
    byte[] byteArray = new byte[0];
    int[] intArray = new int[0];
    ArrayList<Integer> list = new ArrayList<Integer>();

    public static void main(String[] args) {
        CharArray charArray = new CharArray();

        //StringBuilder
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                charArray.stringArray.append(j).append(" ");
            }
        }
        long end = System.currentTimeMillis();
        System.out.print("StringBuilder: ");
        System.out.println(end - start);

        //String
        start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                charArray.standardString += j + " ";
            }
        }
        end = System.currentTimeMillis();
        System.out.print("String: ");
        System.out.println(end - start);

        //Integer
        start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                int[] temp = Arrays.copyOfRange(charArray.intArray, 0, charArray.intArray.length + 1);
                temp[temp.length - 1] = j;
                charArray.intArray = temp;
            }
        }
        end = System.currentTimeMillis();
        System.out.print("Integer: ");
        System.out.println(end - start);

        //Character
        start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                charArray.charArray.append((char)j);
            }
        }
        end = System.currentTimeMillis();
        System.out.print("Character: ");
        System.out.println(end - start);

        //ArrayList
        start = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                charArray.list.add(j);
            }
        }
        end = System.currentTimeMillis();
        System.out.print("ArrayList: ");
        System.out.println(end - start);

    }
}
