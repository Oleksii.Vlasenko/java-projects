package ua.com.danit.homeworks.homework_11.dao;

import ua.com.danit.homeworks.homework_11.entity.Family;

import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    boolean saveFamily(Family family);
}
