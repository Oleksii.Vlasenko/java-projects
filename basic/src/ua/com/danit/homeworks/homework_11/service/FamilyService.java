package ua.com.danit.homeworks.homework_11.service;

import ua.com.danit.homeworks.homework_11.dao.CollectionFamilyDao;
import ua.com.danit.homeworks.homework_11.dao.FamilyDao;
import ua.com.danit.homeworks.homework_11.entity.Family;
import ua.com.danit.homeworks.homework_11.entity.Human;
import ua.com.danit.homeworks.homework_11.entity.Pet;
import ua.com.danit.homeworks.homework_11.entity.humans.Man;
import ua.com.danit.homeworks.homework_11.entity.humans.Woman;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyService {

    private final CollectionFamilyDao collectionFamilyDao;

    public FamilyService(CollectionFamilyDao collectionFamilyDao) {
        this.collectionFamilyDao = collectionFamilyDao;
    }

    private List<Family> getAllFamilies() {
        return this.collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        this.collectionFamilyDao.getAllFamilies().forEach(f -> System.out.println(f.toString()));
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return this.collectionFamilyDao.getAllFamilies().stream()
                .filter(f -> f.countFamily() > count)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int count) {
        return this.collectionFamilyDao.getAllFamilies().stream()
                .filter(f -> f.countFamily() < count)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int count) {
        return (int) this.collectionFamilyDao.getAllFamilies().stream()
                .filter(f -> f.countFamily() == count).count();
    }

    public boolean createNewFamily(Human mother, Human father) {
        return this.collectionFamilyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return this.collectionFamilyDao.deleteFamily(index);
    }

    public boolean bornChild(Family family, String mName, String fName) throws ParseException {

        if (family.getMother() == null || family.getFather() == null) {
            return false;
        }
        Random random = new Random();
        Human child;
        int iq = (family.getFather().getIq() + family.getMother().getIq()) / 2;
        if (random.nextInt(2) == 1) {
            String name = mName;
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            child = new Man(name, family.getFather().getSurname(), "11/05/2000", iq, null);
        } else {
            String name = fName;
            name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
            child = new Woman(name, family.getFather().getSurname(), "11/05/2000", iq, null);
        }
        family.addChild(child);
        return true;
    }

    public boolean adoptChild(Family family, Human human) {
        boolean flag = false;
        for (Family currFamily : this.collectionFamilyDao.getAllFamilies()) {
            if (family.equals(currFamily)) {
                currFamily.addChild(human);
                flag = true;
                break;
            }
        }
        return flag;
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.collectionFamilyDao.getAllFamilies()
                .forEach(f -> {
                    List<Human> childrenToRemove = f.getChildren().stream()
                            .filter(child -> Integer.parseInt(child.describeAge().split("-")[0]) > age)
                            .collect(Collectors.toList());
                    childrenToRemove.forEach(f::deleteChild);
                });
    }

    public int count() {
        return this.collectionFamilyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return this.collectionFamilyDao.getFamilyByIndex(index);
    }

    public HashSet<Pet> getPets(int index) {
        return this.collectionFamilyDao.getFamilyByIndex(index).getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        this.collectionFamilyDao.getFamilyByIndex(familyIndex).addPet(pet);
    }

    public FamilyDao getFamilyDao() {
        return this.collectionFamilyDao;
    }
}
