package ua.com.danit.homeworks.homework_11.entity.humans;

import ua.com.danit.homeworks.homework_11.entity.Human;

public interface HumanCreator {
    Human bornChild(Human human) throws Exception;
}
