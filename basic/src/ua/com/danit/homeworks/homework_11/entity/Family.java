package ua.com.danit.homeworks.homework_11.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class Family {

    private final Human mother;
    private final Human father;
    private final ArrayList<Human> children = new ArrayList<Human>();
    private HashSet<Pet> pets = new HashSet<Pet>();

    public ArrayList<Human> getChildren() {
        return children;
    }

    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return father;
    }

    public HashSet<Pet> getPets() {
        return pets;
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
    }

    static {
        System.out.println("Download the Family class");
    }

    {
        System.out.println("Create the Family object");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Family(Human mother, Human father, HashSet<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.pets = pets;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        for (Pet pet : this.pets) {
            pet.setFamily(this);
        }
    }

    public void addChild(Human child) {
        this.children.add(child);
        child.setFamily(this);
    }

    public void deleteChild(Human child) {
        this.children.remove(child);
    }

    public void deleteChild(int index) {
        this.children.remove(index);
    }

    public int countFamily() {
        int count = 0;
        if (this.mother != null) {
            count++;
        }
        if (this.father != null) {
            count++;
        }
        for (Human child : children) {
            if (child != null) {
                count++;
            }
        }
        return count;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        if (this.mother != null) {
            str.append(this.mother.toString());
        }
        if (this.father != null) {
            str.append('\n').append(this.father.toString());
        }
        if (this.children.size() > 0) {
            for (Human child : this.children) {
                str.append('\n').append(child.toString());
            }
        }
        for (Pet pet : pets) {
            str.append('\n').append(pet.toString());
        }
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                children.equals(family.children) &&
                Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pets);
        result = 31 * result + Arrays.hashCode(new ArrayList[]{children});
        return result;
    }

    @Override
    public void finalize() {
        System.out.println("Family object deleted!");
    }
}
