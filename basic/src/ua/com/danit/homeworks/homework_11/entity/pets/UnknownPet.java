package ua.com.danit.homeworks.homework_11.entity.pets;

import ua.com.danit.homeworks.homework_11.entity.Pet;

public class UnknownPet extends Pet {
    public UnknownPet() {
        super("UNKNOWN");
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я не знаю кто я, но соскучился!)");
    }
}
