package main;

import java.util.*;
import java.util.stream.Collector;

public class MyClass {

    protected List some() {
        return null;
    }
}

class MyChild extends MyClass {
    int a;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyChild myChild = (MyChild) o;
        return a == myChild.a;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a);
    }

    @Override
    public ArrayList some() {
        super.some();
        return null;
    }
}
