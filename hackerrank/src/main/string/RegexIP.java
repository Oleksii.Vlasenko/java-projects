package main.string;

import java.util.Scanner;
import java.util.regex.Pattern;

public class RegexIP {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            String IP = in.next();
            System.out.println(IP.matches(new MyRegex().pattern));
        }

    }
}

class MyRegex {
    private final String ipNum = "([01]?[0-9]{1,2}|2?[0-4][0-9]|25[0-5])";
    public final String pattern = ipNum + "\\." + ipNum + "\\." + ipNum + "\\." + ipNum;
}
