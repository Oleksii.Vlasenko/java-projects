package main.string;

import java.util.Scanner;
import java.util.regex.*;

public class PatternSyntaxChecker {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());
        int i = 0;
        String[] outputs = new String[testCases];
        while (testCases > 0) {
            try {
                testCases--;
                String pattern = in.nextLine();
                Pattern.compile(pattern);
                outputs[i++] = "Valid";
            } catch (Exception e) {
                outputs[i++] = "Invalid";
            }
        }
        for (String item : outputs) {
            System.out.println(item);
        }
    }
}


