package main.string;

import java.io.*;
import java.util.*;

public class Tokens {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        if (s.length() > 0) {
            String[] arr = s.trim().split("[!,?._ '@]+");
            if (arr.length == 0) {
                System.out.println(0);
            } else {
                int start = 0;
                int end = 0;
                if (arr[0].equals("")) start++;
                if (arr[arr.length - 1].equals("")) end++;
                System.out.println(arr.length - start - end);
                for (String item : arr) {
                    if (!item.equals("")) System.out.println(item);
                }
            }
        } else {
            System.out.println(0);
        }
        scan.close();
    }
}
