package main.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IpExample {
    private class IP {
        public int a;
        public int b;
        public int c;
        public int d;

        IP(int a, int b, int c, int d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }

    private List<IP> ips;

    IpExample(ArrayList<IP> ips) {
        this.ips = ips;
    }

    private Set<Long> uniqueIp2(List<IP> ips) {
        return ips.stream().map(i -> {
            long ip = i.d;
            ip = ip << 8 + i.c;
            ip = ip << 8 + i.b;
            ip = ip << 8 + i.a;
            return ip;
        }).collect(Collectors.toSet());
    }

    private static long pow(int a, int x) {
        return IntStream.rangeClosed(1, x).reduce(1, (acc, i) -> acc * a);
    }

    private Set<Long> uniqueIp10(List<IP> ips) {
        return ips.stream().map(ip ->
                ip.d + ip.c * pow(256, 1) + ip.b * pow(256, 2) + ip.a * pow(256, 3)).collect(Collectors.toSet());
    }
}
