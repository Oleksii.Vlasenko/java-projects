<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>History</title>
</head>
<body>
<ul>
    <#list ops as op>
        <li>
            <p>${op}</p>
        </li>
    </#list>
</ul>
</body>
</html>
