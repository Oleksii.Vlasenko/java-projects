package servlet;

import util.TemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class FileServletFreemarker2 extends HttpServlet {

    /**
     * http://localhost:8080/fm
     *
     */

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // TemplateEngine.folder("...") - from project
        // TemplateEngine.resources("...") - from resources
        TemplateEngine engine = TemplateEngine.resources("/content");

        HashMap<String, Object> data = new HashMap<>();
        data.put("name", "Jim");

        List<Student> students = Arrays.asList(
                new Student("Den", 20, "FS11"),
                new Student("Jim", 19, "FS12"),
                new Student("John", 20, "FS11"),
                new Student("Rick", 19, "FS12"),
                new Student("Gio", 18, "FS13")
        );
        data.put("students", students);
        data.put("total", students.size());

        engine.render("dynamic2.ftl", data, resp);
    }
}
