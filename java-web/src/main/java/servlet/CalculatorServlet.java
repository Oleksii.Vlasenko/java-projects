package servlet;

import domain.COAdd;
import domain.COSub;
import domain.CalcOperation;
import model.OperationLog;
import domain.CalcService;
import service.Storage;
import util.Params;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class CalculatorServlet extends HttpServlet {

    /**
     * /calc --- POST: /calc ---> OK
     * /extra/calc --- POST: /calc ---> ERR
     * /extra/calc --- POST: calc ---> OK
     */

    private final CalcService calc = new CalcService();
    private final Storage storage;
    static final CalcOperation add = new COAdd();
    static final CalcOperation sub = new COSub();

    public CalculatorServlet(Storage storage) {
        this.storage = storage;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (ServletOutputStream os = resp.getOutputStream()) {
            URI uri = this.getClass().getClassLoader().getResource("calc.html").toURI();
            Path path = Paths.get(uri);
            Files.copy(path, os);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private String represent(int a, int b, String op, int r) {
        return String.format("%d %d = %d", a, b, r);
    }

    private Optional<CalcOperation> parseOp(Optional<String> op) {
        return op.flatMap(s -> {
            switch (s) {
                case "plus": return Optional.of(add);
                case "minus": return Optional.of(sub);
                default: return Optional.empty();
            }
        });
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Integer> a = Params.getIntParam("a", req);
        Optional<Integer> b = Params.getIntParam("b", req);
        Optional<CalcOperation> op = parseOp(Params.getStrParam("operation", req));

        Optional<String> result = a.flatMap(ai ->
                b.flatMap(bi ->
                        op.map(opr -> {
                            int r = opr.doOp(ai, bi);
                            storage.log(new OperationLog(ai, bi, opr.show(), r));
                            return represent(ai, bi, opr.show(), r);
                        })
                )
        );

        try(PrintWriter w = resp.getWriter()) {
            w.println(result.orElse("Missed data"));
        }

    }
}
