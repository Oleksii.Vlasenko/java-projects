package servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AnyFileFromAssetsServlet extends HttpServlet {
    private final String ASSETS_ROOT = "assets";

    /**
     * Paths.get("a", "b") => "a/b" (or "a\b" on Windows)
     * Paths.get("a/", "b") => "a/b" (or "a\b" on Windows)
     * Paths.get("a", "/b") => "a/b" (or "a\b" on Windows)
     * Paths.get("a/", "/b") => "a/b" (or "a\b" on Windows)
     */

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /**
         *  HTML: href="/css/style.css"
         *  => handler.addServlet(CSSServlet.class, "/assets/css/style.css");
         *
         *  req.getRequestURI() => "/css/style.css"
         *
         *  Paths.get(ASSETS_ROOT, rqName); => assets/css/style.css
         *
         */

        try (ServletOutputStream outputStream = resp.getOutputStream()) {
            String rqName = req.getRequestURI();
            Path fileNameFromAssets = Paths.get(ASSETS_ROOT, rqName);
            Path res = Paths.get(this.getClass().getClassLoader().getResource(fileNameFromAssets.toString()).toURI());
            Files.copy(res, outputStream);
        } catch (URISyntaxException e) {
            resp.setStatus(400);
        }
    }
}
