package servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class FileServlet extends HttpServlet {

    /**
     * http://localhost:8080/file
     *
     */

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        URL resource = this.getClass().getClassLoader().getResource("1.html");
        try(ServletOutputStream os = resp.getOutputStream();) {
            Files.copy(Paths.get("1.html"), os);
        }
//        File file = new File("java-web/1.html");
//        try (BufferedReader reader = new BufferedReader(new FileReader(file));
//             PrintWriter writer = resp.getWriter()) {
//            String content = reader.lines().collect(Collectors.joining("\n"));
//            writer.println(content);
//        }
    }
}
