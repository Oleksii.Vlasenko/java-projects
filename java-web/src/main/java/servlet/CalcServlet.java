package servlet;

import domain.CalcService;
import util.Params;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

/**
 * http://localhost:8080/calc?a=5&b=6
 * http://localhost:8080/calc?a=5&a=6&b=7
 * Map<String, String[]> parameterMap = req.getParameterMap();
 */

public class CalcServlet extends HttpServlet {
    private final CalcService calc;

    public CalcServlet(CalcService calc) {
        this.calc = calc;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Integer> a = Params.getIntParam("a", req);
        Optional<Integer> b = Params.getIntParam("b", req);

        try (PrintWriter w = resp.getWriter()) {
            w.println(calc.add(a, b).map(String::valueOf).orElse("No values"));
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Optional<Integer> a = Params.getIntParam("a", req);
        Optional<Integer> b = Params.getIntParam("b", req);

        try (PrintWriter w = resp.getWriter()) {
            w.println(calc.add(a, b).map(String::valueOf).orElse("No values"));
        }
    }
}
