package servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public class ReqExplorerServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String a = req.getParameter("a"); // NULL!!!
        Map<String, String[]> parameterMap = req.getParameterMap();
        String header = req.getHeader("Autherization");
        Cookie[] cookies = req.getCookies();
        String method = req.getMethod();
        Collection<Part> parts = req.getParts();
        for (Part part : parts) {
            part.getContentType();
            part.getInputStream();
            part.getSubmittedFileName();
            part.getName();
        }
        String pathInfo = req.getPathInfo(); // without params
        String queryString = req.getQueryString(); // with params http://...x=5&y=5
        String contextPath = req.getContextPath(); // mounting point : http://localhost:8080/file -> /file

        // body for PUT, POST, etc.
        ServletInputStream inputStream = req.getInputStream();
    }
}
