import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import service.SQLStorage;
import service.Storage;
import servlet.*;

/**
     * public void doSomething(ArrayList<Integer> data) {
     * public void doSomething(List<Integer> data) {
     * public void doSomething(Collection<Integer> data) {
     * public void doSomething(Iterable<Integer> data) {
     *      data.remove();
     *      data.add();
     * }
     *
 */

public class App {
    public static void main(String[] args) throws Exception {
        Storage storage =
//                new InMemoryStorage();
                new SQLStorage();
        Server server = new Server(8080);
//        ServletContextHandler servletContextHandler = new ServletContextHandler();
//        servletContextHandler.addServlet(FileServlet.class, "/file");
//        servletContextHandler.addServlet(FileServletFreemarker.class, "/fm");
//        servletContextHandler.addServlet(FileServletFreemarker2.class, "/fm2");

//        CalcServlet calcServlet = new CalcServlet(new Calculator());
//        ServletHolder calcHolder = new ServletHolder(calcServlet);
//        servletContextHandler.addServlet(calcHolder, "/calc");

        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(IndexServlet.class, "/home");
        handler.addServlet(new ServletHolder(new CalculatorServlet(storage)), "/calc");
        handler.addServlet(new ServletHolder(new HistoryServlet(storage)), "/history");
        handler.addServlet(AnyFileFromAssetsServlet.class, "/css/*");
        handler.addServlet(AnyFileFromAssetsServlet.class, "/img/*");

        handler.addServlet(RedirectServlet.class, "/*");

        server.setHandler(handler);
        server.start();
        server.join();

    }
}
