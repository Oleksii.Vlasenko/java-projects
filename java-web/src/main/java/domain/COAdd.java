package domain;

public class COAdd implements CalcOperation {
    @Override
    public int doOp(int a, int b) {
        return a + b;
    }

    @Override
    public String show() {
        return "+";
    }
}

