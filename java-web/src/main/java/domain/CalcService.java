package domain;

import java.util.*;

public class CalcService {
    private final Calculator calc = new Calculator();

    public Optional<Integer> add(Optional<Integer> a, Optional<Integer> b) {
        return a.flatMap(ai -> b.map(bi -> calc.add(ai, bi)));
    }
    public Optional<Integer> sub(Optional<Integer> a, Optional<Integer> b) {
        return a.flatMap(ai -> b.map(bi -> calc.sub(ai, bi)));
    }

    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add("###");
        Iterator iterator = list.iterator();
        while(iterator.hasNext()) {
            Object element = iterator.next();
            System.out.println(element.toString() == "###");
        }

    }

}
