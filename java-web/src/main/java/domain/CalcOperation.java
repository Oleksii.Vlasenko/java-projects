package domain;

public interface CalcOperation {
    int doOp(int a, int b);
    String show();
}
