import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Month {

    public static void main(String[] args) throws IOException {
//        for (int i = 1; i < 13; i++) {
//            System.out.print(i + ":" + getMonth(i) + " ");
//        }

        List<Integer> list = new ArrayList<>();
        list.add(73);
        list.add(67);
        list.add(38);
        list.add(33);
        list.add(100);
        list.add(49);
        list.add(22);
        list.add(91);
        System.out.println(gradingStudents(list));
    }

    public static int getMonth(int month) {
        return 28 + (month - month / 8) % 2 + 2 % month + (1 / month) * 2;
    }

    public static List<Integer> gradingStudents(List<Integer> grades) {
        return grades.stream()
                .map(g -> (1 - g / 51) * (1 - g / 38) * (g % 38)
                        + ((g / 38 + 1) / 2)
                            * ((g % 5) / 3 * (g + 5 - g % 5) + (5 - g % 5) / 3 * g))
                .collect(toList());
    }
}
