package sql;

import model.OperationLog;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

public class SQLAppRead {

    /**
     * server: localhost
     * db_name: fs11
     * port: 5432 (pg) / 3306 (mysql)
     * username: postgres / root
     * password: admin / ""
     * http://127.0.0.1:50985/?key=d0e21f90-fccb-4968-a7d7-b3d4f86d9df5
     */

    public static void main(String[] args) throws SQLException {
        Conn.get().flatMap(conn -> {
            try {
                PreparedStatement stmt = conn.prepareStatement("select * from history");
                ResultSet resultSet = stmt.executeQuery();
                ArrayList<OperationLog> operationLogs = new ArrayList<>();
                while (resultSet.next()) {
                    int a = resultSet.getInt("a");
                    int b = resultSet.getInt("b");
                    int r = resultSet.getInt("r");
                    String op = resultSet.getString("op");
                    OperationLog operationLog = new OperationLog(a, b, op, r);
                    operationLogs.add(operationLog);
                }
                return Optional.of(operationLogs);
            } catch (SQLException ex) {
                return Optional.empty();
            }
        }).ifPresent(al -> al.forEach(System.out::println));
    }
}
