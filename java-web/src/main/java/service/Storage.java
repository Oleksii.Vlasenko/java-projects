package service;

import model.OperationLog;

public interface Storage {

    void log(OperationLog op);

    Iterable<OperationLog> get();
}
