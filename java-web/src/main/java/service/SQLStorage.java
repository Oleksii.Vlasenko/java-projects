package service;

import model.OperationLog;
import sql.Conn;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public class SQLStorage implements Storage {



    @Override
    public void log(OperationLog op) {
        Conn.get().ifPresent(conn -> {
            try {
                PreparedStatement stmt = conn.prepareStatement(
                        "INSERT INTO history(a, b, op, r) VALUES (?, ?, ?, ?);"
                );
                stmt.setInt(1, op.a);
                stmt.setInt(2, op.b);
                stmt.setString(3, op.op);
                stmt.setInt(4, op.r);
                stmt.execute();
            } catch(SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    @Override
    public Iterable<OperationLog> get() {
        return Conn.get().flatMap(conn -> {
            try {
                PreparedStatement stmt = conn.prepareStatement("SELECT * FROM history");
                ResultSet resultSet = stmt.executeQuery();
                ArrayList<OperationLog> operationLogs = new ArrayList<>();
                while (resultSet.next()) {
                    int a = resultSet.getInt("a");
                    int b = resultSet.getInt("b");
                    int r = resultSet.getInt("r");
                    String op = resultSet.getString("op");
                    OperationLog operationLog = new OperationLog(a, b, op, r);
                    operationLogs.add(operationLog);
                }
                return Optional.of(operationLogs);
            } catch (SQLException ex) {
                return Optional.empty();
            }
        }).orElse(new ArrayList<>());
    }
}
