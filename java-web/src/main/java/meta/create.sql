create table history
(
    id serial not null
        constraint history_pk
            primary key,
    a  integer,
    b  integer,
    op varchar,
    r  integer
);