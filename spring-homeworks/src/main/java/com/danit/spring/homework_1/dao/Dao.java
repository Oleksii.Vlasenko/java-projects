package com.danit.spring.homework_1.dao;

import java.util.List;

public interface Dao<T> {

    T save(T obj);
    void saveAll(List<T> entities);

    T getOne(long id);
    List<T> findAll();

    boolean delete(T obj);
    void deleteAll(List<T> entities);
    boolean deleteById(long id);

}
