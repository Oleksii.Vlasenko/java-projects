package com.danit.spring.homework_1.dto;

import lombok.Data;

@Data
public class OperationDto {
    private Long id;
    private Double amount;
    private Long destination;
}
