package com.danit.spring.homework_1.dao;

import com.danit.spring.homework_1.model.Customer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class CustomerDao implements Dao<Customer> {

    private final List<Customer> customers;
    private long nextId;

    public CustomerDao() {
        this.customers = new ArrayList<>();
        this.nextId = 1;
    }

    @Override
    public Customer save(Customer customer) {
        if (customer.getId() == null) {
            customer.setId(nextId++);
        } else {
            this.deleteById(customer.getId());
        }
        this.customers.add(customer);
        return customer;
    }

    @Override
    public void saveAll(List<Customer> cs) {
        cs.forEach(a -> {
            if (a.getId() == 0) {
                a.setId(nextId++);
            } else {
                this.deleteById(a.getId());
            }
            this.customers.add(a);
        });
    }

    @Override
    public Customer getOne(long id) {
        return customers.stream()
                .filter(c -> c.getId() == id)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<Customer> findAll() {
        return this.customers;
    }

    @Override
    public boolean delete(Customer c) {
        return this.customers.remove(c);
    }

    @Override
    public void deleteAll(List<Customer> cs) {
        this.customers.removeAll(cs);
    }

    @Override
    public boolean deleteById(long id) {
        return Optional.of(this.getOne(id))
                .map(this::delete)
                .orElse(false);
    }
}
