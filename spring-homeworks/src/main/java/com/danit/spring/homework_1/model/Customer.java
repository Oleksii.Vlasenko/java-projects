package com.danit.spring.homework_1.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Customer {

    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<Account> accounts;

    public Customer(String name, String email, int age) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.accounts = new ArrayList<>();
    }

    public void addAccount(Account account) {
        this.accounts.add(account);
    }
}
