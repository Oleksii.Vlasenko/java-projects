package com.danit.spring.homework_1.model;

public enum Currency {
    USD, EUR, UAH, CHF, GBP
}
