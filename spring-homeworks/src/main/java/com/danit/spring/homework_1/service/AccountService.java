package com.danit.spring.homework_1.service;

import com.danit.spring.homework_1.dao.AccountDao;
import com.danit.spring.homework_1.dto.AccountDto;
import com.danit.spring.homework_1.model.Account;
import com.danit.spring.homework_1.model.Currency;
import com.danit.spring.homework_1.model.Customer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class AccountService {

    private final AccountDao accountDao;

    public AccountDto get(long id) {
        return new AccountDto(this.accountDao.getOne(id));
    }

    public List<Account> getAll() {
        return this.accountDao.findAll();
    }

    public Account create(Currency currency, Customer customer) {
        return this.accountDao.save(new Account(currency, customer));
    }

    public boolean remove(long id) {
        return this.accountDao.deleteById(id);
    }

    public boolean topUp(long id, double amount) {
        try {
            Account account = this.accountDao.getOne(id);
            account.setBalance(account.getBalance() + amount);
            this.accountDao.save(account);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    public boolean withdraw(long id, double amount) {
        try {
            Account account = this.accountDao.getOne(id);
            if (account.getBalance() < amount) throw new IllegalArgumentException("not enough money");
            account.setBalance(account.getBalance() - amount);
            this.accountDao.save(account);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    public boolean transfer(Long to, Long from, Double amount) {
        try {
            Account accountTo = this.accountDao.getOne(to);
            Account accountFrom = this.accountDao.getOne(from);
            if (accountFrom.getBalance() < amount) throw new IllegalArgumentException("not enough money");
            accountFrom.setBalance(accountFrom.getBalance() - amount);
            accountTo.setBalance(accountTo.getBalance() + amount);
            this.accountDao.saveAll(List.of(accountTo, accountFrom));
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }
}
