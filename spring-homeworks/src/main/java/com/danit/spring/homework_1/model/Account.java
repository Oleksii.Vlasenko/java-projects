package com.danit.spring.homework_1.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Account {

    private long id;
    private String number;
    private Currency currency;
    private double balance;
    private Customer customer;

    public Account(Currency currency, Customer customer) {
        this.currency = currency;
        this.balance = 0d;
        this.number = UUID.randomUUID().toString();
        this.customer = customer;
    }
}
