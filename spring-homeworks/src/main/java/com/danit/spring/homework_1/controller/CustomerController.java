package com.danit.spring.homework_1.controller;

import com.danit.spring.homework_1.dto.AccountDto;
import com.danit.spring.homework_1.dto.CustomerDto;
import com.danit.spring.homework_1.model.Account;
import com.danit.spring.homework_1.model.Currency;
import com.danit.spring.homework_1.model.Customer;
import com.danit.spring.homework_1.service.AccountService;
import com.danit.spring.homework_1.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CustomerController {

    private final CustomerService customerService;
    private final AccountService accountService;

    @GetMapping("{id}")
    public CustomerDto get(@PathVariable("id") long id) {
        return this.customerService.get(id);
    }

    @GetMapping("")
    public List<CustomerDto> get() {
        return this.customerService.getAll();
    }

    @PostMapping("")
    public CustomerDto create(@RequestBody CustomerDto c) {
        Customer customer = new Customer(c.getName(), c.getEmail(), c.getAge());
        return this.customerService.create(customer);
    }

    @PutMapping("{id}")
    public CustomerDto update(@PathVariable("id") long id, @RequestBody CustomerDto c) {
        return this.customerService.update(id, c);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") long id) {
        this.customerService.delete(id);
    }

    @PostMapping("{id}")
    public AccountDto createAccount(@PathVariable("id") long id, @RequestBody Currency currency) {
        Customer customer = this.customerService.getCustomer(id);
        Account account = this.accountService.create(currency, customer);
        customer.addAccount(account);
        return new AccountDto(account);
    }

    @DeleteMapping("{id}/{accId}")
    public boolean removeAccount(@PathVariable("id") long id, @PathVariable("accId") long accId) {
        return this.customerService.removeAccount(id, accId);
    }






}
