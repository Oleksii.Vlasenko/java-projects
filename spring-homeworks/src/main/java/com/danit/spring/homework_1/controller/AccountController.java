package com.danit.spring.homework_1.controller;

import com.danit.spring.homework_1.dto.AccountDto;
import com.danit.spring.homework_1.dto.OperationDto;
import com.danit.spring.homework_1.model.Account;
import com.danit.spring.homework_1.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/accounts")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AccountController {

    private final AccountService accountService;

    @GetMapping("{id}")
    public AccountDto get(@PathVariable("id") long id){
        return this.accountService.get(id);
    }

    @PostMapping("topup")
    public boolean topUp1(@RequestBody OperationDto o) {
        return this.accountService.topUp(o.getId(), o.getAmount());
    }

    @PostMapping("withdraw")
    public boolean withdraw(@RequestBody OperationDto o) {
        return this.accountService.withdraw(o.getId(), o.getAmount());
    }

    @PostMapping("transfer")
    public boolean transfer1(@RequestBody OperationDto o) {
        return this.accountService.transfer(o.getDestination(), o.getId(), o.getAmount());
    }


}
