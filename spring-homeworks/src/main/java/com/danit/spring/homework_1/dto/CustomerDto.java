package com.danit.spring.homework_1.dto;

import com.danit.spring.homework_1.model.Customer;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Data
@NoArgsConstructor
public class CustomerDto {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<AccountDto> accounts;

    public CustomerDto(Customer customer) {
        this.id = customer.getId();
        this.name = customer.getName();
        this.email = customer.getEmail();
        this.age = customer.getAge();
        this.accounts = customer.getAccounts().stream().reduce(new ArrayList<>(), (acc, a) -> {
            acc.add(new AccountDto(a));
            return acc;
        }, (x, y) -> y);
    }
}
