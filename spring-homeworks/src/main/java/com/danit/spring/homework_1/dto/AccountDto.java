package com.danit.spring.homework_1.dto;

import com.danit.spring.homework_1.model.Account;
import com.danit.spring.homework_1.model.Currency;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountDto {
    private Long id;
    private String number;
    private Currency currency;
    private double balance;

    public AccountDto(Account account) {
        this.id = account.getId();
        this.number = account.getNumber();
        this.currency = account.getCurrency();
        this.balance = account.getBalance();
    }
}
