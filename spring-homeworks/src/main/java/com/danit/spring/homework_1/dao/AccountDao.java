package com.danit.spring.homework_1.dao;

import com.danit.spring.homework_1.model.Account;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class AccountDao implements Dao<Account> {

    private final List<Account> accounts;
    private long nextId;

    public AccountDao() {
        this.accounts = new ArrayList<>();
        this.nextId = 1;
    }

    public Account findByNumber(String number) {
        return this.accounts.stream()
                .filter(a -> a.getNumber().equals(number))
                .findAny()
                .orElseThrow(IllegalAccessError::new);
    }

    @Override
    public Account save(Account account) {
        if (account.getId() == 0) {
            account.setId(nextId++);
        } else {
            this.deleteById(account.getId());
        }
        this.accounts.add(account);
        return account;
    }

    @Override
    public void saveAll(List<Account> as) {
        as.forEach(a -> {
            if (a.getId() == 0) {
                a.setId(nextId++);
            } else {
                this.deleteById(a.getId());
            }
            this.accounts.add(a);
        });
    }

    @Override
    public Account getOne(long id) {
        return this.accounts.stream()
                .filter(a -> a.getId() == id)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<Account> findAll() {
        return this.accounts;
    }

    @Override
    public boolean delete(Account account) {
        return this.accounts.remove(account);
    }

    @Override
    public void deleteAll(List<Account> as) {
        this.accounts.removeAll(as);
    }

    @Override
    public boolean deleteById(long id) {
        return Optional.of(this.getOne(id))
                .map(this::delete)
                .orElse(false);
    }
}
