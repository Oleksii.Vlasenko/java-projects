package ua.com.danit.classwork.lesson_5;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class TwoPointers {

    public static boolean isUnique(List<Integer> list, List<Integer> base) {
        return list.containsAll(base.stream().distinct().collect(Collectors.toList()));
    }

    public static List<Integer> searchPath(List<Integer> list) {
        return search(list, list);
    }

    private static List<Integer> search(List<Integer> list, List<Integer> base) {

        if (!isUnique(list, base) || base.isEmpty()) {
            return base;
        }

        int left = 0, right = 1;

        while(!isUnique(list.subList(left, right), base) && right < list.size()) {
            right++;
        }

        while(isUnique(list.subList(left + 1, right), base) && left < right) {
            left++;
        }

        List<Integer> nextList = search(list.subList(left + 1, list.size()), base);

        return list.subList(left, right).size() < nextList.size()
                ? list.subList(left, right)
                : nextList;
    }
}

class TwoPointersSpec {

    @Test
    public void test1() {
        List<Integer> list = List.of(13);
        List<Integer> result = TwoPointers.searchPath(list);
        assertEquals(List.of(13), result);
    }

    @Test
    public void test2() {
        List<Integer> list = List.of(13, 13, 13);
        List<Integer> result = TwoPointers.searchPath(list);
        assertEquals(List.of(13), result);
    }

    @Test
    public void test3() {
        List<Integer> list = List.of(7, 3);
        List<Integer> result = TwoPointers.searchPath(list);
        assertEquals(List.of(7, 3), result);
    }

    @Test
    public void test4() {
        List<Integer> list = List.of(7,3,7,3,1,3,4,1);
        List<Integer> result = TwoPointers.searchPath(list);
        assertEquals(List.of(7,3,1,3,4), result);
    }

    @Test
    public void test5() {
        List<Integer> list = List.of(2,1,1,3,2,1,1,3);
        List<Integer> result = TwoPointers.searchPath(list);
        assertEquals(List.of(3, 2, 1), result);
    }

    @Test
    public void test6() {
        List<Integer> list = List.of(7,5,2,7,2,7,4,7);
        List<Integer> result = TwoPointers.searchPath(list);
        assertEquals(List.of(5, 2, 7, 2, 7, 4), result);
    }

    @Test
    public void test7() {
        List<Integer> list = List.of(7,5,1,1,1,1,7,5);
        List<Integer> result = TwoPointers.searchPath(list);
        assertEquals(List.of(1, 7, 5), result);
    }

    @Test
    public void test8() {
        List<Integer> list = List.of(3,1,1,1,1,1,2,3);
        List<Integer> result = TwoPointers.searchPath(list);
        assertEquals(List.of(1, 2, 3), result);
    }

}
