package ua.com.danit.classwork.lesson_7;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class GraphImpl implements Graph {
    private final Set<Integer>[] data;

    public GraphImpl(int v) {
        this.data = (Set<Integer>[]) new Set[v];
        for (int i = 0; i < v; i++) {
            data[i] = new HashSet<>();
        }
    }

    @Override
    public int vertexCount() {
        return data.length;
    }

    @Override
    public Set<Integer> adjTo(int vertex) {
        return Collections.unmodifiableSet(data[vertex]);
    }

    @Override
    public void add(int v, int w) {
        data[v].add(w);
    }

    @Override
    public void remove(int v, int w) {
        data[v].remove(w);
    }

    @Override
    public boolean isConnected(int v, int w) {
        return data[v].contains(w);
    }

    @Override
    public String toString() {
        return IntStream.range(0, vertexCount())
                .filter(v -> !data[v].isEmpty())
                .mapToObj(v -> data[v].stream().map(w -> String.format("%d -> %d", v, w)).collect(Collectors.joining(", ")))
                .collect(Collectors.joining("\n"));
    }
}

class GraphSpec {
    @Test
    public void test1() {
        Graph graph = new GraphImpl(10);
        assertEquals(10, graph.vertexCount());
    }

    @Test
    public void test2() {
        Graph graph = new GraphImpl(10);
        graph.add(0, 1);
        graph.add(0, 2);
        graph.add(2, 1);
        graph.add(2, 3);
        graph.add(3, 2);
        graph.add(4, 1);
        System.out.println(graph);
        assertEquals(10, graph.vertexCount());
    }


}