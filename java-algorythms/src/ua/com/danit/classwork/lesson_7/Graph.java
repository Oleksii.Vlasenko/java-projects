package ua.com.danit.classwork.lesson_7;

import java.util.Set;

public interface Graph {

    int vertexCount();
    Set<Integer> adjTo(int vertex);
    void add(int v, int w);
    void remove(int v, int w);
    boolean isConnected(int v, int w);
}
