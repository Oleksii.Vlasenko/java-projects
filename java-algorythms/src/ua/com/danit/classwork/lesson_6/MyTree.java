package ua.com.danit.classwork.lesson_6;

public class MyTree<K extends Comparable<K>, V> {

    static class Node<K extends Comparable<K>, V> {
        K key;
        V value;
        Node<K, V> left;
        Node<K, V> right;
        int n = 1;

        public Node(K key) {
            this.key = key;
        }

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        void updateN() {
            this.n = (this.left == null ? 0 : this.left.n)
                    + (this.right == null ? 0 : this.right.n)
                    + 1;
        }
    }

    Node<K, V> root = null;

    void add(K k) {

    }

    boolean contains_r(K key) {
        
        return true;
    }

    boolean contains_v(K key) {

        return false;
    }
}
