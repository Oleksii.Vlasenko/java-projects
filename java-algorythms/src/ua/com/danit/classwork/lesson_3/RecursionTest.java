package ua.com.danit.classwork.lesson_3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RecursionTest {

    @Test
    void taskA() {
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, Recursion.taskA(10).toArray());
    }

    @Test
    void taskB1() {
        assertArrayEquals(new int[]{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}, Recursion.taskB(10, 20).toArray());
    }

    @Test
    void taskB2() {
        assertArrayEquals(new int[]{5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5}, Recursion.taskB(5, -5).toArray());
    }

    @Test
    void taskB3() {
        assertArrayEquals(new int[]{5}, Recursion.taskB(5, 5).toArray());
    }

//    @Test
//    void taskC1() {
//        assertEquals(5, Recursion.taskC(5, 5));
//    }
//
//    @Test
//    void taskC2() {
//        assertEquals(15, Recursion.taskC(5, 15));
//    }

}