package ua.com.danit.classwork.lesson_3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    public static int factR(int n) {
        if (n == 1) return 1;
        return n * factR(n - 1);
    }

    public static int factS(int n) {
        return IntStream.rangeClosed(1, n).reduce(1, (a, b) -> a * b);
    }

    public static int factI(int n) {
        int r = 1;
        for (int i = 1; i <= n; i++) {
            r = r * i;
        }
        return r;
    }

    public static int countryCount(int[][] arr) {
        int count = arr.length * arr[0].length;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i == 0) {
                    if (j < arr[i].length - 1) {
                        if (arr[i][j] == arr[i][j + 1]) count--;
                    }
                } else {
                    if (arr[i][j] == arr[i - 1][j]) {
                        count--;
                        if (j > 0) {
                            if (arr[i][j] == arr[i - 1][j - 1] && arr[i][j] == arr[i][j - 1]) {
                                count++;
                            }
                        }
                    }
                    if (j < arr[i].length - 1) {
                        if (arr[i][j] == arr[i][j + 1]) count--;
                    }
                }
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int[] index = step(new int[][]{
                {1, 1, 1, 2},
                {1, 2, 1, 2},
                {3, 2, 1, 1},
                {1, 2, 2, 2},
                {3, 1, 4, 4}},
                0, 0);
        System.out.println(Arrays.toString(index));
    }

    public static int[] step(int[][] arr, int i, int j) {
        if (j + 1 < arr[i].length && arr[i][j] == arr[i][j + 1]) {
            System.out.println(i + " - " + (j + 1));
            step(arr, i, j + 1);
        } else {
            return new int[]{i, j + 1};
        }
//        if (j - 1 >= 0 && arr[i][j] == arr[i][j - 1])
//            System.out.println(i + " - " + (j - 1));
        if (i + 1 < arr.length && arr[i][j] == arr[i + 1][j]) {
            System.out.println((i + 1) + " - " + j);
            step(arr, i + 1, j);
        }
//        if (i - 1 >= 0 && arr[i][j] == arr[i - 1][j])
//            System.out.println((i - 1) + " - " + j);
        return new int[]{i, j};
    }

    public static ArrayList<Integer> easyNumbers(int n, ArrayList<Integer> list) {
        if (n == 2) {
            list.add(n);
            return list;
        }
        boolean isEasy = true;
        for (int item : easyNumbers(n - 1, list)) {
            if (n % item == 0) {
                isEasy = false;
                break;
            }
        }
        if (isEasy) list.add(n);
        return list;
    }

    public static Stream<Integer> easyNumbersS(int n) {
        if (n == 2) {
            return Stream.of(n);
        }
        return easyNumbersS(n - 1).anyMatch(i -> n % i == 0)
                ? easyNumbersS(n - 1)
                : Stream.concat(easyNumbersS(n - 1), Stream.of(n));
    }

}
