package ua.com.danit.classwork.lesson_3;

import java.util.Optional;
import java.util.stream.IntStream;

public class Recursion {

    public static IntStream taskA(int i) {
        if (i == 1) {
            return IntStream.of(i);
        }
        return IntStream.concat(taskA(i - 1), IntStream.of(i));
    }

    public static IntStream taskB(int a, int b) {
        if (a == b) {
            return IntStream.of(b);
        }
        return IntStream.concat(IntStream.of(a), taskB(a < b ? a + 1 : a - 1, b));

    }

    public static int taskC(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (m > 0 && n == 0) {
            return taskC(m - 1, 1);
        } else {
            return taskC(m - 1, taskC(m, n - 1));
        }
    }
}
