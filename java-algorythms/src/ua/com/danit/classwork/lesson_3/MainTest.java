package ua.com.danit.classwork.lesson_3;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void countryCount1() {
        assertEquals(3, Main.countryCount(new int[][]{{1, 1, 3}, {1, 1, 2}, {2, 2, 2}, {2, 2, 2}}));
    }

    @Test
    void countryCount2() {
        assertEquals(5, Main.countryCount(new int[][]{{1, 4, 3}, {2, 4, 2}, {2, 1, 2}, {2, 2, 2}}));
    }

    @Test
    void countryCount3() {
        assertEquals(4, Main.countryCount(new int[][]{{1, 1, 3}, {1, 3, 1}, {1, 2, 1}, {1, 1, 1}}));
    }

    @Test
    void countryCount4() {
        assertEquals(2, Main.countryCount(new int[][]{{3}, {2}, {2}, {2}}));
    }

    @Test
    void countryCount5() {
        assertEquals(2, Main.countryCount(new int[][]{{1, 1}, {1, 1}, {2, 2}, {2, 2}}));
    }

    @Test
    void countryCount6() {
        assertEquals(3, Main.countryCount(new int[][]{{1, 1, 1}, {3, 1, 3}, {3, 3, 3}, {1, 1, 1}}));
    }

    @Test
    void countryCount7() {
        assertEquals(7, Main.countryCount(new int[][]{{1, 1, 2, 2}, {1, 3, 2, 2}, {4, 4, 4, 1}, {3, 3, 3, 1}, {1, 3, 1, 1}}));
    }

//    @Test
//    void countryCount8() {
//        assertEquals(8, Main.countryCount(new int[][]{{1, 1, 2, 2}, {1, 2, 2, 2}, {3, 2, 3, 2}, {1, 2, 2, 2}, {3, 1, 4, 4}}));
//    }

    @Test
    void easyNumbers1() {
        assertArrayEquals(new Integer[]{2, 3, 5, 7, 11, 13, 17, 19}, Main.easyNumbers(20, new ArrayList<>()).toArray());
    }

    @Test
    void easyNumbers2() {
        assertArrayEquals(new Integer[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37}, Main.easyNumbers(40, new ArrayList<>()).toArray());
    }

    @Test
    void easyNumbers3() {
        assertArrayEquals(new Integer[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47}, Main.easyNumbers(50, new ArrayList<>()).toArray());
    }

//    @Test
//    void easyNumbersS1() {
//        assertArrayEquals(new Integer[]{2, 3, 5, 7, 11, 13, 17, 19}, Main.easyNumbersS(20, new ArrayList<>()).toArray());
//    }
//
//    @Test
//    void easyNumbersS2() {
//        assertArrayEquals(new Integer[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37}, Main.easyNumbersS(40, new ArrayList<>()).toArray());
//    }
//
//    @Test
//    void easyNumbersS3() {
//        assertArrayEquals(new Integer[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47}, Main.easyNumbersS(50, new ArrayList<>()).toArray());
//    }
}