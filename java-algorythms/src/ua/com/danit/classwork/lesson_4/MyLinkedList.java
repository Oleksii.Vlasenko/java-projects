package ua.com.danit.classwork.lesson_4;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class MyLinkedList<E extends Comparable<E>> {

    class Node<E> {
        final E value;
        Node<E> next;

        Node(E value) {
            this.value = value;
        }
    }

    private Node<E> head = null;

    int size;

    public boolean contains(E value) {
        Node<E> curr = head;
        while (curr != null) {
            if (value == curr.value) return true;
            curr = curr.next;
        }
        return false;
    }

    public List<E> toList() {
        ArrayList<E> list = new ArrayList<>();
        Node curr = head;
        while (curr != null) {
            list.add((E) curr.value);
            curr = curr.next;
        }
        return list;
    }

    public void addHead(E value) {
        Node node = new Node(value);
        node.next = head;
        head = node;
    }

    public void addTail(E value) {
        if (head == null) {
            addHead(value);
            return;
        }
        Node node = new Node(value);

        Node curr = head;
        while (curr.next != null) {
            curr = curr.next;
        }
        curr.next = node;
    }

    public void addBefore(E value, E target) {
        if (!contains(value)) {
            throw new NoSuchElementException();
        }

        Node curr = head;

        while (curr.next != null) {
            if (curr.next.value.equals(target)) {
                Node node = new Node(value);
                Node temp = curr.next;
                curr.next = node;
                node.next = temp;
                return;
            }
            curr = curr.next;
        }

        throw new NoSuchElementException();
    }

    public void addAfter(E target, E value) {
        if (!contains(value)) throw new NoSuchElementException();

        Node node = new Node(target);

        Node curr = head;
        while (curr.next != null) {
            if (curr.value == value) {
                node.next = curr.next;
                curr.next = node;
            }
            curr = curr.next;
        }
    }

    public int lengthITR() {
        if (head == null) {
            return 0;
        }
        int count = 0;
        Node curr = head;
        while(curr != null) {
            count++;
            curr = curr.next;
        }
        return count;
    }

    private int lengthR(Node curr) {
        if (curr == null) {
            return 0;
        }
        return 1 + lengthR(curr.next);
    }

    public int lengthR() {
        if (head == null) {
            return 0;
        }
        return lengthR(head);
    }

    private int lengthTR(Node curr, int count) {
        if (curr == null) {
            return count;
        }
        return lengthTR(curr.next, 1 + count);
    }

    public int lengthTR() {
        return lengthTR(head, 0);
    }

    public void remove(E value) {
        if (!contains(value)) throw new NoSuchElementException();
        Node curr = head;
        while(curr.next != null) {
            if (curr.next.value == value) {
                curr.next = curr.next.next;
                return;
            }
            curr = curr.next;
        }
        throw new NoSuchElementException();
    }

    public void removeBefore(E target, E value) {
        if (!contains(target) || !contains(value)) throw new NoSuchElementException();

        Node curr = head;

        while(curr.next.next != null) {
            if (curr.next.next.value == value && curr.next.value == target) {
                curr.next = curr.next.next;
                return;
            }
            curr = curr.next;

        }
        throw new NoSuchElementException();
    }

    public void removeAfter(E value, E target) {
        if (!contains(target) || !contains(value)) throw new NoSuchElementException();

        Node curr = head;

        while(curr.next != null) {
            if (curr.value == value && curr.next.value == target) {
                curr.next = curr.next.next;
                return;
            }
            curr = curr.next;
        }
        throw new NoSuchElementException();
    }

    private void addAfter(Node curr, E value, E target) {
        if (curr.value == value) {
            Node node = new Node(target);
            node.next = curr.next;
            curr.next = node;
            return;
        }
        addAfter(curr.next, value, target);
    }

    public void addAfterR(E value, E target) {
        if (!contains(value)) throw new NoSuchElementException();
        addAfter(head, value, target);
    }

    public void addInOrder(E value, boolean isDefault) {

    }

    public Node merge(Node second) {
        throw new IllegalArgumentException();
    }

    private Node reverse(Node curr) {
        if (curr.next == null) {
            head = curr;
            return curr;
        }
        reverse(curr.next).next = curr;
        return curr;
    }

    public void reverse() {
        if (head == null) return;
        reverse(head).next = null;
    }

}

class MyLinkedListSpec {

    @Test
    public void test1() {
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        assertFalse(myll.contains(10));
    }

    @Test
    public void test2() {
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        assertEquals(new ArrayList<Integer>(), myll.toList());
    }

    @Test
    public void test3() {
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        myll.addHead(5);
        assertEquals(Arrays.asList(5), myll.toList());
    }

    @Test
    public void test5_before() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 2, 7, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.addBefore(2, 7);
        assertEquals(after, myll.toList());
    }

    @Test
    public void test6_before() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 7, 2, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.addBefore(2, 2);
        assertEquals(after, myll.toList());
    }

    @Test
    public void test7_after() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 2, 7, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.addAfter(2, 4);
        assertEquals(after, myll.toList());
    }

    @Test
    public void test8_lengthITR() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 2, 7, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before.size(), myll.lengthITR());
        myll.addAfter(2, 4);
        assertEquals(after.size(), myll.lengthITR());
    }

    @Test
    public void test9_lengthR() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 2, 7, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before.size(), myll.lengthR());
        myll.addAfter(2, 4);
        assertEquals(after.size(), myll.lengthR());
    }

    @Test
    public void test10_lengthTR() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 2, 7, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before.size(), myll.lengthTR());
        myll.addAfter(2, 4);
        assertEquals(after.size(), myll.lengthTR());
    }

    @Test
    public void test11_remove() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.remove(7);
        assertEquals(after, myll.toList());
    }

    @Test
    public void test12_removeBefore() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.removeBefore(7, 2);
        assertEquals(after, myll.toList());
    }

    @Test
    public void test13_removeAfter() {
        List<Integer> before = Arrays.asList(4, 7, 2);
        List<Integer> after = Arrays.asList(4, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.removeAfter(4, 7);
        assertEquals(after, myll.toList());
    }

    @Test
    public void test14_addAfterR() {
        List<Integer> before = Arrays.asList(4, 1, 7, 2);
        List<Integer> after = Arrays.asList(4, 1, 7, 5, 2);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.addAfterR(7, 5);
        assertEquals(after, myll.toList());
    }

    @Test
    public void test15_reverse() {
        List<Integer> before = Arrays.asList(4, 1, 7, 2);
        List<Integer> after = Arrays.asList(2, 7, 1, 4);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.reverse();
        assertEquals(after, myll.toList());
    }

    @Test
    public void test16_reverse() {
        List<Integer> before = Arrays.asList(4);
        List<Integer> after = Arrays.asList(4);
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        before.forEach(x -> {
            myll.addTail(x);
        });
        assertEquals(before, myll.toList());
        myll.reverse();
        assertEquals(after, myll.toList());
    }

    @Test
    public void test17_reverse() {
        MyLinkedList<Integer> myll = new MyLinkedList<>();
        assertEquals(new ArrayList<>(), myll.toList());
        myll.reverse();
        assertEquals(new ArrayList<>(), myll.toList());
    }
}