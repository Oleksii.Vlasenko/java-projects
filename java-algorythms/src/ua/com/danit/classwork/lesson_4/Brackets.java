package ua.com.danit.classwork.lesson_4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Brackets {

    public HashMap<Character, Character> symbols = new HashMap<>() {{
        put('(', ')');
        put('[', ']');
        put('{', '}');
    }};

    boolean isCorrect(String s) {
        Stack<Character> chars = new Stack<>();
        int index = 0;

        for (char ch : s.toCharArray()) {
            if (symbols.containsKey(ch)) {
                chars.push(s.charAt(index));
            } else {
                if (symbols.get(chars.peek()) != ch) return false;
                chars.pop();
            }
        }
        return chars.empty();
    }
}

class BracketsSpec {

    private Brackets b;

    @BeforeEach
    public void before() {
        this.b = new Brackets();
    }

    @Test
    public void test1() {
        assertTrue(b.isCorrect("([]){}"));
        assertTrue(b.isCorrect("([])({})"));
        assertTrue(b.isCorrect("[()][{()}()]"));
        assertTrue(b.isCorrect("{([()])(())}"));
        assertFalse(b.isCorrect("((}}"));
        assertFalse(b.isCorrect("(([])"));
    }
}
