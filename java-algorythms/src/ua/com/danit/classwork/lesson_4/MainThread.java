package ua.com.danit.classwork.lesson_4;

import java.util.*;
import java.util.stream.Collectors;

public class MainThread {
    static class Counter{
        private int i = 0;

        public synchronized int increment() {
            return i = i + 1;
        }
    }

    public static void main(String[] args) throws InterruptedException {
//        Counter c = new Counter();
//        int REPEAT = 10_000_000;
//        Runnable r = () -> {
//            for (int i = 0; i < REPEAT; i++) {
//                c.increment();
//            }
//        };
//        Thread t1 = new Thread(r);
//        Thread t2 = new Thread(r);
//
//        System.out.println(t1.getState());
//        t1.start();
//        t2.start();
//        System.out.println(t1.isAlive());
//        t1.join();
//        t2.join();
//
//        System.out.println(t1.isAlive());
//        System.out.println(c.increment());
//
//        Collection<Integer> collection = new HashSet<>(){{
//            add(1);
//            add(2);
//        }};
//        String[] str = collection.toArray(new String[0]);
//
//
        List<String> str = Arrays.asList(new String[]{"a", "b", "c", "d", "e"});
        List<String> strings = str.subList(0, 3);
        System.out.println(strings);
        strings.set(1, "x");
        System.out.println(str);

    }
}
