package ua.com.danit.classwork.lesson_4;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class Rectangle {

    private final int width;
    private final int height;

    Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int inscribed(Rectangle r) {
        int hCompare = r.getHeight() - this.height;
        int wCompare = r.getWidth() - this.width;
        int result = hCompare + wCompare;
        if ((hCompare < 0 && wCompare < 0) || (hCompare > 0 && wCompare > 0)) return result;
        return 0;
    }

    public static ArrayList<Rectangle> searchThread(LinkedList<Rectangle> list) {
        ArrayList<ArrayList<Rectangle>> aal = list.stream().reduce(new ArrayList<>(), (x, y) -> {
                    x.forEach(al -> {
                        if (al.stream().allMatch(r -> r.inscribed(y) != 0)) {
                            al.add(y);
                        }
                    });
                    x.add(new ArrayList<>(List.of(y)));
                    return x;
                }, (x, y) -> x);
        ArrayList<Rectangle> al = aal.stream().reduce((x, y) -> x.size() > y.size() ? x : y).orElseGet(ArrayList::new);
        al.sort(Rectangle::inscribed);
        return al;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return width == rectangle.width &&
                height == rectangle.height;
    }

    @Override
    public int hashCode() {
        return Objects.hash(width, height);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}

class RectangleListSpec {

    @Test
    public void compare_test1() {
        Rectangle r1 = new Rectangle(5, 5);
        Rectangle r2 = new Rectangle(4, 4);
        Rectangle r3 = new Rectangle(3, 5);
        assertTrue(r1.inscribed(r2) < 0);
        assertFalse(r2.inscribed(r1) < 0);
        assertEquals(r1.inscribed(r3), 0);
        assertFalse(r1.inscribed(r3) < 0);
    }

    @Test
    public void search_test1() {
        LinkedList<Rectangle> list = new LinkedList<>(){{
                add(new Rectangle(101, 1));
                add(new Rectangle(102, 2));
                add(new Rectangle(103, 3));
                add(new Rectangle(4, 204));
                add(new Rectangle(5, 205));
                add(new Rectangle(11, 11));
                add(new Rectangle(12, 12));
                add(new Rectangle(13, 13));
                add(new Rectangle(14, 14));
            }};
        LinkedList<Object> result = new LinkedList<>() {{
            add(new Rectangle(14, 14));
            add(new Rectangle(13, 13));
            add(new Rectangle(12, 12));
            add(new Rectangle(11, 11));

        }};
        ArrayList<Rectangle> arrayList = Rectangle.searchThread(list);
        assertEquals(result, arrayList);
    }
}