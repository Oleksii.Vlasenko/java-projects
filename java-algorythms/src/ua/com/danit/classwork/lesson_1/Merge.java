package ua.com.danit.classwork.lesson_1;

import java.io.FileInputStream;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Merge<A extends Comparable<A>> {

    public A[] merge (Class<A> type, A[] a, A[] b) {
        A[] c = (A[]) Array.newInstance(type,a.length + b.length);
        int i = 0;
        int j = 0;

        while(i < a.length && j < b.length)
            c[i + j] = a[i].compareTo(b[j]) < 0 ? a[i++] : b[j++];

        while (i < a.length)
            c[i + j] = a[i++];

        while (j < b.length)
            c[i + j] = b[j++];

        return c;
    }

    public static void main(String[] args) {
        Integer[] a = {1, 100};
        Integer[] b = {2, 3, 4, 5, 6};
        Merge<Integer> mergeApp = new Merge<>();
        Integer[] c = mergeApp.merge(Integer.class, a, b);
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
        System.out.println(Arrays.toString(c));
    }
}
