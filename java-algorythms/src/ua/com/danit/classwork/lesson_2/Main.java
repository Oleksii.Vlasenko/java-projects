package ua.com.danit.classwork.lesson_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Main {

    public static final String[] NOTATIONS = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D",
            "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public static String intToString(int num) {
        String result = "";
        int i = 0;
        while ((num >> i) > 0) {
            result = (((num >> i++) & 1) == 0 ? "0" : "1") + result;
        }
        return result;
    }

    public static int stringToInt1(String str) {
        int result = 0;
        int i = 0;
        while (str.length() > i) {
            result += str.charAt(str.length() - 1 - i) == '1' ? (int) Math.pow(2, i) : 0;
            i++;
        }
        return result;
    }

    public static int stringToInt2(String str) {
        int result = 0;
        int i = 0;
        while (str.indexOf('1', i) != -1) {
            result += Math.pow(2, str.indexOf('1', i));
            i = str.indexOf('1', i) + 1;
        }
        return result;
    }

    public static int pow(int a, int p) {
        return IntStream.rangeClosed(1, p).map(i -> a)
                .reduce(1, (x, y) -> x * y);
    }

    public static String[] combineToString(int num) {
        String[] result = new String[Main.pow(2, num)];
        Arrays.fill(result, "");
        IntStream.rangeClosed(0, pow(2, num) - 1)
                .forEach(i -> IntStream.rangeClosed(0, num - 1)
                        .forEach(x ->
                                result[i] = (((i >> x) & 1) == 0 ? "0" : "1") + result[i]));
        return result;
    }

    public static String[] combineToStringWhile(int num) {
        String[] result = new String[Main.pow(2, num)];
        Arrays.fill(result, "");
        int i = 0;
        while(i < Main.pow(2, num)) {
            for (int j = 0; j < num; j++) {
                result[i] = (((i >> j) & 1) == 0 ? "0" : "1") + result[i];
            }
            i++;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(combineToStringWhile(3)));
    }

    public static ArrayList<String> combineToStringR(int num, ArrayList<String> result, String item) {
        if (num-- < 1) {
            result.add(item);
            return result;
        }
        combineToStringR(num, result, item + "0");
        combineToStringR(num, result, item + "1");
        return result;
    }

    // return result; //java.lang.IllegalStateException: stream has already been operated upon or closed



    public static Stream<String> combineToStringRStream(int i, String item) {
        if (i == 0) {
            return Stream.of(item);
        }
        return Stream.concat(
                combineToStringRStream(i - 1, item + "0"),
                combineToStringRStream(i - 1, item + "1"));
    }

    public static String convertNumber(int num, int not) {
        StringBuilder result = new StringBuilder();
        while (num > 0) {
            result.insert(0, NOTATIONS[num % not]);
            num /= not;
        }
        return result.toString();
    }

    public static String convertNumberR(int num, int not, String str) {
        if (num == 0) return str;
        return convertNumberR(num / not, not, NOTATIONS[num % not] + str);
    }
}

class MainTest {

    @Test
    void intToString1() {
        assertEquals("101", Main.intToString(5));
    }

    @Test
    void intToString2() {
        assertEquals("111", Main.intToString(7));
    }

    @Test
    void stringToInt11() {
        assertEquals(5, Main.stringToInt1("101"));
    }

    @Test
    void stringToInt12() {
        assertEquals(9, Main.stringToInt1("1001"));
    }

    @Test
    void combineToString1() {
        assertArrayEquals(new String[]{"0", "1"}, Main.combineToString(1));
    }

    @Test
    void combineToString2() {
        assertArrayEquals(new String[]{"00", "01", "10", "11"}, Main.combineToString(2));
    }

    @Test
    void combineToString3() {
        assertArrayEquals(new String[]{"000", "001", "010", "011", "100", "101", "110", "111"}, Main.combineToString(3));
    }

    @Test
    void combineToStringR1() {
        assertArrayEquals(new String[]{"0", "1"}, Main.combineToStringR(1, new ArrayList<String>(), "").toArray());
    }

    @Test
    void combineToStringR2() {
        assertArrayEquals(new String[]{"00", "01", "10", "11"}, Main.combineToStringR(2, new ArrayList<String>(), "").toArray());
    }

    @Test
    void combineToStringR3() {
        assertArrayEquals(new String[]{"000", "001", "010", "011", "100", "101", "110", "111"}, Main.combineToStringR(3, new ArrayList<String>(), "").toArray());
    }

    @Test
    void combineToStringRStream1() {
        assertArrayEquals(new String[]{"0", "1"}, Main.combineToStringRStream(1, "").toArray());
    }

    @Test
    void combineToStringRStream2() {
        assertArrayEquals(new String[]{"00", "01", "10", "11"}, Main.combineToStringRStream(2, "").toArray());
    }

    @Test
    void combineToStringRStream3() {
        assertArrayEquals(new String[]{"000", "001", "010", "011", "100", "101", "110", "111"}, Main.combineToStringRStream(3, "").toArray());
    }

    @Test
    void convertNumber1() {
        assertEquals("F", Main.convertNumber(15, 16));
    }

    @Test
    void convertNumber2() {
        assertEquals("1111", Main.convertNumber(15, 2));
    }


    @Test
    void convertNumber3() {
        assertEquals("Z", Main.convertNumber(35, 36));
    }

    @Test
    void convertNumberR1() {
        assertEquals("F", Main.convertNumberR(15, 16, ""));
    }

    @Test
    void convertNumberR2() {
        assertEquals("1111", Main.convertNumberR(15, 2, ""));
    }


    @Test
    void convertNumberR3() {
        assertEquals("Z", Main.convertNumberR(35, 36, ""));
    }
}