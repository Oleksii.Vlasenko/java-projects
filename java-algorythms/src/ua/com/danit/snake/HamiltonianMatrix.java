package ua.com.danit.snake;

import org.junit.jupiter.api.Test;

import java.util.*;

public class HamiltonianMatrix {

    class Node {
        Node up;

        Node right;
        Node down;
        Node left;
        int status = 0;

        private final int[] position;

        Node(int[] position) {
            this.position = position;
        }

        public int[] getPosition() {
            return position;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return status == node.status &&
                    Arrays.equals(position, node.position);
        }

        @Override
        public int hashCode() {
            int result = Objects.hash(status);
            result = 31 * result + Arrays.hashCode(position);
            return result;
        }

        @Override
        public String toString() {
            return "Vertex{" +
                    ", position=" + Arrays.toString(position) +
                    ", status=" + status +
                    '}';
        }

    }

    private final int boardWidth;
    private final int boardHeight;
    private Node startNode;

    public ArrayList<LinkedList<Node>>[][] getBasicP() {
        return basicP;
    }

    private Node[][] graph;

    public Node[][] getBaseMatrix() {
        return baseMatrix;
    }

    private Node[][] baseMatrix;
    private ArrayList<LinkedList<Node>>[][] basicP;

    public HamiltonianMatrix(int boardWidth, int boardHeight, int[] startPosition) {
        this.boardWidth = boardWidth;
        this.boardHeight = boardHeight;
        this.graph = new Node[boardHeight][boardWidth];
        baseMatrix = new Node[boardHeight * boardWidth][boardHeight * boardWidth];
        basicP = new ArrayList[boardHeight * boardWidth][boardHeight * boardWidth];
        this.initGraph();
        this.startNode = this.getNode(startPosition);
    }

    private void initGraph() {
        for (int i = 0; i < this.boardHeight; i++) {
            for (int j = 0; j < this.boardWidth; j++) {
                this.graph[i][j] = new Node(new int[]{i, j});
            }
        }
        for (int i = 0; i < this.boardHeight; i++) {
            for (int j = 0; j < this.boardWidth; j++) {
                Node node = this.getNode(new int[]{i, j});
                if (i - 1 >= 0) {
                    node.up = this.getNode(new int[]{i - 1, j});
                    baseMatrix[i * boardWidth + j][(i - 1) * boardWidth + j] = this.getNode(new int[]{i - 1, j});
                    basicP[i * boardWidth + j][(i - 1) * boardWidth + j] = new ArrayList<LinkedList<Node>>();
                }
                if (j + 1 <= this.boardWidth - 1) {
                    node.right = this.getNode(new int[]{i, j + 1});
                    baseMatrix[i * boardWidth + j][i * boardWidth + j + 1] = this.getNode(new int[]{i, j + 1});
                    basicP[i * boardWidth + j][i * boardWidth + j + 1] = new ArrayList<LinkedList<Node>>();
                }
                if (i + 1 <= this.boardHeight - 1) {
                    node.down = this.getNode(new int[]{i + 1, j});
                    baseMatrix[i * boardWidth + j][(i + 1) * boardWidth + j] = this.getNode(new int[]{i + 1, j});
                    basicP[i * boardWidth + j][(i + 1) * boardWidth + j] = new ArrayList<LinkedList<Node>>();
                }
                if (j - 1 >= 0) {
                    node.left = this.getNode(new int[]{i, j - 1});
                    baseMatrix[i * boardWidth + j][i * boardWidth + j - 1] = this.getNode(new int[]{i, j - 1});
                    basicP[i * boardWidth + j][i * boardWidth + j - 1] = new ArrayList<LinkedList<Node>>();
                }
                this.graph[i][j] = node;
            }
        }
    }

    public Node getNode(int[] position) {
        for (int i = 0; i < this.boardHeight; i++) {
            for (int j = 0; j < this.boardWidth; j++) {
                if (Arrays.equals(this.graph[i][j].getPosition(), position)) {
                    return this.graph[i][j];
                }
            }
        }
        throw new NoSuchElementException();
    }
    
//    private ArrayList<LinkedList<Node>> addToList(ArrayList<LinkedList<Node>> list, Node node) {
//        if (list.size() > 0) {
//            Iterator iterator = list.iterator();
//            while(iterator.hasNext()) {
//
//            }
//        }
//
//        return result;
//    }

    public ArrayList<LinkedList<Node>>[][] matrixMultiply() {
        ArrayList<LinkedList<Node>>[][] result = new ArrayList[basicP.length][basicP[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = basicP[i][j];
            }
        }
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                for (int k = 0; k < result.length; k++) {
//                    result[i][j] = result[i][k] + baseMatrix[k][j];
//                    result[i][j] = result[i][j] != null ? result[i][k] + baseMatrix[k][j];

                }
            }
        }


        return null;
    }


}

class HamiltonianMatrixSpec {

    @Test
    public void test_1() {
        HamiltonianMatrix hm = new HamiltonianMatrix(2, 2, new int[]{0, 0});
        System.out.println(Arrays.deepToString(hm.getBaseMatrix()));
        System.out.println(Arrays.deepToString(hm.getBasicP()));
    }
}
