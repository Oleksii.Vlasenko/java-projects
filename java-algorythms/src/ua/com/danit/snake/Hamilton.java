package ua.com.danit.snake;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class Hamilton {

    private final int height;
    private final int width;
    private final Set<Integer>[] graph;
    private final LinkedList<Integer> path;
    private final int start;

    public Hamilton(int height, int width) {
        this(height, width, new int[]{0, 0});
    }

    public Hamilton(int height, int width, int[] start) {
        this.height = height;
        this.width = width;
        this.graph = init();
        this.path = new LinkedList<>();
        this.start = start[0] * width + start[1];
    }

    public Set<Integer>[] getGraph() {
        return graph;
    }

    private Set<Integer>[] init() {
        Set<Integer>[] graph = new Set[height * width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                graph[i * width + j] = new HashSet<Integer>();
                if (i - 1 >= 0) graph[i * width + j].add((i - 1) * width + j);
                if (j + 1 <= width - 1) graph[i * width + j].add(i * width + j + 1);
                if (i + 1 <= height - 1) graph[i * width + j].add((i + 1) * width + j);
                if (j - 1 >= 0) graph[i * width + j].add(i * width + j - 1);
            }
        }
        return graph;
    }

    public static void swap(LinkedList<Integer> list, int i) {
        while (i > 1) {
            int item = list.remove(1);
            list.add(i, item);
            i--;
        }
    }

    public LinkedList<Integer> bfs(int v, int w) {
        boolean[] visited = new boolean[this.graph.length];
        int[] color = new int[this.graph.length];
        int[] parent = new int[this.graph.length];
        int[] dist = new int[this.graph.length];

        Arrays.fill(color, -1);
        Arrays.fill(parent, -1);
        Arrays.fill(dist, 0);

        color[v] = color[v] + 1;
        LinkedList<Integer> q = new LinkedList<>();
        q.addLast(v);
        while (!q.isEmpty()) {
            int u = q.removeFirst();
            for (Integer item : this.graph[u]) {
                if (color[item] == -1) {
                    color[item] = color[item] + 1;
                    dist[item] = dist[u] + 1;
                    parent[item] = u;
                    q.addLast(item);
                }
            }
            color[u] = color[u] + 1;
        }
        LinkedList<Integer> result = new LinkedList<>();
        int curr = w;
        while (curr != v) {
            result.addFirst(curr);
            curr = parent[curr];
        }
        result.addFirst(v);
        return result;
    }

    public LinkedList<Integer> bfs(int v, int w, int stone, int[] snake) {
        boolean[] visited = new boolean[this.graph.length];
        int[] color = new int[this.graph.length];
        int[] parent = new int[this.graph.length];
        int[] dist = new int[this.graph.length];

        Arrays.fill(color, -1);
        Arrays.fill(parent, -1);
        Arrays.fill(dist, 0);

        color[stone] = 2;
        for (int i = 0; i < snake.length; i++) {
            color[snake[i]] = 2;
        }

        color[v] = color[v] + 1;
        LinkedList<Integer> q = new LinkedList<>();
        q.addLast(v);
        while (!q.isEmpty()) {
            int u = q.removeFirst();
            for (Integer item : this.graph[u]) {
                if (color[item] == -1) {
                    color[item] = color[item] + 1;
                    dist[item] = dist[u] + 1;
                    parent[item] = u;
                    q.addLast(item);
                }
            }
            color[u] = color[u] + 1;
        }
        LinkedList<Integer> result = new LinkedList<>();
        int curr = w;
        while (curr != v) {
            result.addFirst(curr);
            curr = parent[curr];
        }
        result.addFirst(v);
        return result;
    }

    public LinkedList<Integer> searchPaths(int stone) {
        LinkedList<Integer> result = new LinkedList<>();
        for (int i = 0; i < this.graph.length; i++) {
            if (i != stone) result.addLast(i);
        }
        int count = 0;
        int max = 0;
        while (count < result.size() - 1) {
            if (!this.graph[result.get(0)].contains(result.get(1))) {
                count = 0;
                int j = 2;
                while (!this.graph[result.get(0)].contains(result.get(j))) j++;
                swap(result, j);
                continue;
            }
            count++;
            if (max < count) max = count;
            int temp = result.removeFirst();
            result.addLast(temp);
        }
        return result;
    }

    public boolean hasPath(int curr, boolean[] used, LinkedList<Integer> path, int last) {
        path.addLast(curr);
        used[curr] = true;
        if (path.size() == graph.length) {
            if (path.getLast() == last) {
                return true;
            } else {
                path.removeLast();
                used[curr] = false;
                return false;
            }
        }
        for (Integer v : graph[curr]) {
            if (!used[v])
                if (hasPath(v, used, path, last))
                    return true;
        }
        used[curr] = false;
        path.removeLast();
        return false;
    }

    public LinkedList<Integer> hasMaxPath(int curr) {
        for (Integer v : graph[curr]) { //1
            for (Integer a : graph[v]) { //0 2 7
                if (a != curr) { // 2 7
                    HashSet<Integer> set = new HashSet<>(graph[a]);
                    set.remove(v);
                    if (set.retainAll(graph[curr])) {
                        if (set.contains(v)) continue;
                        for (Integer b : set) {
                            return new LinkedList<>() {{
                                add(curr);
                                add(v);
                                add(a);
                                add(b);
                            }};
                        }
                    }
                }
            }
        }
        return new LinkedList<>();
    }

    private LinkedList<Integer> concat(LinkedList<Integer> a, LinkedList<Integer> b) {
        throw new IllegalArgumentException();
    }

    public LinkedList<Integer> searchFirst(int item0) {
        LinkedList<Integer> result = new LinkedList<>();
        for (Integer item1 : this.graph[item0]) {
            for (Integer item2 : this.graph[item1]) {
                if (item2.equals(item0)) continue;
                for (Integer item3 : this.graph[item2]) {
                    if (item3.equals(item1)) continue;
                    if (this.graph[item3].contains(item0)) {
                        result.add(item0);
                        result.add(item1);
                        result.add(item2);
                        result.add(item3);
                        return result;
                    }
                }
            }
        }
        return result;
    }

    public LinkedList<Integer> searchNext(LinkedList<Integer> list) {
        throw new IllegalArgumentException();
    }
}

class HamiltonSpec {

    @DisplayName("Should init graph")
    @Test
    public void test_1() {
        Hamilton path = new Hamilton(3, 3);
        LinkedList<Integer> integers = path.searchPaths(2);
    }

    @DisplayName("Reverse swap")
    @Test
    public void test_2() {
        LinkedList<Integer> ll = new LinkedList<Integer>() {{
            add(0);
            add(1);
            add(2);
            add(3);
            add(4);
            add(5);
            add(6);
            add(7);
            add(8);
            add(9);
        }};
        LinkedList<Integer> result = new LinkedList<Integer>() {{
            add(0);
            add(6);
            add(5);
            add(4);
            add(3);
            add(2);
            add(1);
            add(7);
            add(8);
            add(9);
        }};
        Hamilton.swap(ll, 6);
        assertEquals(result, ll);
    }

    @DisplayName("Search min graph")
    @Test
    public void test_3() {
        Hamilton path = new Hamilton(6, 6);
        LinkedList<Integer> b = path.hasMaxPath(8);
        System.out.println(b);
        assertTrue(b.contains(8));
    }
}
