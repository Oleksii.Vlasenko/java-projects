package ua.com.danit.snake;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class HamiltonianPath {

    class Node {

        Node up;
        Node right;
        Node down;
        Node left;

        int status = 0;

        private final int[] position;

        Node(int[] position) {
            this.position = position;
        }

        public int[] getPosition() {
            return position;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return status == node.status &&
                    Arrays.equals(position, node.position);
        }

        @Override
        public int hashCode() {
            int result = Objects.hash(status);
            result = 31 * result + Arrays.hashCode(position);
            return result;
        }

        @Override
        public String toString() {
            return "Vertex{" +
                    ", position=" + Arrays.toString(position) +
                    ", status=" + status +
                    '}';
        }
    }

    private final int boardWidth;
    private final int boardHeight;
    private Node startNode;
    private Node[][] graph;
    private LinkedList<Node> way;

    public HamiltonianPath(int boardWidth, int boardHeight, int[] startPosition) {
        this.boardWidth = boardWidth;
        this.boardHeight = boardHeight;
        this.graph = new Node[boardHeight][boardWidth];
        this.initGraph();
        this.startNode = this.getNode(startPosition);
    }

    private void initGraph() {
        for (int i = 0; i < this.boardHeight; i++) {
            for (int j = 0; j < this.boardWidth; j++) {
                this.graph[i][j] = new Node(new int[]{i, j});
            }
        }
        for (int i = 0; i < this.boardHeight; i++) {
            for (int j = 0; j < this.boardWidth; j++) {
                if (i == 2 && j == 2) continue;
                Node node = this.getNode(new int[]{i, j});
                node.up = i - 1 < 0 ? null : this.getNode(new int[]{i - 1, j});
                node.right = j + 1 > this.boardWidth - 1 ? null : this.getNode(new int[]{i, j + 1});
                node.down = i + 1 > this.boardHeight - 1 ? null : this.getNode(new int[]{i + 1, j});
                node.left = j - 1 < 0 ? null : this.getNode(new int[]{i, j - 1});
                this.graph[i][j] = node;
            }
        }
    }

    public Node getNode(int[] position) {
        for (int i = 0; i < this.boardHeight; i++) {
            for (int j = 0; j < this.boardWidth; j++) {
                if (Arrays.equals(this.graph[i][j].getPosition(), position)) {
                    return this.graph[i][j];
                }
            }
        }
        throw new NoSuchElementException();
    }

    public Node getStartNode() {
        return startNode;
    }

    private boolean isLastNode(Node node) {
        if (node == null) return false;
        return (node.up != null && node.up.equals(startNode))
                || (node.right != null && node.right.equals(startNode))
                || (node.down != null && node.down.equals(startNode))
                || (node.left != null && node.left.equals(startNode));
    }

    public Stream<ArrayList<Node>> searchPaths(ArrayList<Node> list, Node curr) {
        if (curr == null || list.contains(curr)) {
            return Stream.empty();
        }
        if (list.size() == (this.boardWidth * this.boardHeight / 2) * 2) {
            return isLastNode(list.get(list.size() - 1)) ? Stream.of(list) : Stream.empty();
        }
        list.add(curr);
        return Stream.concat(
                Stream.concat(searchPaths(new ArrayList<>(list), curr.up), searchPaths(new ArrayList<>(list), curr.right)),
                Stream.concat(searchPaths(new ArrayList<>(list), curr.down), searchPaths(new ArrayList<>(list),curr.left)));
    }



    public ArrayList<Node> concat(ArrayList<Node> a, ArrayList<Node> b) {
        throw new IllegalArgumentException();
    }
}

class HamiltonianPathSpec {
    public HamiltonianPath hp;

    @BeforeEach
    public void init() {
        hp = new HamiltonianPath(5, 5, new int[]{0, 0});
    }

    @Test
    public void test1() {
        assertArrayEquals(new int[]{1, 2}, hp.getNode(new int[]{1, 2}).getPosition());
        assertFalse(Arrays.equals(new int[]{2, 2}, hp.getNode(new int[]{1, 2}).getPosition()));
        assertEquals(hp.getNode(new int[]{0, 2}), hp.getNode(new int[]{1, 2}).up);
    }

    @Test
    public void test2() {
        HamiltonianPath.Node apple = hp.getNode(new int[]{3, 3});
        hp.searchPaths(new ArrayList<>(), hp.getNode(new int[]{0, 0}))
                .distinct()
                .reduce(new ArrayList<>(), (y, x) ->
                        (y.contains(apple) && y.indexOf(apple) < x.indexOf(apple)) ? y : x
                )
                .forEach(System.out::println);
        assertFalse(false);
    }
}
