package ua.com.danit.sorting;

import java.util.ArrayList;
import java.util.Arrays;

public class Examples {
    private static final int[] array = {9, 7, 1, 6, 7, 3, 8, 4, 5, 2};

    public static void main(String[] args) {
        System.out.println(Arrays.toString(bubble_sort(array)));
        System.out.println(Arrays.toString(insertion_sort(array)));
        System.out.println(Arrays.toString(selection_sort(array)));
        System.out.println(Arrays.toString(merge_sort(array)));
        System.out.println(Arrays.toString(quick_sort(array, 0, array.length - 1)));
        System.out.println(Arrays.toString(cocktail_sort(array)));
        System.out.println(Arrays.toString(shell_sort(array, 5)));
        System.out.println(Arrays.toString(comb_sort(array)));
        System.out.println(Arrays.toString(gnome_sort(array)));
    }

    /**
     * time: O(n) - O(n2) - O(n2);
     * memory: O(1) - O(1) - O(1);
     *
     * Прохід зліва направо. Якщо елемент більший за наступний - міняємо місцями. Таким чином відсортований масив вибудовується з правої частини.
     */

    public static int[] bubble_sort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        return arr;
    }

    /**
     * time: O(n) - O(n2) - O(n2);
     * memory: O(1) - O(1) - O(1);
     * Прохід зліва направо. Відсортований масив вибудовується зліва. Кожен наступний елемент вставляється на необхідне місце зліва.
     */

    public static int[] insertion_sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        return arr;
    }

    /**
     * time: O(n) - O(n2) - O(n2);
     * memory: O(1) - O(1) - O(1);
     * Обираємо мінімальний елемент і вставляємо на початок масиву. Продовжуємо з наступного і т.д.
     */

    public static int[] selection_sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    minIndex = j + 1;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
        return arr;
    }

    /**
     * time: O(n·log n) - O(n·log n) - O(n·log n);
     * memory: O(n) - O(n) - O(n);
     * Ділимо масив пополам, поки не отримаємо масив з 1 елемента. Потім з'єднуємо сортуючи.
     */

    public static int[] merge_sort(int[] arrayA) {
        if (arrayA == null) {
            return null;
        }
        if (arrayA.length < 2) {
            return arrayA;
        }

        int[] arrayB = new int[arrayA.length / 2];
        System.arraycopy(arrayA, 0, arrayB, 0, arrayA.length / 2);

        int[] arrayC = new int[arrayA.length - arrayA.length / 2];
        System.arraycopy(arrayA, arrayA.length / 2, arrayC, 0, arrayA.length - arrayA.length / 2);

        arrayB = merge_sort(arrayB);
        arrayC = merge_sort(arrayC);

        return mergeArray(arrayB, arrayC);
    }

    public static int[] mergeArray(int[] arrayA, int[] arrayB) {
        int[] arrayC = new int[arrayA.length + arrayB.length];
        int indexA = 0;
        int indexB = 0;

        for (int i = 0; i < arrayC.length; i++) {
            if (indexA == arrayA.length) {
                arrayC[i] = arrayB[indexB];
                indexB++;
            } else if(indexB == arrayB.length) {
                arrayC[i] = arrayA[indexA];
                indexA++;
            } else if (arrayA[indexA] < arrayB[indexB]) {
                arrayC[i] = arrayA[indexA];
                indexA++;
            } else {
                arrayC[i] = arrayB[indexB];
                indexB++;
            }
        }

        return arrayC;
    }

    /**
     * time: O(n·log n) - O(n·log n) - O(n2);
     * memory: O(1) - O(1) - O(1);
     * Обираємо елемент і розташовуємо так, щоб менші були ліворуч, більші - праворуч. Тким чином елемент на своєму місці. Потім повторюємо для лівої та правої частин.
     */

    public static int[] quick_sort(int[] arr, int leftBorder, int rightBorder) {
        int leftMarker = leftBorder;
        int rightMarker = rightBorder;
        int item = arr[(leftMarker + rightMarker) / 2];
        do {
            while (arr[leftMarker] < item) {
                leftMarker++;
            }
            while (arr[rightMarker] > item) {
                rightMarker--;
            }
            if (leftMarker <= rightMarker) {
                if (leftMarker < rightMarker) {
                    int temp = arr[rightMarker];
                    arr[rightMarker] = arr[leftMarker];
                    arr[leftMarker] = temp;
                }
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);
        if (leftMarker < rightBorder) {
            quick_sort(arr, leftMarker, rightBorder);
        }
        if (leftBorder < rightMarker) {
            quick_sort(arr, leftBorder, rightMarker);
        }
        return arr;
    }

    /**
     * time: O(n) - O(n2) - O(n2);
     * memory: O(n) - O(n) - O(n);
     * Різновид bubble_sort.
     */

    public static int[] cocktail_sort (int[] arr) {
        int buff;
        int left = 0; //перший елемент
        int right = arr.length - 1; //останній елемент

        do {
            //шукаємо макс
            for (int i = left; i < right; i++) {
                if (arr[i] > arr[i + 1]) {
                    buff = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = buff;
                }
            }
            right--;
            //шукаємо мін
            for (int i = right; i > left; i--) {
                if (arr[i] < arr[i - 1]) {
                    buff = arr[i];
                    arr[i] = arr[i - 1];
                    arr[i - 1] = buff;
                }
            }
            left++;
        } while (left < right);
        return arr;
    }

    public static int[] shell_sort (int[] arr, int n) {
        for (int step = n / 2; step > 0; step /= 2) {
            for (int i = step; i < n; i++) {
                for (int j = i - step; j >= 0 && arr[j] > arr[j + step] ; j -= step) {
                    int x = arr[j];
                    arr[j] = arr[j + step];
                    arr[j + step] = x;
                }
            }
        }
        return arr;
    }

    /**
     *
     */

    public static int[] comb_sort (int[] arr) {
        int gap = arr.length;
        boolean swapped = true;
        while (gap > 1 || swapped) {
            if (gap > 1)
                gap = (int) (gap / 1.247330950103979); //фактор зменшення 1 / (1 - е^(-ф)), ф - золотий перетин, ф = (1 + sqrt(5) / 2) == 1.6180339887..., e = 2.71828

            int i = 0;
            swapped = false;
            while (i + gap < arr.length) {
                if (arr[i] > arr[i + gap]) {
                    int t = arr[i];
                    arr[i] = arr[i + gap];
                    arr[i + gap] = t;
                    swapped = true;
                }
                i++;
            }
        }
        return arr;
    }

    public static int[] gnome_sort (int[] arr) {
        int i = 1;
        int j = 2;
        while (i < arr.length) {
            if (arr[i] >= arr[i - 1]) {
                i = j;
                j++;
            } else {
                int tmp = arr[i];
                arr[i] = arr[i - 1];
                arr[i - 1] = tmp;
                i--;
                if (i == 0) {
                    i = j;
                    j++;
                }
            }
        }


        return arr;
    }
}
